<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/img/AKScolor-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li><a href={{ url('/admin/user') }}><span>Manajemen User</span></a></li>
            <li><a href="{{ url('/admin/pendaftaran/') }}"><span>Pendaftaran Mahasiswa</span></a></li>
            <li class="treeview">
                <a href="#"><span>Administrasi Data</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/admin/program-studi') }}">Program Studi</a></li>
                    <li><a href="{{ url('/admin/dosen') }}"><span>Dosen</span></a></li>
                    <li><a href="{{ url('/admin/mahasiswa') }}"><span>Mahasiswa</span></a></li>
                    <li><a href="{{ url('/admin/mahasiswa') }}"><span></span></a></li>
                    <li><a href={{ url('/admin/matakuliah') }}>Matakuliah</a></li>
                    <li><a href={{ url('/admin/kurikulum') }}>Kurikulum</a></li>
                    <li><a href={{ url('/admin/kelas/') }}>Kelas</a></li>
                    <li><a href="{{ url('/admin/mahasiswa') }}"><span></span></a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><span>Proses Perkuliahan</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">KRS Mahasiswa</a></li>
                    <li><a href="{{ url('/admin/krs') }}">KRS Kolektif</a></li>
                    <li><a href="{{ url('/admin/nilai') }}">Input Nilai</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><span>View Data</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/admin/khs') }}">KHS </a></li>
                    <li><a href="{{ url('/admin/transkrip') }}">Transkrip </a></li>
                    <li><a href="{{ url('/admin/mahasiswa') }}"><span></span></a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>