<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset("/components/iCheck/skins/flat/green.css")}}" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('admin/header')

<!-- Sidebar -->
@include('admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Tambah Kurikulum" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box">
                <div class="box-body">
                    <form id="frmInput" method="POST" action="{{ url('/admin/kurikulum/store') }}" name="frmInput" class="form-horizontal">

                        <div class="form-group">
                            <label for="nama" class="col-md-2 control-label">Nama</label>

                            <div class="col-md-2">
                                <input id="nama" type="text" class="form-control" name="nama">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="prodi" class="col-md-2 control-label" > Program Studi</label>
                            <div class="col-md-10">
                            <select disabled class="form-control col-md-1 select2" style="width: 10%;" id="prodi" name="prodi">
                                @foreach ($prodis as $prodi)
                                    <option @if ($prodi->id == $prodiId) {{ 'selected' }} @endif value={{$prodi->prodi_id}}>{{$prodi->prodi_name}}</option>
                                @endforeach
                            </select>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="tahun" class="col-md-2 control-label" > Tahun Berlaku</label>
                            <div class="col-md-10">
                            <select class="form-control select2" style="width: 10%;" id="tahun" name="tahun">
                                @foreach ($years as $year)
                                    <option @if ($year->id == $yearId) {{ 'selected' }} @endif value={{$year->year_id}}>{{$year->year_akademik}}</option>
                                @endforeach
                            </select>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="required" class="col-md-2 control-label">Jumlah SKS Wajib</label>

                            <div class="col-md-2">
                                <input id="required" type="text" class="form-control" value="{{ $info->sksWajib }}" name="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="elective" class="col-md-2 control-label">Jumlah SKS Pilihan</label>

                            <div class="col-md-2">
                                <input id="elective" type="text" class="form-control" value="{{ $info->sksPilihan }}" name="elective">
                            </div>
                        </div>

                        <input id="kurikulum_id" type="hidden" name="kurikulum_id" value="{{ $info->kurikulum_id }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                </div>
            </div>
            <div class="box">
                <div class="box-body">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-danger btn-block" id="btn-cancel" value="cancel">Batal</button>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-block" id="btn-save" value="add">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Admin</h4>
                        </div>
                        <div class="modal-body">
                            <table id= "mk_list" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr role="row" class="odd">
                                    <th>Kode MK</th>
                                    <th>Nama MK</th>
                                    <th>SKS</th>
                                </tr>
                                </thead>
                                <tbody id="data_list">
                                @foreach ($datas as $data)

                                    <tr id="data{{$data->matakuliah_id}}">
                                        <td>{{$data->kode_mk}}</td>
                                        <td>{{$data->nama_mk}}</td>
                                        <td>{{$data->sks}}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- /.box -->>
<!-- Modal -->
<div class="modal fade" id="dlgConfirm" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Data berhasil disimpan, tambahkan data lagi?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- Footer -->
@include('footer')

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>
<!-- iCheck    -->
<script src="{{ asset ("/components/iCheck/iCheck.js") }}"></script>

<script>

    $(document).ready(function(){
        var t = $('#mk_list').DataTable({
            'aoColumnDefs': [
                { targets: [1], searchable: true, orderable:false},
                { targets: '_all', searchable:false, orderable:false}

            ],
            "bAutoWidth": false, // Disable the auto width calculation
            "aoColumns": [
                { "sWidth": "5%" }, // 2nd column width
                { "sWidth": "38%" },
                { "sWidth": "2%" },
                { "sWidth": "10%" },
                { "sWidth": "5%" },
                { "sWidth": "37%" },
                { "sWidth": "3%" }
            ],
            "iDisplayLength": 50
        });
    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
