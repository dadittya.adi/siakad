<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset("/components/iCheck/skins/flat/green.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('/admin/header')

<!-- Sidebar -->
@include('/admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Manajemen User" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <!-- form start -->
                            <form class="form-horizontal">

                                <div class="form-group">

                                    <label for="sel_level" class="col-md-1 control-label" > Level</label>

                                    <select class="form-control col-md-1 select2" style="width: 10%;" id="sel_level" name="sel_level">
                                        <option selected="selected">Admin</option>
                                        <option>Dosen</option>
                                        <option>Mahasiswa</option>
                                    </select>
                                </div>
                                <div class="form-group row">
                                    <label for="sel_status" class="col-md-1 control-label" > Status</label>
                                    <select class="form-control col-md-1 select2" style="width: 10%;" id="sel_status" name="sel_status">
                                        <option selected="selected" value="2">All</option>
                                        <option value="1">Aktif</option>
                                        <option value="0">Non-Aktif</option>
                                    </select>
                                    <button class="btn btn-info" id="btn_search">
                                        <span class="glyphicon glyphicon-search"></span> Search
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat pull-right" id="btn-add" name="btn-add">
                                <span class="glyphicon glyphicon-plus"></span> Tambah Admin</button>
                        </div>
                    </div>

                    <!-- /.box-footer -->
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <table id= "user_table" class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr role="row" class="odd">
                            <th>Nama</th>
                            <th class="nosort">Username</th>
                            <th class="nosort">Level</th>
                            <th class="nosort"align="center"></th>
                        </tr>
                        </thead>
                        <tbody id="user_list">
                        @foreach ($datas as $data)
                            <tr id="user{{$data->user_id}}">
                                <td>{{$data->name}}</td>
                                <td>{{$data->username}}</td>
                                <td>{{$data->level}}</td>
                                <td>
                                    <div class="col-sm-2">
                                        <button   class="btn btn-warning  btn-xs  btn-block btn-detail open-modal" value="{{$data->user_id}}">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                    </div>
                                    <div class="col-sm-2">
                                    @if($data->status == '1')
                                        <button class="btn btn-success  btn-xs btn-block btn-deactivate deactivate-user" value="{{$data->user_id}}">
                                            <span class="glyphicon glyphicon-lock"></span>
                                        </button>
                                    @else
                                        <button class="btn btn-danger  btn-xs btn-block btn-activate activate-user" value="{{$data->user_id}}">
                                            <span class="glyphicon glyphicon-unlock"></span>
                                        </button>
                                    @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Level</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Admin</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmUser" name="frmUser" class="form-horizontal" novalidate="">

                                <div class="form-group">
                                    <label for="input_level" class="col-md-4 control-label">Level</label>

                                    <div class="col-md-7">
                                        <input id="input_level" type="text" class="form-control" name="input_level" value="Admin" disabled>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('input_username') ? ' has-error' : '' }}">
                                    <label for="input_username" class="col-md-4 control-label">Username</label>

                                    <div class="col-md-7">
                                        <input id="input_username" type="text" class="form-control" name="input_username" value="{{ old('input_username') }}" required autofocus>

                                        @if ($errors->has('input_username'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('input_username') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_name" class="col-md-4 control-label">Name</label>

                                    <div class="col-md-7">
                                        <input id="input_name" type="text" class="form-control" name="input_name" value="{{ old('input_name') }}" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="input_password" class="col-md-4 control-label" >Password</label>

                                    <div class="col-md-7">
                                        <input id="input_password" type="password" class="form-control" name="input_password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_rePassword" class="col-md-4 control-label" id="label_rePassword">Confirm Password</label>

                                    <div class="col-md-7">
                                        <input id="input_rePassword" type="password" class="form-control" name="input_rePassword" required>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-danger btn-block" id="btn-cancel" value="cancel">Batal</button>
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-success btn-block" id="btn-save" value="add">Simpan</button>
                                </div>
                                <div class="col-sm-3">
                                </div>
                            </div>
                        </div>

                            <input type="hidden" id="user_id" name="user_id" value="0">

                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
@include('footer')
</div><!-- ./wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- iCheck    -->
<script src="{{ asset ("/components/iCheck/iCheck.js") }}"></script>
<script>
    $(function () {
        $('#user').DataTable({
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort'],

            }],
            "bAutoWidth": false, // Disable the auto width calculation
            "aoColumns": [
                { "sWidth": "35%" }, // 1st column width
                { "sWidth": "30%" }, // 2nd column width
                { "sWidth": "10%" },
                { "sWidth": "25%" }
            ],
            "iDisplayLength": 50,
            "processing": true,
            "language": {
                "processing": "<div></div><div></div><div></div><div></div><div></div>"
            }
        });
    });
</script>
<script>
    $(document).ready(function(){

        var url = "user";

        //display modal form for user editing
        $('.open-modal').click(function(){
            var user_id = $(this).val();
            console.log(url + '/' + user_id);

            $.get("user/" + user_id, function (user) {
                //success data
                console.log(user);
                $('#user_id').val(user.user_id);
                $('#input_level').val(user.level);
                $('#input_name').val(user.name);
                $('#input_username').val(user.username);
                $('#input_password').val('123456789');
                $('#input_rePassword').val('123456789');


                $('#btn-save').val("update");

                document.getElementById('input_name').disabled = true;
                document.getElementById('input_password').disabled = true;
                $('#input_rePassword').hide();
                $('#label_rePassword').hide();


                $('#myModal').modal('show');


            })
        });

        $('body').on('shown.bs.modal', '#myModal', function () {
            $('input:visible:enabled:first', this).focus();
        })

        //display modal form for creating new user
        $('#btn-add').click(function(){


            $('#btn-save').val("add");
            $('#frmUser').trigger('reset');
            $('#input_username').focus();
            document.getElementById('input_name').disabled = false;
            document.getElementById('input_password').disabled = false;
            $('#input_rePassword').show();
            $('#label_rePassword').show();
            $('#myModal').modal('show');
        });

        $('#btn-cancel').click(function(){


            $('#frmUser').trigger("reset");

            $('#myModal').modal('hide')
        });

         //create new user / update existing user
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var user_id = $('#user_id').val();
            var formData = {
                id: $('#user_id').val(),
                name: $('#input_name').val(),
                username: $('#input_username').val(),
                level: $('#input_level').val(),
                password: $('#input_password').val()
            }

            //used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();
            var my_url = "user";

            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += "/edit";
            }
            else {
                var type = "POST"; //for creating new resource
                var user_id = $('#user_id').val();

               my_url += "/add";
            }


            console.log(formData);
            console.log(my_url);
            console.log(type);

            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    var user = '<tr id="user' + data.user_id + '"><td>' + data.name + '</td><td>' + data.username + '</td><td>' + data.level + '</td>';
                    user += '<td><div class="col-sm-2"><button class="btn btn-warning btn-block btn-detail open-modal" value="' + data.user_id + '"><span class="glyphicon glyphicon-pencil"></span> Edit</button></div>';
                    user += '<div class="col-sm-2">';
                    if (data.status == "1") { //if user aktif show deactivate button
                        user += '<button class="btn btn-success btn-block btn-deactivate deactivate-user" value="' + data.user_id + '"> <span class="glyphicon glyphicon-lock"></span> Kunci</button>';
                    }else { //if user non aktif show activate button
                        user += '<button class="btn btn-danger btn-block btn-activate activate-user" value="' + data.user_id + '"> <span class="glyphicon glyphicon-unlock"></span> Buka</button>';
                    }
                    user += '</div></tr>';
                    if (state == "add"){ //if user added a new record
                        $('#user_list').append(user);
                    }else{ //if user updated an existing record

                        $("#user" + data.user_id).replaceWith( user );

                    }

                    $('#frmUser').trigger("reset");

                    $('#myModal').modal('hide')
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#btn_search").click(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var formData = {
                level: $('#sel_level').val(),
                status: $('#sel_status').val(),
            };

            console.log(formData.level);
            console.log(formData.status);
            $('#user_list').empty();
            $.ajax({

                type: "POST",
                url: 'user/search',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    if (data.length == 0) {
                        var str_html = '<tr id="user"><td></td><td></td><td></td><td><div class="col-sm-2"</div><div class="col-sm-2"></div></tr>';

                        $('#user_list').append(str_html);
                    }
                    $.each(data, function(i,item) {
                        var str_html = '<tr id="user' + item.user_id + '"><td>' + item.name + '</td><td>' + item.username + '</td><td>' + item.level + '</td>';
                        str_html += '<td><div class="col-sm-2"><button class="btn btn-warning btn-block btn-detail open-modal" value="' + item.user_id + '"><span class="glyphicon glyphicon-pencil"></span> Edit</button></div>';
                        str_html += '<div class="col-sm-2">';
                        if (item.status == "1") { //if user aktif show deactivate button
                            str_html += '<button class="btn btn-success btn-block btn-deactivate deactivate-user" value="' + item.user_id + '"><span class="glyphicon glyphicon-lock"></span> Kunci</button>';
                        }else { //if user non aktif show activate button
                            str_html += '<button class="btn btn-danger btn-block btn-activate activate-user" value="' + item.user_id + '"><span class="glyphicon glyphicon-unlock"></span> Buka</button>';
                        }
                        str_html += '</div></tr>';

                        $('#user_list').append(str_html);

                    })
                },
                error: function (data) {
                    console.log('Error:', data);
                }

        });


        });
    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
