<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/_all-skins.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />

    <!-- iCheck -->
    <link href="{{ asset("/components/iCheck/skins/flat/green.css")}}" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('admin/header')

<!-- Sidebar -->
@include('admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Manajemen Dosen" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <!-- form start -->
                            <form class="form-horizontal">

                                <div class="form-group row">

                                    <label for="sel_status" class="col-md-1 control-label" > Status</label>
                                    <select class="form-control col-md-1 select2" style="width: 10%;" id="sel_status" name="sel_status">
                                        <option selected="selected" value="0">Semua</option>
                                        @foreach($arrStatus as $status)
                                            <option @if ($status->status_nama == 'Aktif') {{ 'selected' }} @endif
                                                    value={{$status->status_id}}>{{$status->status_nama}}</option>
                                        @endforeach
                                    </select>

                                    <label for="sel_gender" class="col-md-1 control-label" > Gender</label>

                                    <select class="form-control col-md-1 select2" style="width: 10%;" id="sel_gender" name="sel_gender">
                                        <option selected="selected" value="0">Semua</option>
                                        <option value ="L">Laki Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>

                                <div class="form-group row">

                                    <label for="sel_agama" class="col-md-1 control-label" > Agama</label>

                                    <select class="form-control col-md-1 select2" style="width: 10%;" id="sel_agama" name="sel_agama">
                                        <option selected="selected" value="0">Semua</option>
                                        @foreach($arrAgama as $agama)
                                            <option value={{$agama->id}}>{{$agama->name}}</option>
                                        @endforeach
                                    </select>
                                    <button class="btn btn-info" id="btn_search">
                                        <span class="glyphicon glyphicon-search"></span> Search
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat pull-right" id="btn-add" name="btn-add">
                                <span class="glyphicon glyphicon-plus"></span> Tambah Dosen</button>
                        </div>
                    </div>

                    <!-- /.box-footer -->
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <table id= "data_list" class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr role="row" class="odd">
                            <th>Nama</th>
                            <th class="nosort">NIDN/NUP/NIDK</th>
                            <th class="nosort">NIP</th>
                            <th>L/P</th>
                            <th>Agama</th>
                            <th class="nosort"align="center">Tanggal Lahir</th>
                            <th>Status</th>
                            <th class="nosort"align="center"></th>
                        </tr>
                        </thead>
                        <tbody id="data_list">

                        @foreach ($datas as $data)
                            <tr id="data{{$data->dosen_id}}">
                                <td>{{$data->name}}</td>
                                <td>{{$data->nid}}</td>
                                <td>{{$data->nip}}</td>
                                <td>{{$data->gender}}</td>
                                <td>{{$data->agama}}</td>
                                <td>{{$data->dob}}</td>
                                <td>{{$data->status}}</td>
                                <td>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary btn-xs btn-detail open-modal" value="{{$data->dosen_id}}">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmInput" name="frmInput" class="form-horizontal" novalidate="">

                                <div class="form-group" id="groupNama">
                                    <label for="input_nama" class="col-md-4 control-label">Nama <b class="text-red">*</b></label>
                                    <div class="col-md-7 ">
                                        <input id="input_nama" type="text" class="form-control input_nama" name="input_nama">
                                    </div>
                                </div>


                                <div class="form-group"  id="groupNIDN">
                                    <label for="input_nidn" class="col-md-4 control-label">NIDN / NUP / NIDK<b class="text-red">*</b></label>

                                    <div class="col-md-7">
                                        <input id="input_nidn" type="text" class="form-control" name="input_nidn" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_nip" class="col-md-4 control-label">NIP</label>

                                    <div class="col-md-7">
                                        <input id="input_nip" type="text" class="form-control" name="input_nip" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_alamat" class="col-md-4 control-label">Alamat<b class="text-red">*</b></label>

                                    <div class="col-md-7">
                                        <input id="input_alamat" type="text" class="form-control" name="input_alamat" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group"  id="groupTempatLahir">
                                    <label for="input_tempatLahir" class="col-md-4 control-label">Tempat Lahir<b class="text-red">*</b></label>

                                    <div class="col-md-7">
                                        <input id="input_tempatLahir" type="text" class="form-control" name="input_tempatLahir" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group"  id="groupDOB">
                                    <label for="input_dob" class="col-md-4 control-label">Tanggal Lahir<b class="text-red">*</b></label>

                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input id="input_dob" type="text" class="form-control" name="input_dob" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_fixedPhone" class="col-md-4 control-label">Telepon Rumah</label>

                                    <div class="col-md-7">
                                        <input id="input_fixedPhone" type="tel" class="form-control" name="input_fixedPhone" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_mobilePhone" class="col-md-4 control-label">No. Handphone</label>

                                    <div class="col-md-7">
                                        <input id="input_mobilePhone" type="tel" class="form-control" name="input_mobilePhone" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_email" class="col-md-4 control-label">Email</label>

                                    <div class="col-md-7">
                                        <input id="input_email" type="text" class="form-control" name="input_email" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group row" id="radioGender">
                                    <label class="col-md-4 control-label" > Jenis Kelamin<b class="text-red">*</b></label>
                                    <div class="col-md-8">
                                        <label class="radio-inline">
                                            <input class="minimal" type="radio"  name="radio_gender" id="radioGenderL" value="L">
                                            <label for="radioGenderL">&nbsp;Laki-Laki</label>
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="minimal"  name="radio_gender" id="radioGenderP" value="P">
                                            <label for="radioGenderP">&nbsp;Perempuan</label>
                                        </label>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="input_agama" class="col-md-4 control-label">Agama</label>

                                    <div class="col-md-7">
                                        <select class="form-control col-md-1 select2"  id="input_agama" name="input_agama" >
                                            @foreach($arrAgama as $agama)
                                                <option @if($agama->name == 'Islam') {{'selected'}} @endif value={{$agama->id}}>{{$agama->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">

                                    <label for="sel_prodi" class="col-md-4 control-label" > Program Studi</label>
                                    <div class="col-md-7">
                                    <select class="form-control col-md-1 select2" id="input_prodi" name="input_prodi">
                                        <option selected value="0">&emsp;</option>
                                        @foreach ($arrProdi as $prodi)
                                            <option value={{$prodi->id}}>{{$prodi->name}}</option>
                                        @endforeach
                                    </select>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="input_ikatan" class="col-md-4 control-label">Ikatan Kerja</label>

                                    <div class="col-md-7">
                                        <select class="form-control col-md-1 select2"  id="input_ikatan" name="input_ikatan">
                                            <option value="Tetap">Tetap</option>
                                            <option value="Tidak Tetap">Tidak Tetap</option>
                                            <option value="Honorer">Honorer</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_status" class="col-md-4 control-label">Status</label>

                                    <div class="col-md-7">
                                        <select class="form-control col-md-1 select2"  id="input_status" name="input_status">
                                            @foreach($arrStatus as $status)
                                                <option @if($status->status_nama == 'Aktif') {{ 'selected' }} @endif value={{$status->status_id}}>{{$status->status_nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-danger btn-block" id="btn-cancel" value="cancel">Batal</button>
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-success btn-block" id="btn-save" value="add">Simpan</button>
                                </div>
                                <div class="col-sm-3">
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="dosen_id" name="dosen_id" value="0">

                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('footer')
</div><!-- ./wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>
<!-- iCheck    -->
<script src="{{ asset ("/components/icheck/icheck.js") }}"></script>

<script>
    $(function () {
        var t = $('#data_list').DataTable({
            'aoColumnDefs': [
                { targets: [1], searchable: true, orderable:false},
                { targets: '_all', searchable:false, orderable:false}

            ],
            "bAutoWidth": false, // Disable the auto width calculation
            "aoColumns": [
                { "sWidth": "30%" }, // 2nd column width
                { "sWidth": "22%" },
                { "sWidth": "22%" },
                { "sWidth": "2%" },
                { "sWidth": "5%" },
                { "sWidth": "11%" },
                { "sWidth": "3%" },
                { "sWidth": "3%" }
            ],
            "iDisplayLength": 50,
            "processing": true,
            "language": {
                "processing": "<div></div><div></div><div></div><div></div><div></div>",
            },
            "order": [[ 1, 'asc' ]]
        });
    });
</script>
<script>
    $(document).ready(function(){

        //Datemask dd/mm/yyyy
        $('#input_dob').inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        var url = "dosen";
        var inputGender = 0;

        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('input[name="radio_gender"]').on('ifClicked', function (event) {
            inputGender = this.value;
        });

        //display modal form for user editing
        $('.open-modal').click(function(){
            var dosen_id = $(this).val();
            console.log(url + '/' + dosen_id);
            $('#frmInput').trigger("reset");
            $.get("dosen/" + dosen_id, function (dosen) {
                //success data
                console.log(dosen);
                //var dob = moment($('#input_dob').val(), "YYYY-MM-DD").format("DD/MM/YYYY");
                $('#dosen_id').val(dosen.dosen_id);
                $('#input_nama').val(dosen.name);
                $('#input_nidn').val(dosen.nid);
                $('#input_nip').val(dosen.nip);
                $('#input_alamat').val(dosen.alamat);
                $('#input_tempatLahir').val(dosen.tempat_lahir);
                $('#input_dob').val(dosen.dob);
                $('#input_fixedPhone').val(dosen.fixedPhone);
                $('#input_mobilePhone').val(dosen.mobilePhone);
                $('#input_email').val(dosen.email);
                $('#input_prodi').val(dosen.prodi_id).change();
                inputGender = dosen.gender;
                if (dosen.gender == 'L') {
                    $('#radioGenderL').iCheck('check');
                }
                else {
                    $('#radioGenderP').iCheck('check');
                }
                $('#input_gender').val(dosen.gender).change();
                $('#input_agama').val(dosen.agamaId).change();

                $('#input_status').val(dosen.statusId).change();
                $('#input_ikatan').val(dosen.ikatan).change();


                $('#btn-save').val("update");

                $('#myModal').modal('show');


            })
        });

        $('body').on('shown.bs.modal', '#myModal', function () {
            $('input:visible:enabled:first', this).focus();
        })

        //display modal form for creating new data
        $('#btn-add').click(function(){


            $('#btn-save').val("add");
            $('#frminput').trigger("reset");
            $('#myModal').modal('show');
        });

        $('#btn-cancel').click(function(){


            $('#frmInput').trigger("reset");

            $('#myModal').modal('hide')
        });

        //create new user / update existing user
        $("#btn-save").click(function (e) {
            var validation = false;
            if ($('#input_nama').val() == '') {
                $('#input_nama').effect("shake",{ times:3}, 300);
                $('#input_nama').focus();
            }
            else if ($('#input_nidn').val() == '') {
                $('#input_nidn').effect("shake",{ times:3}, 300);
                $('#input_nidn').focus();
            }
            else if ($('#input_alamat').val() == '') {
                $('#input_alamat').effect("shake",{ times:3}, 300);
                $('#input_alamat').focus();
            }
            else if ($('#input_tempatLahir').val() == '') {
                $('#input_tempatLahir').effect("shake",{ times:3}, 300);
                $('#input_tempatLahir').focus();
            }
            else if ($('#input_dob').val() == '') {
                $('#input_dob').effect("shake",{ times:3}, 300);
                $('#input_dob').focus();
            }
            else if (inputGender == 0) {
                $('#radioGender').effect("shake",{ times:3}, 300);
            }
            else {
                validation = true;
            }

            if (validation) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })

                e.preventDefault();

                var dosen_id = $('#dosen_id').val();
                var dob = moment($('#input_dob').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
                var dobforpswd = moment($('#input_dob').val(), "DD/MM/YYYY").format("DDMMYYY");
                var formData = {
                    id: $('#dosen_id').val(),
                    nama: $('#input_nama').val(),
                    nidn: $('#input_nidn').val(),
                    nip: $('#input_nip').val(),
                    alamat: $('#input_alamat').val(),
                    tempatLahir:  $('#input_tempatLahir').val(),
                    dob: dob,
                    fixedPhone: $('#input_fixedPhone').val(),
                    mobilePhone: $('#input_mobilePhone').val(),
                    email: $('#input_email').val(),
                    gender: inputGender,
                    agama: $('#input_agama').val(),
                    prodi: $('#input_prodi').val(),
                    ikatan: $('#input_ikatan').val(),
                    plaindob: dobforpswd,
                    status: $('#input_status').val()
                }

                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var my_url = "dosen";

                if (state == "update") {
                    type = "PUT"; //for updating existing resource
                    my_url += "/edit";
                }
                else {
                    var type = "POST"; //for creating new resource
                    var dosen_id = $('#dosen_id').val();

                    my_url += "/add";
                }


                console.log(formData);
                console.log(my_url);
                console.log(type);

                $.ajax({

                    type: type,
                    url: my_url,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);

                        if (state == "add") { //if user added a new record
                            alert('add');
                            var str_html = '<tr id="data' + data.dosen_id + '"><td>' + data.nama + '</td><td>' + data.nidn + '</td><td>' + data.nip + '</td><td>' + data.gender + '</td><td>' + data.agama + '</td><td>' + data.dob + '</td><td>' + data.status + '</td>';
                            str_html += '<td><div class="col-sm-2"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' + data.dosen_id + '"><span class="glyphicon glyphicon-pencil"></span></button></div>';
                            str_html += '</div></td></tr>';

                            $('#data_list').append(str_html);

                        } else { //if user updated an existing record
                            var str_html = '<tr id="data' + data.dosen_id + '"><td>' + data.nama + '</td><td>' + data.nidn + '</td><td>' + data.nip + '</td><td>' + data.gender + '</td><td>' + data.agama + '</td><td>' + data.dob + '</td><td>' + data.status + '</td>';
                            str_html += '<td><div class="col-sm-2"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' + data.dosen_id + '"><span class="glyphicon glyphicon-pencil"></span></button></div>';
                            str_html += '</div></td></tr>';

                            $("#data" + data.dosen_id).replaceWith(str_html);

                        }

                        $('#frminput').trigger("reset");

                        $('#myModal').modal('hide')
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        $("#btn_search").click(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var formData = {
                status: $('#sel_status').val(),
                gender: $('#sel_gender').val(),
                agama: $('#sel_agama').val()
            };
            var head = '<thead><tr role="row" class="odd"><th>Nama</th><th class="nosort">NIDN/NUP/NIDK</th><th class="nosort">NIP</th><th>L/P</th><th>Agama</th><th class="nosort"align="center">Tanggal Lahir</th><th>Status</th><th class="nosort"align="center"></th></tr></thead>';;
            $('#data_list').empty();

            $.ajax({

                type: "POST",
                url: 'dosen/search',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    var str_html;

                    if (data.length == 0) {
                        str_html += '<tr id="data"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><div class="col-sm-4"</div><div class="col-sm-4"></div></tr></td>';

                        //$('#data_list').append(str_html);
                    }
                    $.each(data, function(i,item) {
                        if (item.agama == null) {
                            item.agama = '';
                        }

                        if (item.status == null) {
                            item.status = '';
                        }

                        if (item.dob == null) {
                            item.dob = '';
                        }

                        str_html += '<tr id="data' + item.dosen_id + '"><td>' + item.name + '</td><td>' + item.nidn + '</td><td>' + item.nip + '</td><td>' + item.gender + '</td><td>' + item.agama + '</td><td>' + item.dob + '</td><td>' + item.status + '</td>';
                        str_html += '<td><div class="col-sm-2"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' + item.dosen_id + '"><span class="glyphicon glyphicon-pencil"></span></button></div>';
                        str_html += '</div></td></tr>';

                        //$('#data_list').append(str_html);

                    })
                    $('#data_list').append(head+str_html);
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });


        });

    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
