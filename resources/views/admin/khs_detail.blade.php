<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('admin/header')

<!-- Sidebar -->
@include('admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "KHS" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <!-- form start -->
                            <form class="form-horizontal">

                                <div class="form-group row">
                                    <div class="col-md-1 control-label" > Nama </div>
                                    <div id="txtNama" class="col-md-2 control-label"> : &emsp; <b>{{$krsInfo->nama}}</b></div>

                                    <div class="col-md-2 control-label" > Beban SKS yang diijinkan </div>
                                    <div id="txtSKS" class="col-md-1 control-label"> :&emsp;<b>{{$krsInfo->allowed_sks}}</b></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-1 control-label" > NIM </div>
                                    <div id="txtNIM" class="col-md-2 control-label"> :&emsp;<b>{{$krsInfo->nim}}</b></div>

                                    <div class="col-md-2 control-label" > IP Semester Lalu </div>
                                    <div id="txtIP" class="col-md-2 control-label"> :&emsp;<b>{{$krsInfo->ip_lalu}}</b></div>
                                </div>
                                <input type="hidden" id="hidkrs_id" name="hidkrs_id" value= {{$id}} />
                                <input type="hidden" id="mahasiswa_id" name="mahasiswa_id" value= {{$krsInfo->mahasiswa_id}} />
                            </form>
                        </div>
                        <div class="box-footer">
                            <div class="form-group row">
                            <label for="sel_semester" class="col-md-1 control-label" > Semester</label>
                            <select class="form-control col-md-1 select2" style="width: 10%;" id="sel_semester" name="sel_semester">
                                @foreach ($semesters as $semester)
                                    <option selected value={{$semester->krs_id}}>{{$semester->krs_semester}}</option>
                                @endforeach
                            </select>

                                <div class="col-sm-1 pull-right">
                                    <button type="submit" class="btn btn-success btn-flat pull-right" id="btn-print" name="btn-print">
                                        <span class="glyphicon glyphicon-print"></span> &nbsp; Cetak</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- /.box-footer -->
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <table id= "data_list" class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr role="row" class="odd">
                            <th>No</th>
                            <th>Kode MK</th>
                            <th>Nama MK</th>
                            <th>SKS</th>
                            <th>Nilai</th>
                            <th class="nosort"align="center"></th>
                        </tr>
                        </thead>
                        <tbody id="data_list">
                        @foreach ($datas as $data)

                            <tr id="data{{$data->krs_detail_id}}">
                                <td></td>
                                <td>{{$data->kode_mk}}</td>
                                <td>{{$data->nama_mk}}</td>
                                <td>{{$data->sks}}</td>
                                <td>{{$data->nilai}}</td>
                                <td>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary btn-xs btn-detail open-modal" value="{{$data->krs_detail_id}}">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('footer')
</div><!-- ./wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>


<script>
    $(function () {
        var t = $('#data_list').DataTable({
            'aoColumnDefs': [
                { targets: [2], searchable: true, orderable:false},
                { targets: '_all', searchable:false, orderable:false}

            ],
            "bAutoWidth": false, // Disable the auto width calculation
            "aoColumns": [
                { "sWidth": "2%" }, // 1st column width
                { "sWidth": "5%" }, // 2nd column width
                { "sWidth": "38%" },
                { "sWidth": "2%" },
                { "sWidth": "10%" },
                { "sWidth": "5%" }
            ],
            "iDisplayLength": 50
        });

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
<script>
    $(document).ready(function(){

        //Datemask dd/mm/yyyy
        var krsId = $('#hidkrs_id').val();
        $('#input_dob').inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        var url = "dosen";

        //display modal form for user editing
        $('.open-modal').click(function(){
            var dosen_id = $(this).val();
            $('#frmInput').trigger("reset");
            $.get("dosen/" + dosen_id, function (dosen) {
                //success data
                console.log(dosen);
                var dob = moment($('#input_dob').val(), "YYYY-MM-DD").format("DD/MM/YYYY");
                $('#dosen_id').val(dosen.dosen_id);
                $('#input_nama').val(dosen.nama);
                $('#input_nidn').val(dosen.nidn);
                $('#input_nip').val(dosen.nip);
                $('#input_gender').val(dosen.gender);
                $('#input_agama').val(dosen.agama);
                $('#input_dob').val(dob);
                $('#input_status').val(dosen.status);


                $('#btn-save').val("update");

                $('#myModal').modal('show');


            })
        });

        $('body').on('shown.bs.modal', '#myModal', function () {
            $('input:visible:enabled:first', this).focus();
        })
        $('#sel_semester').on('change', function(e){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();
            var head = '<thead><tr role="row" class="odd"><th>No</th><th>Kode MK</th><th>Nama MK</th><th>SKS</th><th>Nilai</th><th class="nosort"align="center"></th></tr></thead>';
            $('#data_list').empty();
            var formData = {
                krsId: $('#sel_semester').val(),
            }

            console.log(formData);
            $.ajax({

                type: "POST",
                url: 'khs/search/semester',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    var str_html = head;
                    if (data.length == 0) {
                        str_html += '<tr id="data"><td></td><td></td><td></td><td></td><td></td><td></td></tr></td>';


                    }
                    $.each(data.datas, function(i,item) {
                        str_html += '<tr id="' + item.krs_detail_id + '"><td>'+(i+1)+'</td><td>' + item.kode_mk + '</td><td>' + item.nama_mk + '</td><td>' + item.sks + '</td><td>' + item.nilai + '</td>';
                        str_html += '<td><div class="col-sm-1"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' + item.krs_detail_id + '"><span class="glyphicon glyphicon-pencil"></span></button></div></td>';
                        str_html += '</div></tr>';



                    })
                    $('#data_list').append(str_html);
                    $('#txtNama').text(data.krsInfo.nama);
                    $('#txtNIM').text(data.krsInfo.nim);
                    $('#txtSKS').text(data.krsInfo.allowed_sks);
                    $('#txtIP').text(data.krsInfo.ip_lalu);
                    krsId = data.krsInfo.krs_id;

                },
                error: function (data) {
                    console.log('Error:', data);
                    console.log(formData);
                }
            });
        });

        $('#btn-print').on('click', function () {

            url = 'print-single/' + krsId;
            var win = window.open(url, '_blank');
            win.focus();
        })

    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
