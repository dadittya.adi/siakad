<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" class="cssdeck">

    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset("/components/iCheck/skins/flat/green.css")}}" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('admin/header')

<!-- Sidebar -->
@include('admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Penerimaan Mahasiswa" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box">
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#biodata" data-toggle="tab">Data Diri</a></li>
                        <li><a href="#parent" data-toggle="tab">Data Orang Tua</a></li>
                        <li><a href="#education" data-toggle="tab">Pendidikan</a></li>
                    </ul>
                    <form id="frmInput" method="POST" name="frmInput" class="form-horizontal" novalidate="">
                    <div class="tab-content">

                            <div class="tab-pane active in" id="biodata">
                                <div class="form-group">
                                </div>
                                <div class="form-group">
                                    <label for="input_mhsNim" class="col-md-2 control-label">NIM  <b class="text-red">*</b></label>

                                    <div class="col-md-2">
                                        <input id="input_mhsNim" type="text" class="form-control" name="input_mhsNim" value={{ $newNIM }} required autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 control-label" >Program Studi</label>
                                    <div class="col-md-8">
                                        @foreach ($arrProdi as $prodi)
                                            <label class="radio-inline">
                                                <input type="radio"
                                                @if ($mhs->prodiId == $prodi->prodi_id) {{ 'checked' }}
                                                @endif
                                                class="minimal" name="radio_prodi" id={{'radio'.$prodi->prodi_name}} value={{$prodi->prodi_id}}>
                                                <label for={{'radio'.$prodi->prodi_name}}>&nbsp;&nbsp;{{$prodi->prodi_name}}</label>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group">
                                <label for="input_dtlNama" class="col-md-2 control-label">Nama</label>
                                <div class="col-md-8">
                                    <input id="input_dtlNama" type="text" class="form-control" name="input_dtlNama" value="{{ $mhs->nama }}" required autofocus>
                                </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_mhsTahun" class="col-md-2 control-label">Tahun Masuk <b class="text-red">*</b></label>

                                    <div class="col-md-2">
                                        <input id="input_mhsTahun" type="text" class="form-control" name="input_mhsTahun" readonly value="{{  $year }}">
                                    </div>
                                </div>

                                <div class="form-group">

                                </div>


                                <div class="form-group">
                                    <label for="input_dtlKktp" class="col-md-2 control-label">No. KTP</label>

                                    <div class="col-md-2">
                                        <input id="input_dtlKktp" type="text" class="form-control" name="input_dtlKktp" value="{{ $mhs->ktp }}" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_dob" class="col-md-2 control-label">Tanggal Lahir</label>
                                    <div class="col-md-2">
                                        <input id="input_tempatLahir" type="text" class="form-control" value={{ $mhs->tempatLahir }} name="input_tempatLahir" placeholder="Tempat Lahir">
                                    </div>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input id="input_dob" type="text" class="form-control" name="input_dob" value={{ $mhs->dob }} data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label class="col-md-2 control-label" > Jenis Kelamin</label>
                                    <div class="col-md-8">
                                        <label class="radio-inline">
                                            <input class="minimal" type="radio"
                                                   @if ($mhs->gender == 'L') {{ 'checked' }}
                                                   @endif
                                                   name="radio_gender" id="radioGenderL" value="L">
                                            <label for="radioGenderL">&nbsp;Laki-Laki</label>
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="minimal"
                                                   @if ($mhs->gender == 'P')  {{ 'checked' }}
                                                   @endif
                                                   name="radio_gender" id="radioGenderP" value="P">
                                            <label for="radioGenderP">&nbsp;Perempuan</label>
                                        </label>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Agama</label>
                                    <div class="col-md-8">
                                        @foreach ($arrAgama as $agama)
                                            <label class="radio-inline ">
                                                <input type="radio" class="minimal"
                                                @if ($mhs->agamaId == $agama->agama_id) {{ 'checked' }}
                                                @endif
                                                name="radio_agama" id={{'radio'.$agama->agama_nama}} value={{$agama->agama_id}}>
                                                <label for={{'radio'.$agama->agama_nama}}>{{$agama->agama_nama}}</label>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_dtlAlamat" class="col-md-2 control-label">Alamat</label>

                                    <div class="col-md-8">
                                        <input id="input_dtlAlamat" type="text" class="form-control" name="input_dtlAlamat" value="{{ $mhs->alamat }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label">Ds/Kelurahan</label>
                                        <input id="input_dtlKelurahan" type="text" class="form-control" name="input_dtlKkelurahan" value="{{ $mhs->kelurahan }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label">Kecamatan</label>
                                        <input id="input_dtlKecamatan" type="text" class="form-control" name="input_dtlKecamatan" value="{{ $mhs->kecamatan }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">Kota/Kab.</label>
                                        <input id="input_dtlKota" type="text" class="form-control" name="input_dtlKota" value="{{ $mhs->kota }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label">Propinsi</label>
                                        <input id="input_dtlPropinsi" type="text" class="form-control" name="input_dtlPropinsi" value="{{ $mhs->propinsi }}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">Kode Pos</label>
                                        <input id="input_dtlKodePos" type="text" class="form-control" name="input_dtlKodePos" value="{{ $mhs->kodePos }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Telepon Rumah / Handphone</label>
                                    <div class="col-md-2">
                                        <input id="input_dtlFixedPhone" type="text" class="form-control" name="input_dtlFixedPhone" placeholder="Telepon Rumah" value="{{ $mhs->fixedPhone }}">
                                    </div>
                                    <div class="col-md-2">
                                        <input id="input_dtlMobilePhone" type="text" class="form-control" name="input_dtlMobilePhone" placeholder="Handphone" value="{{ $mhs->mobilePhone }}">
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="parent">
                                <div class="form-group">
                                </div>
                                <div class="form-group">
                                    <label for="input_namaAyah" class="col-md-2 control-label">Nama Ayah</label>

                                    <div class="col-md-8">
                                        <input id="input_namaAyah" type="text" class="form-control" name="input_namaAyah" value="{{ $mhs->namaAyah }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_namaIbu" class="col-md-2 control-label">Nama Ibu</label>

                                    <div class="col-md-8">
                                        <input id="input_namaIbu" type="text" class="form-control" name="input_namaIbu" value="{{ $mhs->namaIbu }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" >Pekerjaan Ayah</label>
                                    <div class="col-md-9">
                                        @foreach ($arrPekerjaan as $pekerjaan)
                                            <label class="radio-inline">
                                                <input type="radio" class="minimal" name="radio_pekerjaan"
                                                @if ($mhs->pekerjaanAyahId == $pekerjaan->pekerjaan_id) {{ 'checked' }}
                                                @endif
                                                id={{'radio'.$pekerjaan->pekerjaan_nama}} value={{$pekerjaan->pekerjaan_id}}>
                                                <label for={{'radio'.$pekerjaan->pekerjaan_nama}}>&nbsp;{{$pekerjaan->pekerjaan_nama}}</label>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_instansiAyan" class="col-md-2 control-label">Nama Instansi</label>

                                    <div class="col-md-4">
                                        <input id="input_instansiAyah" type="text" class="form-control" name="input_instansiAyah" value="{{ $mhs->InstansiAyah }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" >Pekerjaan ibu</label>
                                    <div class="col-md-9">
                                        @foreach ($arrPekerjaan as $pekerjaan)
                                            <label class="radio-inline">
                                                <input type="radio" class="minimal" name="radio_pekerjaan"
                                                       @if ($mhs->pekerjaanIbuId == $pekerjaan->pekerjaan_id) {{ 'checked' }}
                                                       @endif
                                                       id={{'radio'.$pekerjaan->pekerjaan_nama}} value={{$pekerjaan->pekerjaan_id}}>
                                                <label for={{'radio'.$pekerjaan->pekerjaan_nama}}>&nbsp;{{$pekerjaan->pekerjaan_nama}}</label>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_instansiIbu" class="col-md-2 control-label">Nama Instansi</label>

                                    <div class="col-md-4">
                                        <input id="input_instansiIbu" type="text" class="form-control" name="input_instansiIbu"  value="{{ $mhs->InstansiAyah }}">
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="education">
                                <div class="form-group">
                                </div>
                                <div class="form-group">
                                    <label for="input_asalSekolah" class="col-md-2 control-label">Asal Sekolah / Tahun Lulus</label>

                                    <div class="col-md-6">
                                        <input id="input_asalSekolah" type="text" class="form-control" name="input_asalSekolah" placeholder="Asal Sekolah">
                                    </div>
                                    <div class="col-md-2">
                                        <input id="input_TahunLulus" type="text" class="form-control" name="input_TahunLulus" placeholder="Tahun Lulus">
                                    </div>
                                </div>
                            </div>

                    </div>
                    </form>
                        </div>
                </div>
                </div>
            </div>
            <div class="box">
                <div class="box-body">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-danger btn-block" id="btn-cancel" value="cancel">Batal</button>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-block" id="btn-save" value="add">Simpan</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
    <!-- /.box -->>
<!-- Modal -->
<div class="modal fade" id="dlgConfirm" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Data berhasil disimpan, tambahkan data lagi?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    <!-- Footer -->
    @include('footer')

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>
<!-- iCheck    -->
<script src="{{ asset ("/components/iCheck/iCheck.js") }}"></script>

<script>

    $(document).ready(function(){

        var inputAgama;
        var inputGender;
        var inputPekerjaan;
        var inputProdi;
        var inputInfSource;



        //Datemask dd/mm/yyyy
        $('#input_dob').inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('input[name="radio_gender"]').on('ifClicked', function (event) {
            inputGender = this.value;
        });

        $('input[name="radio_agama"]').on('ifClicked', function (event) {
            inputAgama = this.value;
        });

        $('input[name="radio_pekerjaan"]').on('ifClicked', function (event) {
            inputPekerjaan = this.value;
        });

        $('input[name="radio_pekerjaan"]').on('ifClicked', function (event) {
            inputPekerjaan = this.value;
        });

        $('input[name="radio_prodi"]').on('ifClicked', function (event) {
            inputProdi = this.value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            event.preventDefault();

            var formData = {
                prodiId: inputProdi,
                tahun: $('#input_tahun').val()
            }

            console.log(formData);

            $.ajax({

                type: "POST",
                url: 'grabRegNumber/',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $('#input_noPendaftaran').val(data.no_pendaftaran);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('input[name="radio_sumberInf"]').on('ifClicked', function (event) {
            inputInfSource = this.value;
        });

        //create new user / update existing user
        $("#btn-save").click(function (e) {
            var my_url = "pendaftaran";

            var state = $('#btn-save').val();
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += "/edit";
            }
            else {
                var type = "POST"; //for creating new resource

                my_url += "/store";
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();


            var dob = moment($('#input_dob').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var formData = {
                noPendaftaran: $('#input_noPendaftaran').val(),
                tahun: $('#input_tahun').val(),
                nama: $('#input_nama').val(),
                ktp: $('#input_ktp').val(),
                tempatLahir: $('#input_tampatLahir').val(),
                dob: dob,
                gender: inputGender,
                agama: inputAgama,
                alamat: $('#input_alamat').val(),
                kelurahan: $('#input_kelurahan').val(),
                kecamatan: $('#input_kecamatan').val(),
                kota: $('#input_kota').val(),
                propinsi: $('#input_propinsi').val(),
                kodePos: $('#input_kodePos').val(),
                fixedPhone: $('#input_fixedPhone').val(),
                mobilePhone: $('#input_mobilePhone').val(),
                asalSekolah: $('#input_asalSekolah').val(),
                tahunLulus: $('#input_TahunLulus').val(),
                namaAyah: $('#input_namaAyah').val(),
                namaIbu: $('#input_namaIbu').val(),
                pekerjaanAyah: inputPekerjaan,
                instansi: $('#input_instansi').val(),
                prodi: inputProdi,
                infSource:inputInfSource
            }

            //used to determine the http verb to use [add=POST], [update=PUT]

            console.log(formData);
            console.log(my_url);
            console.log(type);

            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    window.location.href = "/admin/pendaftaran/";

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
