<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body onload="window.print();">
<div class="wrapper">
    <!-- Main content -->
    <section style="height: 13.5cm">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-1" style="width: 12.499999995%">
                <img style="float: left;width: 52px;height: 52px; " src="{{ asset("/img/logo.png") }} "/>
            </div>
            <div class="col-xs-6">
                <b>Akademi Kesejahteraan Sosial "Ibu Kartini" Semarang</b>
                <br><b>Jl. Sultan Agung 77 Candibaru Semarang 50232</b>
                <br><b>Telp/Fax : (024)8315304</b></p>
            </div>
            <div class="col-xs-3 pull-right">
                <b>KARTU HASIL STUDI (KHS}</b>
                <br><b>Program Studi : {{$krsInfo->nama_prodi}} </b>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row">

                    <div class="col-xs-1 control-label" style="width: 16.499999995%" > Nama Mahasiswa</div>
                    <div id="txtNama" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->nama}}</b></div>
        </div>
        <div class="row">
            <div class="col-xs-1 control-label" style="width: 16.499999995%" > NIM </div>
            <div id="txtNIM" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->nim}}</b></div>
        </div>
        <div class="row">
            <div class="col-xs-1 control-label" style="width: 16.499999995%" > Semester </div>
            <div id="txtSemester" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->semester}}</b></div>
            <div class="col-xs-1 control-label" style="width: 16.499999995%" > Tahun Akademik </div>
            <div id="txtSemester" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->tahun_akademik}}</b></div>
            <div class="col-xs-3 control-label"  > <b>Semarang, {{$tanggal}}</b> </div>
        </div>
        <div class="row">
            <div class="col-xs-1 control-label" style="width: 22.499999995%"> Beban SKS yang diijinkan</div>
            <div id="txtSKS" class="col-xs-1 control-label" style="width: 10.499999995%"> : &emsp;<b>{{$krsInfo->allowed_sks}}</b></div>
            <div class="col-xs-1 control-label" style="width: 16.499999995%" > IP Semester Lalu </div>
            <div id="txtIP" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->ip_lalu}}</b></div>
            <div class="col-xs-1 control-label" style="width: 13.499999995%" > <b>Mahasiswa</b> </div>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row" style="height: 7.5cm">
            <div class="col-xs-12 table-responsive" style="width: 12.5cm; height: 7.5cm">
                <table cellpadding="20px">
                    <thead>
                    <tr>
                        <th style="width: 5%">No</th>
                        <th style="width: 20%">Kode MK</th>
                        <th style="width: 50%; text-align: center" >Mata Kuliah</th>
                        <th style="width: 5%; text-align: center">SKS</th>
                        <th style="width: 10%; text-align: center">Nilai</th>
                        <th style="width: 10%; text-align: center">SKS x N</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php ($counter = 1)
                    @php ($sksxn = 0)
                    @foreach($arrKrsDetail as $krsDetail)
                    <tr>

                            <td>{{$counter}}</td>
                            <td><small>{{ $krsDetail->kode_mk }}</small></td>
                            <td><small>{{ $krsDetail->nama_mk }}</small></td>
                            <td style="text-align: center"><small>{{ $krsDetail->sks }}</small></td>
                            <td style="text-align: center"><small>{{ $krsDetail->nilai }}</small></td>
                            <td style="text-align: center"><small>{{ $krsDetail->sksxn }}</small></td>
                            @php ($counter += 1)
                            @php ($sksxn += floatval($krsDetail->sksxn))

                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-xs-1" style="width: 0.9cm; height: 7.5cm">
            </div>
            <div>
                <br><br>
                <b>({{$krsInfo->nama}}</b><br>
                _____________________________ <br>
                <b>Disetujui oleh</b>
                <b>Dosen Wali</b><br><br><br><br>
                <b>({{$krsInfo->nama_wali}})</b><br>
                _____________________________ <br>
                <b>Kaprodi</b><br><br><br><br>
                <b>({{$krsInfo->kaprodi}})</b><br>
                _____________________________ <br>

            </div>
        </div>

        <div class="row" style="height: 1cm">
            <div class="col-xs-12 table-responsive" style="width: 12.5cm; height: 1cm">
                <table cellpadding="20px">
                    <tr>
                        <td style="width: 5%"></td>
                        <td style="width: 20%"></td>
                        <td style="width: 50%; text-align: right" ><b>Jumlah Kredit Semester : </b></td>
                        <td style="width: 5%; text-align: right"><b>{{ $krsInfo->total_sks }}</b></td>
                        <td style="width: 10%; text-align: right"><b>SKS</b></td>
                        <td style="width: 10%; text-align: center"><b>{{ $sksxn }}</b></td>
                    </tr>
                    <tr>
                        <td style="width: 5%"></td>
                        <td style="width: 20%"></td>
                        <td style="width: 50%; text-align: right" ><b>Indeks Prestasi Semester : </b></td>
                        <td style="width: 5%; text-align: right"><b>{{ $krsInfo->ip_semester }}</b></td>
                        <td style="width: 10%; text-align: right"></td>
                        <td style="width: 10%; text-align: center"></td>

                    </tr>
                </table>
            </div>
            <div class="col-xs-1" style="width: 0.9cm;  height: 1cm">
            </div>
            <div >
                <div style=" height: 1cm">
                    <table >
                        <tr>
                            <td style="width: 73%"><b>Jumlah Kredit Semester</b></td>
                            <td style="width: 2%"><b>:</b></td>
                            <td style="width: 25%"><b>{{ $krsInfo->sks_transkrip }} sks</b></td>
                        </tr>
                        <tr>
                            <td style="width: 73%"><b>IP Komulatif</b></td>
                            <td style="width: 2%"><b>:</b></td>
                            <td style="width: 25%"><b>{{ $krsInfo->ipk }} sks</b></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-1" style="width:40%;">
                Dicetak pada : {{ $tanggal. ', ' .date('G:i', time())}}
            </div>
            <div class="col-xs-3 pull-right" style="width: 60%;">
                <b>Catatan : Nilai T tidak diurus dalam 3 bulan dianggap E</b>
            </div>
        <!-- /.row -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <section style="height: 12.5cm">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-1" style="width: 12.499999995%">
                <img style="float: left;width: 52px;height: 52px; " src="{{ asset("/img/logo.png") }} "/>
            </div>
            <div class="col-xs-6">
                <b>Akademi Kesejahteraan Sosial "Ibu Kartini" Semarang</b>
                <br><b>Jl. Sultan Agung 77 Candibaru Semarang 50232</b>
                <br><b>Telp/Fax : (024)8315304</b></p>
            </div>
            <div class="col-xs-3 pull-right">
                <b>KARTU HASIL STUDI (KHS}</b>
                <br><b>Program Studi : {{$krsInfo->nama_prodi}} </b>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row">

            <div class="col-xs-1 control-label" style="width: 16.499999995%" > Nama Mahasiswa</div>
            <div id="txtNama" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->nama}}</b></div>
        </div>
        <div class="row">
            <div class="col-xs-1 control-label" style="width: 16.499999995%" > NIM </div>
            <div id="txtNIM" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->nim}}</b></div>
        </div>
        <div class="row">
            <div class="col-xs-1 control-label" style="width: 16.499999995%" > Semester </div>
            <div id="txtSemester" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->semester}}</b></div>
            <div class="col-xs-1 control-label" style="width: 16.499999995%" > Tahun Akademik </div>
            <div id="txtSemester" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->tahun_akademik}}</b></div>
            <div class="col-xs-3 control-label"  > <b>Semarang, {{$tanggal}}</b> </div>
        </div>
        <div class="row">
            <div class="col-xs-1 control-label" style="width: 22.499999995%"> Beban SKS yang diijinkan</div>
            <div id="txtSKS" class="col-xs-1 control-label" style="width: 10.499999995%"> : &emsp;<b>{{$krsInfo->allowed_sks}}</b></div>
            <div class="col-xs-1 control-label" style="width: 16.499999995%" > IP Semester Lalu </div>
            <div id="txtIP" class="col-xs-2 control-label"> : &emsp;<b>{{$krsInfo->ip_lalu}}</b></div>
            <div class="col-xs-1 control-label" style="width: 13.499999995%" > <b>Mahasiswa</b> </div>
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row" style="height: 7.5cm">
            <div class="col-xs-12 table-responsive" style="width: 12.5cm; height: 7.5cm">
                <table cellpadding="20px">
                    <thead>
                    <tr>
                        <th style="width: 5%">No</th>
                        <th style="width: 20%">Kode MK</th>
                        <th style="width: 50%; text-align: center" >Mata Kuliah</th>
                        <th style="width: 5%; text-align: center">SKS</th>
                        <th style="width: 10%; text-align: center">Nilai</th>
                        <th style="width: 10%; text-align: center">SKS x N</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php ($counter = 1)
                    @php ($sksxn = 0)
                    @foreach($arrKrsDetail as $krsDetail)
                        <tr>

                            <td>{{$counter}}</td>
                            <td><small>{{ $krsDetail->kode_mk }}</small></td>
                            <td><small>{{ $krsDetail->nama_mk }}</small></td>
                            <td style="text-align: center"><small>{{ $krsDetail->sks }}</small></td>
                            <td style="text-align: center"><small>{{ $krsDetail->nilai }}</small></td>
                            <td style="text-align: center"><small>{{ $krsDetail->sksxn }}</small></td>
                            @php ($counter += 1)
                            @php ($sksxn += floatval($krsDetail->sksxn))

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-xs-1" style="width: 0.9cm; height: 7.5cm">
            </div>
            <div>
                <br><br>
                <b>({{$krsInfo->nama}}</b><br>
                _____________________________ <br>
                <b>Disetujui oleh</b>
                <b>Dosen Wali</b><br><br><br><br>
                <b>({{$krsInfo->nama_wali}})</b><br>
                _____________________________ <br>
                <b>Kaprodi</b><br><br><br><br>
                <b>({{$krsInfo->kaprodi}})</b><br>
                _____________________________ <br>

            </div>
        </div>

        <div class="row" style="height: 1cm">
            <div class="col-xs-12 table-responsive" style="width: 12.5cm; height: 1cm">
                <table cellpadding="20px">
                    <tr>
                        <td style="width: 5%"></td>
                        <td style="width: 20%"></td>
                        <td style="width: 50%; text-align: right" ><b>Jumlah Kredit Semester : </b></td>
                        <td style="width: 5%; text-align: right"><b>{{ $krsInfo->total_sks }}</b></td>
                        <td style="width: 10%; text-align: right"><b>SKS</b></td>
                        <td style="width: 10%; text-align: center"><b>{{ $sksxn }}</b></td>
                    </tr>
                    <tr>
                        <td style="width: 5%"></td>
                        <td style="width: 20%"></td>
                        <td style="width: 50%; text-align: right" ><b>Indeks Prestasi Semester : </b></td>
                        <td style="width: 5%; text-align: right"><b>{{ $krsInfo->ip_semester }}</b></td>
                        <td style="width: 10%; text-align: right"></td>
                        <td style="width: 10%; text-align: center"></td>

                    </tr>
                </table>
            </div>
            <div class="col-xs-1" style="width: 0.9cm;  height: 1cm">
            </div>
            <div >
                <div style=" height: 1cm">
                    <table >
                        <tr>
                            <td style="width: 73%"><b>Jumlah Kredit Semester</b></td>
                            <td style="width: 2%"><b>:</b></td>
                            <td style="width: 25%"><b>{{ $krsInfo->sks_transkrip }} sks</b></td>
                        </tr>
                        <tr>
                            <td style="width: 73%"><b>IP Komulatif</b></td>
                            <td style="width: 2%"><b>:</b></td>
                            <td style="width: 25%"><b>{{ $krsInfo->ipk }} sks</b></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-1" style="width:40%;">
                Dicetak pada : {{ $tanggal. ', ' .date('h:i', time())}}
            </div>
            <div class="col-xs-3 pull-right" style="width: 60%;">
                <b>Catatan : Nilai T tidak diurus dalam 3 bulan dianggap E</b>
            </div>
            <!-- /.row -->
        </div>
            <!-- /.row -->
    </section>
</div>
<!-- ./wrapper -->
</body>
</html>

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>


<script>
    $(function () {
        var t = $('#data_list').DataTable({
            'aoColumnDefs': [
                { targets: [2], searchable: true, orderable:false},
                { targets: '_all', searchable:false, orderable:false}

            ],
            "bAutoWidth": false, // Disable the auto width calculation
            "aoColumns": [
                { "sWidth": "2%" }, // 1st column width
                { "sWidth": "5%" }, // 2nd column width
                { "sWidth": "38%" },
                { "sWidth": "2%" },
                { "sWidth": "10%" },
                { "sWidth": "5%" }
            ],
            "iDisplayLength": 50
        });

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
<script>
    $(document).ready(function(){

        //Datemask dd/mm/yyyy
        $('#input_dob').inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        var url = "dosen";

        //display modal form for user editing
        $('.open-modal').click(function(){
            var dosen_id = $(this).val();
            $('#frmInput').trigger("reset");
            $.get("dosen/" + dosen_id, function (dosen) {
                //success data
                console.log(dosen);
                var dob = moment($('#input_dob').val(), "YYYY-MM-DD").format("DD/MM/YYYY");
                $('#dosen_id').val(dosen.dosen_id);
                $('#input_nama').val(dosen.nama);
                $('#input_nidn').val(dosen.nidn);
                $('#input_nip').val(dosen.nip);
                $('#input_gender').val(dosen.gender);
                $('#input_agama').val(dosen.agama);
                $('#input_dob').val(dob);
                $('#input_status').val(dosen.status);


                $('#btn-save').val("update");

                $('#myModal').modal('show');


            })
        });

        $('body').on('shown.bs.modal', '#myModal', function () {
            $('input:visible:enabled:first', this).focus();
        })
        $('#sel_semester').on('change', function(e){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();
            var head = '<thead><tr role="row" class="odd"><th>No</th><th>Kode MK</th><th>Nama MK</th><th>SKS</th><th>Nilai</th><th class="nosort"align="center"></th></tr></thead>';
            $('#data_list').empty();
            var formData = {
                mhsId: $('#mahasiswa_id').val(),
                semester: $('#sel_semester').val(),
            }

            console.log(formData);
            $.ajax({

                type: "POST",
                url: 'khs/search/semester',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    var str_html = head;
                    if (data.length == 0) {
                        str_html += '<tr id="data"><td></td><td></td><td></td><td></td><td></td><td></td></tr></td>';


                    }
                    $.each(data.datas, function(i,item) {
                        str_html += '<tr id="' + item.krs_detail_id + '"><td>'+(i+1)+'</td><td>' + item.kode_mk + '</td><td>' + item.nama_mk + '</td><td>' + item.sks + '</td><td>' + item.nilai + '</td>';
                        str_html += '<td><div class="col-sm-1"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' + item.krs_detail_id + '"><span class="glyphicon glyphicon-pencil"></span></button></div></td>';
                        str_html += '</div></tr>';



                    })
                    $('#data_list').append(str_html);
                    $('#txtNama').text(data.krsInfo.nama);
                    $('#txtNIM').text(data.krsInfo.nim);
                    $('#txtSKS').text(data.krsInfo.sks);
                    $('#txtIP').text(data.krsInfo.ip);

                },
                error: function (data) {
                    console.log('Error:', data);
                    console.log(formData);
                }
            });
        });

        $("#btn_search").click(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var formData = {
                prodiId: $('#sel_status').val(),
                gender: $('#sel_gender').val(),
                agama: $('#sel_agama').val()
            };

            $('#data_list').empty();
            $.ajax({

                type: "POST",
                url: 'dosen/search',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    if (data.length == 0) {
                        var str_html = '<tr id="data"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><div class="col-sm-4"</div><div class="col-sm-4"></div></tr></td>';

                        $('#data_list').append(str_html);
                    }
                    $.each(data, function(i,item) {
                        var str_html = '<tr id="data' + item.dosen_id + '"><td></td><td>' + item.nama + '</td><td>' + item.nidn + '</td><td>' + item.nip + '</td><td>' + item.gender + '</td><td>' + item.agama + '</td><td>' + item.dob + '</td><td>' + item.status + '</td>';
                        str_html += '<td><div class="col-sm-4"><button class="btn btn-warning btn-block btn-detail open-modal" value="' + item.dosen_id + '"><span class="glyphicon glyphicon-pencil"></span> Edit</button></div></td>';
                        str_html += '</tr>';

                        $('#data_list').append(str_html);

                    })
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });


        });

    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
