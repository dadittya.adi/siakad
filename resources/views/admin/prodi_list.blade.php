<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />

    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('admin/header')

<!-- Sidebar -->
@include('admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Program Studi" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box">
                <div class="box-body">
                    <table id= "data_list" class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr role="row" class="odd">
                            <th>Kode Prodi</th>
                            <th>Program Studi</th>
                            <th>Ketua Prodi</th>
                            <th class="nosort"align="center"></th>
                        </tr>
                        </thead>
                        <tbody id="data_list">
                        @foreach ($datas as $data)

                            <tr id="data{{$data->prodi_id}}">
                                <td>{{$data->kode}}</td>
                                <td>{{$data->nama}}</td>
                                <td>{{$data->kaprodi}}</td>
                                <td>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary btn-xs btn-detail open-modal" value="{{$data->prodi_id}}">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmInput" name="frmInput" class="form-horizontal" novalidate="">

                                <div class="form-group{{ $errors->has('input_nama') ? ' has-error' : '' }}">
                                    <label for="input_kode" class="col-md-4 control-label">Kode Prodi</label>

                                    <div class="col-md-7">
                                        <input id="input_kode" type="text" class="form-control" name="input_kode" value="{{ old('input_kode') }}" required autofocus>

                                        @if ($errors->has('input_kode'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('input_kode') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('input_nama') ? ' has-error' : '' }}">
                                    <label for="input_nama" class="col-md-4 control-label">Program Studi</label>

                                    <div class="col-md-7">
                                        <input id="input_nama" type="text" class="form-control" name="input_nama" value="{{ old('input_nama') }}" required autofocus>

                                        @if ($errors->has('input_nama'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('input_nama') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="sel_kaprodi" class="col-md-4 control-label">Ketua Prodi</label>

                                    <div class="col-md-7">
                                        <select class="form-control col-md-1 kaprodi" style="width: 100%;" id="sel_kaprodi" name="sel_kaprodi">
                                            @foreach($dosens as $dosen)
                                                <option value={{$dosen->id}}>{{$dosen->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('input_kodeNim') ? ' has-error' : '' }}">
                                    <label for="input_kodeNim" class="col-md-4 control-label">Kode NIM</label>

                                    <div class="col-md-7">
                                        <input id="input_kodeNim" type="text" class="form-control" name="input_kodeNim" value="{{ old('input_kodeNim') }}" required autofocus>

                                        @if ($errors->has('input_kodeNim'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('input_kodeNim') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_kodeIjazah" class="col-md-4 control-label">Kode Ijazah</label>

                                    <div class="col-md-7">
                                        <input id="input_kodeIjazah" type="text" class="form-control" name="input_kodeIjazah" value="{{ old('input_kodeIjazah') }}" required autofocus>

                                        @if ($errors->has('input_kodeIjazah'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('input_kodeIjazah') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="input_sk" class="col-md-4 control-label">SK Prodi</label>

                                    <div class="col-md-7">
                                        <textarea id="input_sk" type="text" class="form-control" name="input_sk"  required autofocus></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-danger btn-block" id="btn-cancel" value="cancel">Batal</button>
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-success btn-block" id="btn-save" value="add">Simpan</button>
                                </div>
                                <div class="col-sm-3">
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="prodi_id" name="prodi_id" value="0">

                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('footer')
</div><!-- ./wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<!-- jQuery 2.2.3 -->
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- Select 2 -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/select2/select2.full.min.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>


<script>
    $(function () {
        var t = $('#data_list').DataTable({
            'aoColumnDefs': [
                { targets: [2], searchable: true, orderable:false},
                { targets: '_all', searchable:false, orderable:false}

            ],
            "bAutoWidth": false, // Disable the auto width calculation
            "aoColumns": [
                { "sWidth": "5%" }, // 2nd column width
                { "sWidth": "30%" },
                { "sWidth": "60%" },
                { "sWidth": "5%" }
            ],
            "iDisplayLength": 10
        });
    });
</script>
<script>
    $(document).ready(function(){

        $('.kaprodi').select2();

        //Datemask dd/mm/yyyy
        $('#input_dob').inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        var url = "dosen";

        //display modal form for user editing
        $('.open-modal').click(function(){
            var prodi_id = $(this).val();
            $('#frmInput').trigger("reset");

            $.get("program-studi/" + prodi_id, function (prodi) {
                //success data
                console.log(prodi);
                $('#prodi_id').val(prodi.prodi_id);
                $('#input_kode').val(prodi.kode);
                $('#input_nama').val(prodi.nama);
                $("#sel_kaprodi").val(prodi.kaprodi).change();
                $('#input_kodeNim').val(prodi.kodeNim);
                $('#input_kodeIjazah').val(prodi.kodeIjazah);
                $('#input_sk').val(prodi.sk);

                $('#btn-save').val("update");

                $('#myModal').modal('show');


            })
        });

        $('body').on('shown.bs.modal', '#myModal', function () {
            $('input:visible:enabled:first', this).focus();
        })

        $('#btn-cancel').click(function(){


            $('#frmInput').trigger("reset");

            $('#myModal').modal('hide')
        });

        //create new user / update existing user
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var prodi_id = $('#prodi_id').val();
            var formData = {
                id:$('#prodi_id').val(),
                nama:$('#input_nama').val(),
                kode:$('#input_kode').val(),
                kaprodi:$('#sel_kaprodi').val(),
                kodeNim:$('#input_kodeNim').val(),
                kodeIjazah:$('#input_kodeIjazah').val(),
                sk:$('#input_sk').val()
        }

            //used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();
            var my_url = "program-studi";

            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += "/edit";
            }
            else {
                var type = "POST"; //for creating new resource
                var dosen_id = $('#dosen_id').val();

                my_url += "/add";
            }


            console.log(formData);
            console.log(my_url);
            console.log(type);

            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    if (state == "add"){ //if user added a new record
                        var lastrow = $('data_listt').data.count();

                        var str_html = '<tr id="data' + data.prodi_id + '"><td>' + data.kode + '</td><td>' + data.nama + '</td><td>' + data.kaprodi + '</td>';
                        str_html += '<td><div class="col-sm-1"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' +data.prodi_id + '"><span class="glyphicon glyphicon-pencil"></span></button></div></td></tr>';

                        $('#data_list').append(str_html);

                    }else{ //if user updated an existing record
                        var str_html = '<tr id="data' + data.prodi_id + '"><td>' + data.kode + '</td><td>' + data.nama + '</td><td>' + data.kaprodi + '</td>';
                        str_html += '<td><div class="col-sm-1"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' +data.prodi_id + '"><span class="glyphicon glyphicon-pencil"></span></button></div></td></tr>';

                        $("#data" + data.prodi_id).replaceWith(str_html);

                    }

                    //$('#frminput').trigger("reset");

                    $('#myModal').modal('hide')
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#btn_search").click(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var formData = {
                status: $('#sel_status').val(),
                gender: $('#sel_gender').val(),
                agama: $('#sel_agama').val()
            };

            $('#data_list').empty();
            $.ajax({

                type: "POST",
                url: 'dosen/search',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    if (data.length == 0) {
                        var str_html = '<tr id="data"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><div class="col-sm-4"</div><div class="col-sm-4"></div></tr></td>';

                        $('#data_list').append(str_html);
                    }
                    $.each(data, function(i,item) {
                        var str_html = '<tr id="data' + item.prodi_id + '"><td>' + item.kode + '</td><td>' + item.nama + '</td><td>' + item.kaprodi + '</td>';
                        str_html += '<td><div class="col-sm-1"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' +item.prodi_id + '"><span class="glyphicon glyphicon-pencil"></span></button></div></td></tr>';

                        $('#data_list').append(str_html);

                    })
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });


        });

    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
