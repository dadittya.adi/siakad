<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('admin/header')

<!-- Sidebar -->
@include('admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Pendaftaran Calon Mahasiswa" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <!-- form start -->
                            <form class="form-horizontal">

                                <div class="form-group row">

                                    <label for="sel_prodi" class="col-md-2 control-label" > Program Studi</label>
                                    <div class="col-md-2">
                                        <select class="form-control col-md-1 select2" id="sel_prodi" name="sel_prodi">
                                            @foreach ($arrProdi as $prodi)
                                            <option value={{$prodi->prodi_id}}>{{$prodi->prodi_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">

                                    <label for="sel_tahun" class="col-md-2 control-label" > Tahun Akademik</label>
                                    <div class="col-md-2">
                                        <select class="form-control col-md-1 select2" id="sel_tahun" name="sel_tahun">
                                            @foreach($years as $year)
                                                <option
                                                        @if($year->id == $latestYear) {{'selected'}} @endif
                                                        value={{$year->id}}>{{$year->year_akademik}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                    <button class="btn btn-info" id="btn_search">
                                        <span class="glyphicon glyphicon-search"></span> Search
                                    </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-8">
                                </div>
                                <div class="col-sm-2"><button type="submit" class="btn btn-success btn-block btn-flat pull-right" id="btn-add" name="btn-add">
                                        <span class="glyphicon glyphicon-arrow-down"></span> Download</button>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" onclick="window.location.href='pendaftaran/add';" class="btn btn-block btn-success btn-flat pull-right" id="btn-add" name="btn-add">
                                        <span class="glyphicon glyphicon-plus"></span> Tambah</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-footer -->
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <table id= "data_list" class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr role="row" class="odd">
                            <th>No</th>
                            <th>No. Pendaftaran</th>
                            <th>Nama</th>
                            <th>L/P</th>
                            <th>Agama</th>
                            <th class="nosort"align="center">Tanggal Lahir</th>
                            <th>Program Studi</th>
                            <th>Status</th>
                            <th>Tahun Akademik</th>
                            <th class="nosort"align="center"></th>
                        </tr>
                        </thead>
                        <tbody id="data_list">

                        @foreach ($datas as $data)
                            <tr id="data{{$data->calon_mahasiswa_id}}">
                                <td></td>
                                <td>{{$data->no_pendaftaran}}</td>
                                <td>{{$data->nama}}</td>
                                <td>{{$data->gender}}</td>
                                <td>{{$data->agama}}</td>
                                <td>{{$data->dob}}</td>
                                <td>{{$data->nama_prodi}}</td>
                                <td>{{$data->status}}</td>
                                <td>{{$data->angkatan}}</td>
                                <td>
                                    <div class="row">
                                    <div class="col-sm-6">
                                        <button class="btn btn-primary btn-block btn-xs" value="{{$data->calon_mahasiswa_id}}">
                                            <span class="glyphicon glyphicon-pencil"></span> Edit
                                        </button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button  onclick="window.location.href='{{'penerimaan-mahasiswa/'.$data->calon_mahasiswa_id}}';" class="btn btn-block btn-success btn-xs" value="{{$data->calon_mahasiswa_id}}">
                                            <span class="glyphicon glyphicon-ok"></span> Terima
                                        </button>
                                    </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('footer')
</div><!-- ./wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>


<script>
    $(function () {
        var t = $('#data_list').DataTable({
            'aoColumnDefs': [
                { targets: [2], searchable: true, orderable:false},
                { targets: '_all', searchable:false, orderable:false}

            ],
            "bAutoWidth": false, // Disable the auto width calculation
            "aoColumns": [
                { "sWidth": "2%" }, // 1st column width
                { "sWidth": "7%" }, // 2nd column width
                { "sWidth": "31%" },
                { "sWidth": "1%" },
                { "sWidth": "5%" },
                { "sWidth": "11%" },
                { "sWidth": "11%" },
                { "sWidth": "8%" },
                { "sWidth": "11%" },
                { "sWidth": "14%" }
            ],
            "iDisplayLength": 50
        });

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
<script>
    $(document).ready(function(){

        //Datemask dd/mm/yyyy
        $('#input_dob').inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        var url = "dosen";

        //display modal form for user editing
        $('.open-modal').click(function(){
            var dosen_id = $(this).val();
            $('#frmInput').trigger("reset");
            $.get("dosen/" + dosen_id, function (dosen) {
                //success data
                console.log(dosen);
                var dob = moment($('#input_dob').val(), "YYYY-MM-DD").format("DD/MM/YYYY");
                $('#dosen_id').val(dosen.dosen_id);
                $('#input_nama').val(dosen.nama);
                $('#input_nidn').val(dosen.nidn);
                $('#input_nip').val(dosen.nip);
                $('#input_gender').val(dosen.gender);
                $('#input_agama').val(dosen.agama);
                $('#input_dob').val(dob);
                $('#input_status').val(dosen.status);


                $('#btn-save').val("update");

                $('#myModal').modal('show');


            })
        });

        $('body').on('shown.bs.modal', '#myModal', function () {
            $('input:visible:enabled:first', this).focus();
        })

        //display modal form for creating new data


        $('#btn-cancel').click(function(){


            $('#frmInput').trigger("reset");

            $('#myModal').modal('hide')
        });

        //create new user / update existing user
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var dosen_id = $('#dosen_id').val();
            var dob = moment($('#input_dob').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var dobforpswd = moment($('#input_dob').val(), "DD/MM/YYYY").format("DDMMYYY");
            var formData = {
                id: $('#dosen_id').val(),
                nama: $('#input_nama').val(),
                nidn: $('#input_nidn').val(),
                nip: $('#input_nip').val(),
                gender: $('#input_gender').val(),
                agama: $('#input_agama').val(),
                plaindob: dobforpswd,
                dob: dob,
                status: $('#input_status').val()
            }

            //used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#btn-save').val();
            var my_url = "dosen";

            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += "/edit";
            }
            else {
                var type = "POST"; //for creating new resource
                var dosen_id = $('#dosen_id').val();

                my_url += "/add";
            }


            console.log(formData);
            console.log(my_url);
            console.log(type);

            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    if (state == "add"){ //if user added a new record
                        var lastrow = $('data_listt').dara.count();

                        var str_html = '<tr id="data' + data.dosen_id + '"><td>' + lastrow+1 + '</td><td>' + data.nama + '</td><td>' + data.nidn + '</td><td>' + data.nip + '</td><td>' + data.gender + '</td><td>' + data.agama + '</td><td>' + data.dob + '</td><td>' + data.status + '</td>';
                        str_html += '<td><div class="col-sm-2"><button class="btn btn-warning btn-block btn-detail open-modal" value="' + data.dosen_id + '"><span class="glyphicon glyphicon-pencil"></span> Edit</button></div>';
                        str_html += '</div></td></tr>';

                        $('#data_list').append(str_html);

                    }else{ //if user updated an existing record
                        var str_html = '<tr id="data' + data.dosen_id + '"><td>' + data.nama + '</td><td>' + data.nidn + '</td><td>' + data.nip + '</td><td>' + data.gender + '</td><td>' + data.agama + '</td><td>' + data.dob + '</td><td>' + data.status + '</td>';
                        str_html += '<td><div class="col-sm-2"><button class="btn btn-warning btn-block btn-detail open-modal" value="' + data.dosen_id + '"><span class="glyphicon glyphicon-pencil"></span> Edit</button></div>';
                        str_html += '</div></td></tr>';

                        $("#data" + data.dosen_id).replaceWith(str_html);

                    }

                    //$('#frminput').trigger("reset");

                    $('#myModal').modal('hide')
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $("#btn_search").click(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var formData = {
                status: $('#sel_status').val(),
                gender: $('#sel_gender').val(),
                agama: $('#sel_agama').val()
            };

            $('#data_list').empty();
            $.ajax({

                type: "POST",
                url: 'dosen/search',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    if (data.length == 0) {
                        var str_html = '<tr id="data"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><div class="col-sm-4"</div><div class="col-sm-4"></div></tr></td>';

                        $('#data_list').append(str_html);
                    }
                    $.each(data, function(i,item) {
                        var str_html = '<tr id="data' + item.dosen_id + '"><td></td><td>' + item.nama + '</td><td>' + item.nidn + '</td><td>' + item.nip + '</td><td>' + item.gender + '</td><td>' + item.agama + '</td><td>' + item.dob + '</td><td>' + item.status + '</td>';
                        str_html += '<td><div class="col-sm-4"><button class="btn btn-warning btn-block btn-detail open-modal" value="' + item.dosen_id + '"><span class="glyphicon glyphicon-pencil"></span> Edit</button></div></td>';
                        str_html += '</div></tr>';

                        $('#data_list').append(str_html);

                    })
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });


        });

    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
