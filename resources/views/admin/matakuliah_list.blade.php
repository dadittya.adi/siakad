<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('admin/header')

<!-- Sidebar -->
@include('admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Matakuliah" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <!-- form start -->
                            <form class="form-horizontal">

                                <div class="form-group row">

                                    <label for="sel_prodi" class="col-md-1 control-label" > Program Studi</label>
                                    <select class="form-control col-md-1 select2" style="width: 10%;" id="sel_prodi" name="sel_prodi">
                                        <option value="0"> Semua</option>
                                        @foreach ($arrProdi as $prodi)
                                            <option value={{$prodi->prodi_id}}>{{$prodi->prodi_name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group row">

                                    <label for="sel_jenisMk" class="col-md-1 control-label" > Jenis Matakuliah</label>

                                    <select class="form-control col-md-1 select2" style="width: 10%;" id="sel_jenisMk" name="sel_jenisMk">
                                        <option value="0"> Semua</option>
                                        @foreach($arrJenisMk as $jenisMk)
                                            <option value={{$jenisMk->jenis_id}}>{{$jenisMk->jenis_nama}}</option>
                                        @endforeach
                                    </select>

                                    <button class="btn btn-info" id="btn_search">
                                        <span class="glyphicon glyphicon-search"></span> Search
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat pull-right open-modal" id="btn-add" name="btn-add" value="0">
                                <span class="glyphicon glyphicon-plus"></span> Tambah</button>
                        </div>
                    </div>

                    <!-- /.box-footer -->
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <table id= "data_list" class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr role="row" class="odd">
                            <th>Kode MK</th>
                            <th>Nama MK</th>
                            <th>SKS</th>
                            <th>Program Studi</th>
                            <th>Jenis MK</th>
                            <th>Kelompok MK</th>
                            <th class="nosort"align="center"></th>
                        </tr>
                        </thead>
                        <tbody id="data_list">
                        @foreach ($datas as $data)

                            <tr id="data{{$data->matakuliah_id}}">
                                <td>{{$data->kode_mk}}</td>
                                <td>{{$data->nama_mk}}</td>
                                <td>{{$data->sks}}</td>
                                <td>{{$data->nama_prodi}}</td>
                                <td>{{$data->jenisMk}}</td>
                                <td>{{$data->kelompokMk}}</td>
                                <td>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary btn-xs btn-detail open-modal" value="{{$data->mk_id}}">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
                            </div>
                            <div class="modal-body">
                                <form id="frmInput" name="frmInput" class="form-horizontal" novalidate="">

                                    <div class="form-group" id="groupNama">
                                        <label for="input_nama" class="col-md-4 control-label">Nama Matakuliah <b class="text-red">*</b></label>
                                        <div class="col-md-7 ">
                                            <input id="input_nama" type="text" class="form-control input_nama" name="input_nama">
                                        </div>
                                    </div>


                                    <div class="form-group"  id="groupKode">
                                        <label for="input_kode" class="col-md-4 control-label">Kode Matakuliah<b class="text-red">*</b></label>

                                        <div class="col-md-7">
                                            <input id="input_kode" type="text" class="form-control" name="input_kode" required autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group" >

                                        <label for="input_prodi" class="col-md-4 control-label" > Program Studi</label>
                                        <div class="col-md-7">
                                            <select class="form-control col-md-1 select2" id="input_prodi" name="input_prodi">
                                                <option selected value="0">&emsp;</option>
                                                @foreach ($arrProdi as $prodi)
                                                    <option value={{$prodi->prodi_id}}>{{$prodi->prodi_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group" id="groupKelompok">

                                        <label for="input_kelompokMk" class="col-md-4 control-label" > Kelompok Matakuliah<b class="text-red">*</b></label>
                                        <div class="col-md-7">
                                            <select class="form-control col-md-1 select2" id="input_kelompokMk" name="input_kelompokMk">
                                                @foreach ($arrKelompokMk as $kelompok)
                                                    <option value={{$kelompok->kelompok_id}}>{{$kelompok->kelompok_nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group" id="groupJenis">

                                        <label for="input_jeniskMk" class="col-md-4 control-label" > Jenis Matakuliah<b class="text-red">*</b></label>
                                        <div class="col-md-7">
                                            <select class="form-control col-md-1 select2" id="input_jeniskMk" name="input_jeniskMk">
                                                @foreach ($arrJenisMk as $jenis)
                                                    <option value={{$jenis->jenis_id}}>{{$jenis->jenis_nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group" id="groupSKS">
                                        <label for="input_sks" class="col-md-4 control-label">SKS<b class="text-red">*</b></label>

                                        <div class="col-md-7">
                                            <input id="input_sks" type="text" class="form-control" name="input_sks" required autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group" id="groupSemester">
                                        <label for="input_semester" class="col-md-4 control-label">Semester<b class="text-red">*</b></label>

                                        <div class="col-md-7">
                                            <input id="input_semester" type="text" class="form-control" name="input_semester" required autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="input_prayarat" class="col-md-4 control-label">MK Prasyarat</label>

                                        <div class="col-md-7">
                                            <select class="form-control col-md-1 prasyarat" style="width: 100%;" id="input_prasyarat" name="input_prayarat">
                                                <option value="0"></option>
                                                @foreach($arrMK as $mk)
                                                    <option value={{$mk->mk_id}}>{{$mk->nama_mk}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-sm-3">
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-danger btn-block" id="btn-cancel" value="cancel">Batal</button>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-success btn-block" id="btn-save" value="add">Simpan</button>
                                    </div>
                                    <div class="col-sm-3">
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" id="matakuliah_id" name="matakuliah_id" value="0">

                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('footer')
</div><!-- ./wrapper -->
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- Select 2 -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/select2/select2.full.min.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>


<script>
    $(function () {
        var t = $('#data_list').DataTable({
            'aoColumnDefs': [
                { targets: [1], searchable: true, orderable:false},
                { targets: '_all', searchable:false, orderable:false}

            ],
            "bAutoWidth": false, // Disable the auto width calculation
            "aoColumns": [
                { "sWidth": "5%" }, // 2nd column width
                { "sWidth": "38%" },
                { "sWidth": "2%" },
                { "sWidth": "10%" },
                { "sWidth": "5%" },
                { "sWidth": "37%" },
                { "sWidth": "3%" }
            ],
            "iDisplayLength": 50
        });

    });
</script>
<script>
    $(document).ready(function(){

        $('.prasyarat').select2();
        //Datemask dd/mm/yyyy
        $('#input_dob').inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        var url = "matakuliah";

        //display modal form for user editing
        $('.open-modal').click(function(){
            var mk_id = $(this).val();
            $('#frmInput').trigger("reset");
            $('#input_prasyarat').val("0").change();
            if (mk_id != "0")
            {
                $.get("matakuliah/" + mk_id, function (matakuliah) {
                    //success data
                    $('#matakuliah_id').val(matakuliah.matakuliah_id);
                    $('#input_nama').val(matakuliah.nama_mk);
                    $('#input_kode').val(matakuliah.kode_mk);
                    $('#input_prodi').val(matakuliah.prodiId).change();
                    $('#input_kelompokMk').val(matakuliah.kelompokMkId).change();
                    $('#input_jeniskMk').val(matakuliah.jenisMkId).change();
                    $('#input_sks').val(matakuliah.sks);
                    $('#input_semester').val(matakuliah.semester);
                    $('#input_prasyarat').val(matakuliah.prasyarat).change();


                    $('#btn-save').val("update");

                    $('#myModal').modal('show');


                })
            }
            $('#myModal').modal('show');


        });

        $('body').on('shown.bs.modal', '#myModal', function () {
            $('input:visible:enabled:first', this).focus();
        })

        //display modal form for creating new data
        $('#btn-add').click(function(){


            $('#btn-save').val("add");
            $('#frminput').trigger("reset");
            $('#myModal').modal('show');
        });

        $('#btn-cancel').click(function(){


            $('#frmInput').trigger("reset");

            $('#myModal').modal('hide')
        });

        //create new user / update existing user
        $("#btn-save").click(function (e) {
            var validation = false;
            if ($('#input_nama').val() == '') {
                $('#input_nama').effect("shake",{ times:3}, 300);
                $('#input_nama').focus();
            }
            else if($('#input_kode').val() == '') {
                $('#input_kode').effect("shake",{ times:3}, 300);
                $('#input_kode').focus();
            }
            else if($('#input_sks').val() == '') {
                $('#input_sks').effect("shake",{ times:3}, 300);
                $('#input_sks').focus();
            }
            else if($('#input_semester').val() == '') {
                $('#input_semester').effect("shake",{ times:3}, 300);
                $('#input_semester').focus();
            }
            else {
                validation = true;
            }

            if (validation) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })

                e.preventDefault();

                var mk_id = $('#matakuliah_id').val();
                var formData = {
                    id: mk_id,
                    nama: $('#input_nama').val(),
                    kode: $('#input_kode').val(),
                    prodi: $('#input_prodi').val(),
                    jenis: $('#input_jeniskMk').val(),
                    kelompok: $('#input_kelompokMk').val(),
                    sks: $('#input_sks').val(),
                    semester: $('#input_semester').val(),
                    prasyarat: $('#input_prasyarat').val(),
                }

                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var my_url = "matakuliah";

                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += "/edit";
                }
                else {
                    var type = "POST"; //for creating new resource
                    var mk_id = $('#matakuliah_id').val();

                    my_url += "/add";
                }


                console.log(formData);
                console.log(my_url);
                console.log(type);

                $.ajax({

                    type: type,
                    url: my_url,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);

                        if (state == "add"){ //if user added a new record

                            var str_html = '<tr id="data' + data.matakuliah_id + '"><td>' + data.kode_mk +
                                '</td><td>' + data.nama_mk + '</td><td>' + data.sks + '</td><td>' + data.nama_prodi + '</td><td>' +
                                data.jenisMk +'</td><td>' +data.kelompokMk +
                                '</td><td><div class="col-sm-1"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' +
                                data.matakuliah_id +'"><span class="glyphicon glyphicon-pencil">' +
                                '</span></button></div><div class="col-sm-1"><button class="btn btn-danger btn-xs btn-detail open-modal" value="' +
                                data.matakuliah_id +'"><span class="glyphicon glyphicon-remove"></span></button></div></td></tr>';

                            $('#data_list').append(str_html);

                        }else{ //if user updated an existing record
                            var str_html = '<tr id="data' + data.matakuliah_id + '"><td>' + data.kode_mk +
                                '</td><td>' + data.nama_mk + '</td><td>' + data.sks + '</td><td>' + data.nama_prodi + '</td><td>' +
                                data.jenisMk +'</td><td>' +data.kelompokMk +
                                '</td><td><div class="col-sm-1"><button class="btn btn-primary btn-xs btn-detail open-modal" value="' +
                                data.matakuliah_id +'"><span class="glyphicon glyphicon-pencil">' +
                                '</span></button></div><div class="col-sm-1"><button class="btn btn-danger btn-xs btn-detail open-modal" value="' +
                                data.matakuliah_id +'"><span class="glyphicon glyphicon-remove"></span></button></div></td></tr>';

                            $("#data" + data.dosen_id).replaceWith(str_html);

                        }

                        //$('#frminput').trigger("reset");

                        $('#myModal').modal('hide')
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }

        });

        $("#btn_search").click(function (e) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var formData = {
                status: $('#sel_status').val(),
                gender: $('#sel_gender').val(),
                agama: $('#sel_agama').val()
            };

            $('#data_list').empty();
            $.ajax({

                type: "POST",
                url: 'matakuliah/search',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    if (data.length == 0) {
                        var str_html = '<tr id="data"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><div class="col-sm-4"</div><div class="col-sm-4"></div></tr></td>';

                        $('#data_list').append(str_html);
                    }
                    $.each(data, function(i,item) {
                        var str_html = '<tr id="data' + item.dosen_id + '"><td></td><td>' + item.nama + '</td><td>' + item.nidn + '</td><td>' + item.nip + '</td><td>' + item.gender + '</td><td>' + item.agama + '</td><td>' + item.dob + '</td><td>' + item.status + '</td>';
                        str_html += '<td><div class="col-sm-4"><button class="btn btn-warning btn-block btn-detail open-modal" value="' + item.dosen_id + '"><span class="glyphicon glyphicon-pencil"></span> Edit</button></div></td>';
                        str_html += '</div></tr>';

                        $('#data_list').append(str_html);

                    })
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });


        });

    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
