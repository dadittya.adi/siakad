<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAKAD AKS IBU KARTINI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/components/bower/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{ asset("/components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset("/components/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/components/bower/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset("/components/bower/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.walker.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/css/dataTables.customLoader.circle.css")}}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset("/components/iCheck/skins/flat/green.css")}}" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
@include('admin/header')

<!-- Sidebar -->
@include('admin/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Pendaftaran Calon Mahasiswa" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
            <div class="box">
                <div class="box-body">
                <form id="frmInput" method="POST" action="{{ url('/admin/pendaftaran/store') }}" name="frmInput" class="form-horizontal" novalidate="">

                    <div class="form-group{{ $errors->has('input_noPendaftaran') ? ' has-error' : '' }}">
                        <label for="input_noPendaftaran" class="col-md-2 control-label">No. Pendaftaran</label>

                        <div class="col-md-2">
                            <input id="input_noPendaftaran" type="text" class="form-control" name="input_noPendaftaran" value="{{ old('input_noPendaftaran') }}" required autofocus>
                            @if ($errors->has('input_noPendaftaran'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('input_noPendaftaran') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="form-group">
                            <label for="input_tahun" class="col-md-1 control-label">Tahun Akademik</label>

                             <div class="col-md-2">
                                 <input id="input_tahun" type="text" class="form-control text-right" name="input_tahun" readonly value="{{$year}}">
                             </div>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 control-label" >Program Studi</label>
                        <div class="col-md-8">
                            @foreach ($prodis as $prodi)
                                <label class="radio-inline">
                                    <input type="radio" class="minimal" name="radio_prodi" id={{'radio'.$prodi->prodi_id}} value={{$prodi->prodi_id}}>
                                    <label for={{'radio'.$prodi->prodi_name}}>&nbsp;&nbsp;{{$prodi->prodi_name}}</label>

                                </label>
                            @endforeach
                        </div>

                    </div>

                    <div class="form-group{{ $errors->has('input_nama') ? ' has-error' : '' }}">
                        <label for="input_nama" class="col-md-2 control-label">Nama</label>

                        <div class="col-md-8">
                            <input id="input_nama" type="text" class="form-control" name="input_nama" value="{{ old('input_nama') }}" required autofocus>

                            @if ($errors->has('input_nama'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('input_nama') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('input_ktp') ? ' has-error' : '' }}">
                        <label for="input_ktp" class="col-md-2 control-label">No. KTP</label>

                        <div class="col-md-2">
                            <input id="input_ktp" type="text" class="form-control" name="input_ktp" value="{{ old('input_ktp') }}" required autofocus>

                            @if ($errors->has('input_ktp'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('input_ktp') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input_dob" class="col-md-2 control-label">Tanggal Lahir</label>
                        <div class="col-md-2">
                            <input id="input_tampatLahir" type="text" class="form-control" name="input_tempatLahir" placeholder="Tempat Lahir">
                        </div>
                        <div class="col-md-2">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input id="input_dob" type="text" class="form-control" name="input_dob" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                            </div>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 control-label" > Jenis Kelamin</label>
                        <div class="col-md-8">
                            <label class="radio-inline">
                                <input class="minimal" type="radio"  name="radio_gender" id="radioGenderL" value="L">
                                <label for="radioGenderL">&nbsp;Laki-Laki</label>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" class="minimal"  name="radio_gender" id="radioGenderP" value="P">
                                <label for="radioGenderP">&nbsp;Perempuan</label>
                            </label>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Agama</label>
                        <div class="col-md-8">
                            @foreach ($religions as $agama)
                                <label class="radio-inline ">
                                    <input type="radio" class="minimal" name="radio_agama" id={{'radio'.$agama->id}} value={{$agama->id}}>
                                    <label for={{'radio'.$agama->name}}>{{$agama->name}}</label>
                                </label>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="input_alamat" class="col-md-2 control-label">Alamat</label>

                        <div class="col-md-8">
                            <input id="input_alamat" type="text" class="form-control" name="input_alamat">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Ds/Kelurahan</label>
                            <input id="input_kelurahan" type="text" class="form-control" name="input_kelurahan">
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Kecamatan</label>
                            <input id="input_kecamatan" type="text" class="form-control" name="input_kecamatan">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Kota/Kab.</label>
                            <input id="input_kota" type="text" class="form-control" name="input_kota">
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Propinsi</label>
                            <input id="input_propinsi" type="text" class="form-control" name="input_propinsi">
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Kode Pos</label>
                            <input id="input_kodePos" type="text" class="form-control" name="input_kodePos">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Telepon Rumah / Handphone</label>
                        <div class="col-md-2">
                            <input id="input_fixedPhone" type="text" class="form-control" name="input_fixedPhone" placeholder="Telepon Rumah">
                        </div>
                        <div class="col-md-2">
                            <input id="input_mobilePhone" type="text" class="form-control" name="input_mobilePhone" placeholder="Handphone">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="input_asalSekolah" class="col-md-2 control-label">Asal Sekolah / Tahun Lulus</label>

                        <div class="col-md-6">
                            <input id="input_asalSekolah" type="text" class="form-control" name="input_asalSekolah" placeholder="Asal Sekolah">
                        </div>
                        <div class="col-md-2">
                            <input id="input_TahunLulus" type="text" class="form-control" name="input_TahunLulus" placeholder="Tahun Lulus">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="input_namaAyah" class="col-md-2 control-label">Nama Ayah</label>

                        <div class="col-md-8">
                            <input id="input_namaAyah" type="text" class="form-control" name="input_namaAyah">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="input_namaIbu" class="col-md-2 control-label">Nama Ibu</label>

                        <div class="col-md-8">
                            <input id="input_namaIbu" type="text" class="form-control" name="input_namaIbu">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" >Pekerjaan Ayah</label>
                        <div class="col-md-9">
                            @foreach ($jobs as $pekerjaan)
                                <label class="radio-inline">
                                    <input type="radio" class="minimal" name="radio_pekerjaan" id={{'radio'.$pekerjaan->pekerjaan_id}} value={{$pekerjaan->pekerjaan_id}}>
                                    <label for={{'radio'.$pekerjaan->pekerjaan_nama}}>&nbsp;{{$pekerjaan->pekerjaan_nama}}</label>
                                </label>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="input_instansi" class="col-md-2 control-label">Nama Instansi</label>

                        <div class="col-md-4">
                            <input id="input_instansi" type="text" class="form-control" name="input_instansi">
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-2 control-label" >Mengetahui AKS Ibu Kartini dari?</label>
                        <div class="col-md-8">
                            @foreach ($infSources as $sumberInf)
                                <label class="radio-inline">
                                    <input type="radio" class="minimal" name="radio_sumberInf" id={{'radio'.$sumberInf->source_id}} value={{$sumberInf->source_id}}>
                                    <label for={{'radio'.$sumberInf->source_name}}>&nbsp;&nbsp;{{$sumberInf->source_name}}</label>
                                </label>
                            @endforeach
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="box">
                <div class="box-body">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-danger btn-block" id="btn-cancel" value="cancel">Batal</button>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-block" id="btn-save" value="add">Simpan</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
    <!-- /.box -->>
<!-- Modal -->
<div class="modal fade" id="dlgConfirm" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Data berhasil disimpan, tambahkan data lagi?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    <!-- Footer -->
    @include('footer')

<!-- REQUIRED JS SCRIPTS -->
<meta name="_token" content="{!! csrf_token() !!}" />
<script
        src="http://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("/components/bower/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/components/bower/admin-lte/dist/js/demo.js") }}" type="text/javascript"></script>
<!-- DataTables -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/fastclick/fastclick.js") }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/components/bower/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js" ) }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/components/moment/moment.js") }}" type="text/javascript"></script>
<!-- iCheck    -->
<script src="{{ asset ("/components/iCheck/iCheck.js") }}"></script>

<script>

    $(document).ready(function(){

        var inputAgama = '1';
        var inputGender = 'L';
        var inputPekerjaan = '1';
        var inputProdi;
        var inputInfSource = '1';

        //Datemask dd/mm/yyyy
        $('#input_dob').inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('input[name="radio_gender"]').on('ifClicked', function (event) {
            inputGender = this.value;
        });

        $('input[name="radio_agama"]').on('ifClicked', function (event) {
            inputAgama = this.value;
        });

        $('input[name="radio_pekerjaan"]').on('ifClicked', function (event) {
            inputPekerjaan = this.value;
        });

        $('input[name="radio_pekerjaan"]').on('ifClicked', function (event) {
            inputPekerjaan = this.value;
        });

        $('input[name="radio_prodi"]').on('ifClicked', function (event) {
            inputProdi = this.value;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            event.preventDefault();

            var formData = {
                prodiId: inputProdi,
                tahun: $('#input_tahun').val()
            }

            console.log(formData);

            $.ajax({

                type: "POST",
                url: 'grabRegNumber/',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $('#input_noPendaftaran').val(data.no_pendaftaran);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('input[name="radio_sumberInf"]').on('ifClicked', function (event) {
            inputInfSource = this.value;
        });

        //create new user / update existing user
        $("#btn-save").click(function (e) {
            var my_url = "pendaftaran";

            var state = $('#btn-save').val();
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += "/edit";
            }
            else {
                var type = "POST"; //for creating new resource

                my_url += "/store";
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();


            var dob = moment($('#input_dob').val(), "DD/MM/YYYY").format("YYYY-MM-DD");
            var formData = {
                noPendaftaran: $('#input_noPendaftaran').val(),
                tahun: $('#input_tahun').val(),
                nama: $('#input_nama').val(),
                ktp: $('#input_ktp').val(),
                tempatLahir: $('#input_tampatLahir').val(),
                dob: dob,
                gender: inputGender,
                agama: inputAgama,
                alamat: $('#input_alamat').val(),
                kelurahan: $('#input_kelurahan').val(),
                kecamatan: $('#input_kecamatan').val(),
                kota: $('#input_kota').val(),
                propinsi: $('#input_propinsi').val(),
                kodePos: $('#input_kodePos').val(),
                fixedPhone: $('#input_fixedPhone').val(),
                mobilePhone: $('#input_mobilePhone').val(),
                asalSekolah: $('#input_asalSekolah').val(),
                tahunLulus: $('#input_TahunLulus').val(),
                namaAyah: $('#input_namaAyah').val(),
                namaIbu: $('#input_namaIbu').val(),
                pekerjaanAyah: inputPekerjaan,
                instansi: $('#input_instansi').val(),
                prodi: inputProdi,
                infSource:inputInfSource
            }

            //used to determine the http verb to use [add=POST], [update=PUT]

            console.log(formData);
            console.log(my_url);
            console.log(type);

            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    window.location.href = "/admin/pendaftaran/";

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
