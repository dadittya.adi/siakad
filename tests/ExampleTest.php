<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('admin/pendaftaran/store')
            ->type('inputnama', 'nama')
            ->type('ktp', 'ktp')
            ->type('tempatLahir', 'tempatLahir')
            ->type('gender', 'P')
            ->type('agama', '2')
            ->type('alamat', 'alamat')
            ->type('kelurahan', 'kelurahan')
            ->type('kecamatan', 'kecamatan')
            ->type('propinsi', 'propinsi')
            ->type('kota', 'kota')
            ->type('kodepos', 'kodepos')
            ->type('fixedPhone', 'phoneFixed')
            ->type('mobilePhone', 'phoneMobile')
            ->type('asalSekolah', 'asalSekolah')
            ->type('tahunLulus', '2002')
            ->type('namaIbu', 'namaIbu')
            ->type('namaAyah', 'namaAyah')
            ->type('pekerjaanAyah', '3')
            ->type('nopendaftaran', '17211001')
            ->type('pekerjaanAyah', '3')
            ->type('pekerjaanAyah', '3')
             ->see('Laravel');
    }
}
