<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKurikulumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculum', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('name')->nullable();

            $table->string('prodi_id');
            $table->foreign('prodi_id')->references('id')->on('study_programs');

            $table->string('year_id', 36);
            $table->foreign('year_id')->references('id')->on('years');

            $table->integer('required_credit')->default('0');
            $table->integer('elective_credit')->default('0');
            $table->string('edited_by', 36);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
