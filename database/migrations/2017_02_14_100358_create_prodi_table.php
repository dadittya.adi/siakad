<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_programs', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('code');
            $table->string('name');
            $table->string('nim_code');
            $table->string('certificate_code');
            $table->string('lecturer_id', 36)->default('');
            $table->string('status')->default('1');
            $table->string('sk')->default('');
            $table->string('jenjang')->default('D3');
            $table->string('gelar')->default('Ahli Madya');
            $table->integer('other_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
