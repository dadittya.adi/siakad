<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KelasPengajar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_lecturer', function (Blueprint $table) {
            $table->string('id', 36)->primary();

            $table->string('class_id', 36);
            $table->foreign('class_id')->references('id')->on('class');

            $table->string('lecturer_id',36);
            $table->foreign('lecturer_id')->references('id')->on('lecturer');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
