<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailKrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('krs_detail', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('grade', 2);
            $table->decimal('gradecredit', 4,2);

            $table->string('krs_id',36);
            $table->foreign('krs_id')->references('id')->on('krs');

            $table->string('class_id', 36);
            $table->foreign('class_id')->references('id')->on('class');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
