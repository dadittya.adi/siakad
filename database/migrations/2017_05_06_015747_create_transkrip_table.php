<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranskripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transcript', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('code');

            $table->string('student_id', 36);
            $table->foreign('student_id')->references('id')->on('students');
            $table->decimal('gpa', 3,2);
            $table->integer('credit')->default('0');

            $table->date('date')->nullable();

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
