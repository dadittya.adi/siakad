<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('nim')->unique();

            $table->string('year_id', 36);
            $table->foreign('year_id')->references('id')->on('years');

            $table->string('prodi_id', 36);
            $table->foreign('prodi_id')->references('id')->on('study_programs');

            $table->string('lecturer_id',36)->nullable();

            $table->string('curriculum_id', 36);
            $table->foreign('curriculum_id')->references('id')->on('curriculum');

            $table->string('category_id', 36);
            $table->foreign('category_id')->references('id')->on('student_category');

            $table->string('user_id', 36);

            $table->string('status')->default('aktif');
            $table->date('dob');

            $table->string('edited_by',36);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
