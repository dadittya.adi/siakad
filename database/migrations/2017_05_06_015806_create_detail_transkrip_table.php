<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTranskripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transcript_detail', function (Blueprint $table) {
            $table->string('id',36)->primary();

            $table->string('courses_id', 36);
            $table->foreign('courses_id')->references('id')->on('courses');

            $table->string('grade', 2);
            $table->decimal('gradecredit', 4,2);

            $table->string('transcript_id',36);
            $table->foreign('transcript_id')->references('id')->on('transcript');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
