<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMahasiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_detail', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('candidate_id', 36)->nullable();
            $table->string('student_id')->nullable();
            $table->string('name');
            $table->string('idcard_number')->nullable();
            $table->string('dob_place')->nullable();
            $table->string('gender')->nullable();

            $table->string('religion_id')
                ->nullable();
            $table->string('address')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('district')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('phone_fixed')->nullable();
            $table->string('phone_mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('high_school')->nullable();
            $table->string('year_graduate')->nullable();
            $table->string('blood_type')->nullable();
            $table->string('citizen')->nullable();
            $table->string('status_work')->nullable();
            $table->string('status_marital')->nullable();
            $table->string('student_diploma')->nullable();
            $table->string('status_transfer')->nullable();
            $table->string('university_origin')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('parent_address')->nullable();
            $table->string('parent_province')->nullable();
            $table->string('father_education_id')
                ->nullable();
            $table->string('mother_education_id')
                ->nullable();
            $table->string('father_occupation_id')
                ->nullable();
            $table->string('father_agency')->nullable();
            $table->integer('mother_occupation_id')
                ->unsigned()
                ->nullable();
            $table->string('mother_agency')->nullable();

            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
