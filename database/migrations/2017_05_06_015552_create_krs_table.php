<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('krs', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->date('date');
            $table->string('semester', 5);
            $table->string('status', 6);
            $table->integer('credit_next');
            $table->integer('credit_total');
            $table->decimal('last_gp', 4,2);
            $table->decimal('gp', 4,2);

            $table->string('student_id', 36);
            $table->foreign('student_id')->references('id')->on('students');

            $table->string('year_id', 36);
            $table->foreign('year_id')->references('id')->on('years');

            $table->string('edited_by',36);
            $table->timestamps();
        });

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
