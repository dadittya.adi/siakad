<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('name');
            $table->smallInteger('quota')->nullable();
            $table->string('courses_id', 36);
            $table->foreign('courses_id')->references('id')->on('courses');

            $table->string('semester');
            $table->string('year_id',36);
            $table->foreign('year_id')->references('id')->on('years');

            $table->string('prodi_id', 36);
            $table->foreign('prodi_id')->references('id')->on('study_programs');

            $table->string('status');
            $table->string('edited_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
