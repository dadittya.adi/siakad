<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatakuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('code');
            $table->string('name');

            $table->string('prodi_id', 36);

            $table->string('category_id', 36);
            $table->foreign('category_id')->references('id')->on('courses_category');

            $table->string('type_id', 36);
            $table->foreign('type_id')->references('id')->on('courses_type');

            $table->integer('credit');
            $table->string('semester');
            $table->string('prerequisite_course')->nullable();
            $table->string('status')->default('aktif');
            $table->string('year_id', 36);
            $table->string('edited_by',36);
            $table->timestamps();
        });

        Schema::table('matakuliah', function (Blueprint $table) {


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
