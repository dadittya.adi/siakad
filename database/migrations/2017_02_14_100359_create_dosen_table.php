<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('name');
            $table->string('nid')->nullable();
            $table->string('nidn')->nullable();
            $table->string('nip')->nullable();
            $table->string('alamat')->nullable();
            $table->string('phone_fixed')->nullable();
            $table->string('phone_mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('dob_place')->nullable();
            $table->date('dob')->nullable();
            $table->string('type', 36)->nullable();

            $table->string('prodi_id', 36)
                ->default('');

            $table->string('gender');

            $table->string('religion_id', 36)->default('');

            $table->string('status_id', 36);
            $table->foreign('status_id')->references('id')->on('lecturer_status');

            $table->string('user_id', 36);
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('edited_by', 36);
            $table->timestamps();
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
