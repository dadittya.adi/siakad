<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalonMahasiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('reg_number')->unique();
            $table->date('date');

            $table->string('year_id');
            $table->foreign('year_id')->references('id')->on('years');

            $table->string('method')->default('offline');
            $table->string('status');

            $table->string('prodi_id');
            $table->foreign('prodi_id')->references('id')->on('study_programs');

            $table->string('source_id',36);
            $table->foreign('source_id')->references('id')->on('source_information');

            $table->date('dob');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
