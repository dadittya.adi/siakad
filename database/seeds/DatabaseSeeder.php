<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // --------- seeder table user -----------
        //----------------------------------------
        $user_id = Uuid::uuid4()->getHex();
        DB::table('users')->insert([
            'id' => $user_id,
            'name' => 'superadmin',
            'username' => 'superadmin',
            'password' => bcrypt('superpass1234'),
            'level' => 'Admin',
            'status' => 1,
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('study_programs')->insert([
            'id' => $id,
            'name' => 'Tata Rias',
            'nim_code' => '211',
            'certificate_code' => 'R',
            'other_id' => '1'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('study_programs')->insert([
            'id' => $id,
            'name' => 'Tata Busana',
            'nim_code' => '212',
            'certificate_code' => 'BS',
            'other_id' => '2'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('study_programs')->insert([
            'id' => $id,
            'name' => 'Tata Boga',
            'nim_code' => '213',
            'certificate_code' => 'BG',
            'other_id' => '3'
        ]);

        // --------- seeder golongan mahasiswa----
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('student_category')->insert([
            'id' => $id,
            'name' => 'Pelajar Biasa',
            'other_id' => '1'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_category')->insert([
            'id' => $id,
            'name' => 'Beasiswa',
            'other_id' => '2'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_category')->insert([
            'id' => $id,
            'name' => 'Tugas Belajar',
            'other_id' => '3'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_category')->insert([
            'id' => $id,
            'name' => 'Pendengar',
            'other_id' => '4'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_category')->insert([
            'id' => $id,
            'name' => 'Ikatan Dinas',
            'other_id' => '5'
        ]);

        // --------- seeder agama-----------------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('religions')->insert([
            'id' => $id,
            'name' => 'Islam',
            'other_id' => '1'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('religions')->insert([
            'id' => $id,
            'name' => 'Kristen',
            'other_id' => '2'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('religions')->insert([
            'id' => $id,
            'name' => 'Katolik',
            'other_id' => '3'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('religions')->insert([
            'id' => $id,
            'name' => 'Hindu',
            'other_id' => '4'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('religions')->insert([
            'id' => $id,
            'name' => 'Buddha',
            'other_id' => '5'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('religions')->insert([
            'id' => $id,
            'name' => 'Konghuchu',
            'other_id' => '6'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('religions')->insert([
            'id' => $id,
            'name' => 'Lainnya',
            'other_id' => '7'
        ]);

        // --------- seeder grade-----------------
        //----------------------------------------
        DB::table('grade')->insert([
            'grade' => 'A',
            'number' => '4.0',
        ]);
        DB::table('grade')->insert([
            'grade' => 'AB',
            'number' => '3.5',
        ]);
        DB::table('grade')->insert([
            'grade' => 'B',
            'number' => '3.0',
        ]);
        DB::table('grade')->insert([
            'grade' => 'BC',
            'number' => '2.5',
        ]);
        DB::table('grade')->insert([
            'grade' => 'C',
            'number' => '2.0',
        ]);
        DB::table('grade')->insert([
            'grade' => 'CD',
            'number' => '1.5',
        ]);
        DB::table('grade')->insert([
            'grade' => 'D',
            'number' => '1.0',
        ]);
        DB::table('grade')->insert([
            'grade' => 'E',
            'number' => '0',
        ]);
        DB::table('grade')->insert([
            'grade' => 'T',
            'number' => '0',
        ]);

        // --------- seeder Ijazah Terakhir-------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('student_diploma')->insert([
            'id' => $id,
            'name' => 'SMA',
            'other_id' => '1'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_diploma')->insert([
            'id' => $id,
            'name' => 'SMEA',
            'other_id' => '2'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_diploma')->insert([
            'id' => $id,
            'name' => 'SKKA',
            'other_id' => '3'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_diploma')->insert([
            'id' => $id,
            'name' => 'STM',
            'other_id' => '4'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_diploma')->insert([
            'id' => $id,
            'name' => 'SPG',
            'other_id' => '5'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_diploma')->insert([
            'id' => $id,
            'name' => 'SAA',
            'other_id' => '6'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('student_diploma')->insert([
            'id' => $id,
            'name' => 'SPK',
            'other_id' => '7'
        ]);

        // --------- seeder Jenis MK--------------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('courses_type')->insert([
            'id' => $id,
            'name' => 'Wajib',
            'other_id' => '1'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('courses_type')->insert([
            'id' => $id,
            'name' => 'Pilihan',
            'other_id' => '2'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('courses_type')->insert([
            'id' => $id,
            'name' => 'Wajib Permintaan',
            'other_id' => '3'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('courses_type')->insert([
            'id' => $id,
            'name' => 'Pilihan Permintaan',
            'other_id' => '4'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('courses_type')->insert([
            'id' => $id,
            'name' => 'Tugas Akhir/Skripsi',
            'other_id' => '5'
        ]);


        // --------- seeder Pekerjaan--------------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('ocupation')->insert([
            'id' => $id,
            'name' => 'PNS/Pensiunan',
            'other_id' => '1'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('ocupation')->insert([
            'id' => $id,
            'name' => 'TNI/Polri/Purnawirawan',
            'other_id' => '2'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('ocupation')->insert([
            'id' => $id,
            'name' => 'BUMN',
            'other_id' => '3'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('ocupation')->insert([
            'id' => $id,
            'name' => 'Peg. Swasta',
            'other_id' => '4'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('ocupation')->insert([
            'id' => $id,
            'name' => 'Petani',
            'other_id' => '5'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('ocupation')->insert([
            'id' => $id,
            'name' => 'Pedagang',
            'other_id' => '6'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('ocupation')->insert([
            'id' => $id,
            'name' => 'Wiraswasta',
            'other_id' => '7'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('ocupation')->insert([
            'id' => $id,
            'name' => 'Lainnya',
            'other_id' => '8'
        ]);


        // --------- seeder Kelompok MK--------------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('courses_category')->insert([
            'id' => $id,
            'name' => 'MPK-Pengembangan Kepribadian',
            'other_id' => '1'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('courses_category')->insert([
            'id' => $id,
            'name' => 'MKK-Keilmuan dan Ketrampilan',
            'other_id' => '2'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('courses_category')->insert([
            'id' => $id,
            'name' => 'MKB-Keahlian Berkarya',
            'other_id' => '3'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('courses_category')->insert([
            'id' => $id,
            'name' => 'MPB-Perilaku Berkarya',
            'other_id' => '4'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('courses_category')->insert([
            'id' => $id,
            'name' => 'MBB-Berkehidupan Bermasyarakat',
            'other_id' => '5'
        ]);

        // --------- seeder Pendidikan ortu--------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'Tidak Ada',
            'other_id' => '1'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'SD/SR/HIS',
            'other_id' => '2'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'SLP',
            'other_id' => '3'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'SLA',
            'other_id' => '4'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'SM',
            'other_id' => '5'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'Sarjana',
            'other_id' => '6'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'Master',
            'other_id' => '7'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'Doctor',
            'other_id' => '8'
        ]);

        $id = Uuid::uuid4()->getHex();
        DB::table('education')->insert([
            'id' => $id,
            'name' => 'Lainnya',
        ]);

        // --------- seeder Status Dosen ---------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Aktif',
            'other_id' => '1'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Tidak Aktif',
            'other_id' => '2'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Cuti',
            'other_id' => '3'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Keluar',
            'other_id' => '4'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Almarhum',
            'other_id' => '5'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Pensiun',
            'other_id' => '6'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Ijin Belajar',
            'other_id' => '7'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Tugas di Instansi Lain',
            'other_id' => '8'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Ganti NIDN',
            'other_id' => '9'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('lecturer_status')->insert([
            'id' => $id,
            'name' => 'Tugas Belajar',
            'other_id' => '10'
        ]);


        // --------- seeder Sumber Informasi------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('source_information')->insert([
            'id' => $id,
            'name' => 'Internet',
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('source_information')->insert([
            'id' => $id,
            'name' => 'Brosur',
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('source_information')->insert([
            'id' => $id,
            'name' => 'Sekolah',
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('source_information')->insert([
            'id' => $id,
            'name' => 'Koran',
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('source_information')->insert([
            'id' => $id,
            'name' => 'Teman',
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('source_information')->insert([
            'id' => $id,
            'name' => 'Lainnya',
        ]);

        // --------- seeder Tahun Akademik ------
        //----------------------------------------
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1990',
            'range' => '1990/1991'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1991',
            'range' => '1991/1992'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1992',
            'range' => '1992/1993'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1993',
            'range' => '1993/1994'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1994',
            'range' => '1994/1995'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1995',
            'range' => '1995/1996'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1996',
            'range' => '1996/1997'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1997',
            'range' => '1997/1998'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1998',
            'range' => '1998/1999'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '1999',
            'range' => '1999/2000'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2000',
            'range' => '2000/2001'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2001',
            'range' => '2001/2002'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2002',
            'range' => '2002/2003'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2003',
            'range' => '2003/2004'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2004',
            'range' => '2004/2005'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2005',
            'range' => '2005/2006'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2006',
            'range' => '2006/2007'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2007',
            'range' => '2007/2008'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2008',
            'range' => '2008/2009'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2009',
            'range' => '2009/2010'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2010',
            'range' => '2010/2011'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2010',
            'range' => '2010/2011'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2011',
            'range' => '2011/2012'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2012',
            'range' => '2012/2013'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2013',
            'range' => '2013/2014'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2014',
            'range' => '2014/2015'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2015',
            'range' => '2015/2016'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2016',
            'range' => '2016/2017'
        ]);
        $id = Uuid::uuid4()->getHex();
        DB::table('years')->insert([
            'id' => $id,
            'name' => '2017',
            'range' => '2017/2018'
        ]);

    }
}
