<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Ramsey\Uuid\Uuid;

class UserController extends Controller
{

    public function index()
    {
        $datas = User::where('Level', 'Admin')
                                    ->orderBy('username')
                                    ->get();

        return view('/admin/user')->with('datas', $datas);

    }

    public function store(Request $request)
    {


        $rules = array (
            'name' => 'required',
            'username' => 'required',
            'level' => 'required',
            'password' => 'required'
        );
        $validator = Validator::make ( Input::all (), $rules );
        if ($validator->fails ())
            return Response::json ( array (

                'errors' => $validator->getMessageBag ()->toArray ()
            ) );
        else {
            $user_id = Uuid::uuid4()->getHex();

            $user = new User ();
            $user->user_id = $user_id;
            $user->username = $request->username;
            $user->name = $request->name;
            $user->level = $request->level;
            $user->password = bcrypt($request->password);
            $user->status = 1;
            $user->save ();
            return response ()->json ( $user );
        }

    }


    public function show($id)
    {
        $user = User::find ( $id );
        return response ()->json ( $user );
    }

    public function search(Request $request)
    {
        $level = $request->level;
        $status = $request->status;

        if ($status == 2) {
            $user = User::where('level', $level)
                ->get();
        }
        else {
            $user = User::where('level', $level)
                        ->where('status', $status)
                        ->get();
        }
        return response ()-> json ($user);
    }


    public function edit(Request $request) {

        $id = $request->id;
        $user = User::where('user_id', $id)->first();
        $user->username = $request->username;
        $user->update ();
        return response ()->json ( $user );
    }

    public function lock(Request $request) {

        $id = $request->id;
        $user = User::where('id', $id)->first();
        $user->status = 0;
        $user->update ();
        return response ()->json ( $user );
    }

    public function unlock(Request $request) {

        $id = $request->id;
        $user = User::where('id', $id)->first();
        $user->status = 1;
        $user->update ();
        return response ()->json ( $user );
    }



    public function destroy($id)
    {
        //
    }
}
