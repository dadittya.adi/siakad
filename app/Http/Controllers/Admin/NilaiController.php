<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\KelasPerkuliahan;
use App\Models\KrsDetail;
use App\Models\Kurikulum;
use App\Models\Prodi;
use App\Models\TahunAkademik;
use Illuminate\Http\Request;
use App\Models\Krs;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\DB;
use App\Helpers\GlobalHelper;


class NilaiController extends Controller
{

    public function index()
    {
        $prodiId = Prodi::select('id')
            ->orderby('name')
            ->first();
        $prodiId = $prodiId->id;

        $datas = KelasPerkuliahan::select(
            'class.id as kelas_id', 'class.name as nama_kelas', 'courses.code as kode_mk',
            'courses.name as nama_mk', 'courses.credit as sks', 'lecturer.name as dosen',
            DB::raw('count(krs_detail.id) as peserta'), 'study_programs.name as nama_prodi')
            ->leftjoin('krs_detail', 'krs_detail.class_id', '=', 'class.id')
            ->leftjoin('study_programs', 'class.prodi_id', '=', 'study_programs.id')
            ->leftjoin('courses', 'class.courses_id', '=', 'courses.id')
            ->leftjoin('courses_lecturer', 'courses_lecturer.class_id', '=', 'class.id')
            ->leftjoin('lecturer', 'lecturer.id', '=', 'courses_lecturer.lecturer_id')
            ->where('class.prodi_id', '=', $prodiId)
            ->where('class.year_id', GlobalHelper::getLatestYearId())
            ->where('class.semester', '=', GlobalHelper::getLatestSemester())
            ->groupby('class.id')
            ->get();


        $prodis = Prodi::getProdiList();
        $years = TahunAkademik::getYearList();

        return view('/admin/nilai_input_list')
            ->with('datas', $datas)
            ->with('prodis', $prodis)
            ->with('years', $years)
            ->with('now', TahunAkademik::getLatestYearId());
    }

    public function KelasDetail($id) {
        $datas = KrsDetail::select('krs_detail.id as KrsDetailId', 'students.nim as nim', 'student_detail.name as nama',
            'krs_detail.grade as nilai')
                ->leftjoin('krs', 'krs.id', '=', 'krs_detail.krs_id')
                ->leftjoin('students', 'krs.student_id', '=', 'students.id')
                ->leftjoin('student_detail', 'student_detail.student_id', '=', 'students.id')
                ->where('class_id', '=', $id)
                ->orderby('students.nim')
                ->get();

        return view('/admin/nilai_input_detail_list')
            ->with('datas', $datas)
           // ->with('prodis', $prodis)
            //->with('years', $years)
            ->with('now', TahunAkademik::getLatestYearId());

    }

}