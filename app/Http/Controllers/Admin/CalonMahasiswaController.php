<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CalonMahasiswa;
use App\Models\MahasiswaDetail;
use App\Models\Prodi;
use App\Models\TahunAkademik;
use App\Models\Agama;
use App\Models\Pekerjaan;
use App\Models\SumberInformasi;
use App\Helpers\GlobalHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Ramsey\Uuid\Uuid;

class CalonMahasiswaController extends Controller
{
    public function index()
    {
        $datas = CalonMahasiswa::select(
                'candidates.id', 'student_detail.name as nama', 'reg_number as no_pendaftaran',
                'gender as gender', 'religions.name as agama', DB::raw('date_format(dob,"%d/%m/%Y") as dob'),
                'study_programs.name as nama_prodi', 'years.name as angkatan', 'candidates.status as status')
            ->leftjoin('years', 'candidates.year_id', '=', 'years.id' )
            ->leftjoin('study_programs', 'candidates.prodi_id', '=', 'study_programs.id' )
            ->leftjoin('student_detail', 'candidates.id', '=', 'candidate_id' )
            ->leftjoin('religions', 'religion_id', '=', 'religions.id' )
            ->orderBy('reg_number')
            ->get();


        $prodis = Prodi::getProdiList();

        $years = TahunAkademik::getYearList();

        $latestYear = GlobalHelper::getLatestYearId('candidates');
        return view('/admin/pendaftaran_list')
            ->with('datas', $datas)
            ->with('arrProdi', $prodis)
            ->with('years', $years)
            ->with('latestYear', $latestYear);

    }

    public function showMhsRegistrationForm()
    {
        $action = 'add';
        if ($action == 'add' or $action == 'edit') {
            $prodis = Prodi::getProdiList();
            $religions = Agama::getAgamaList();
            $jobs = Pekerjaan::getPekerjaanList();
            $infSources = SumberInformasi::getInfSourceList();
            $year = GlobalHelper::getCurrentYear();

            if ($action == 'add') {
                return view('/admin/pendaftaran_form')
                    ->with('prodis', $prodis)
                    ->with('year', $year)
                    ->with('religions', $religions)
                    ->with('jobs', $jobs)
                    ->with('infSources', $infSources);
            }
            else {

            }
        }
        else {

        }
    }

    public function store(Request $request)
    {
        $rules = array (
            'nama' => 'required',
            'noPendaftaran' => 'required'
        );
        $validator = Validator::make ( Input::all (), $rules );
        if ($validator->fails ())
            return Response::json ( array (

                'errors' => $validator->getMessageBag ()->toArray ()
            ) );
        else {
            DB::beginTransaction();
            try {
                $yearId = TahunAkademik::getYearId($request->tahun);
                $result = CalonMahasiswa::select('id')
                    ->where('reg_number', '=', $request->noPendaftaran)
                    ->first();

                if ($result) {
                    $noPendaftaran = CalonMahasiswa::getNewRegNumber($request->prodi, $yearId);
                }
                else {
                    $noPendaftaran = $request->noPendaftaran;
                }

                $candidateId = Uuid::uuid4()->getHex();
                $calonMahasiswa = new CalonMahasiswa();
                $calonMahasiswa->id = $candidateId;
                $calonMahasiswa->reg_number = $noPendaftaran;
                $calonMahasiswa->year_id = $yearId;
                $calonMahasiswa->method = 'Offline';
                $calonMahasiswa->status = 'Pending';
                $calonMahasiswa->source_id = $request->infSource;
                $calonMahasiswa->prodi_id = $request->prodi;
                $calonMahasiswa->dob = $request->dob;

                $calonMahasiswa->save();

                $detailId = Uuid::uuid4()->getHex();
                $detail = new MahasiswaDetail();

                $detail->id = $detailId;
                $detail->name = $request->nama;
                $detail->idcard_number = $request->ktp;
                $detail->dob_place = $request->tempatLahir;
                $detail->candidate_id = $candidateId;
                $detail->gender = $request->gender;
                $detail->religion_id = $request->agama;
                $detail->address = $request->alamat;
                $detail->kelurahan = $request->kelurahan;
                $detail->district = $request->kecamatan;
                $detail->city = $request->kota;
                $detail->province = $request->propinsi;
                $detail->postal_code = $request->kodePos;
                $detail->phone_fixed = $request->fixedPhone;
                $detail->phone_mobile = $request->mobilePhone;
                $detail->email = '';
                $detail->high_school = $request->asalSekolah;
                $detail->year_graduate = $request->tahunLulus;
                $detail->mother_name = $request->namaIbu;
                $detail->father_name = $request->namaAyah;
                $detail->father_occupation_id = $request->pekerjaanAyah;
                $detail->save();

                DB::commit();

                return response ()-> json ('success');

            }
            catch (\Exception $e) {
                DB::rollback();
                return response ()-> json ($e);

            }

        }

    }

    public function grabNewNoPendaftaran(Request $request)
    {
        $prodiId = $request->prodiId;
        $yearId = TahunAkademik::getYearId($request->tahun);

        $newNumber = CalonMahasiswa::getNewRegNumber($prodiId, $yearId);

        return response ()->json ( ['no_pendaftaran' => $newNumber] );

    }

    public Function getNamaProdi($prodiId) {
        $datas = Prodi::select('prodi_name')
            ->where('prodi_id', $prodiId)
            ->get();

        return $datas->prodi_name;
    }

    public function getDataToAccept($id) {
        $datas = CalonMahasiswa::select(
            'calon_mahasiswa.calon_mahasiswa_id', 'mahasiswa_detail_nama as nama','mahasiswa_detail_ktp as ktp', 'mahasiswa_detail_tempat_lahir as tempatLahir',
            'mahasiswa_detail.agama_id as agamaId', 'mahasiswa_detail_alamat as alamat', 'mahasiswa_detail_alamat_kecamatan as kecamatan',
            'mahasiswa_detail_alamat_kelurahan as kelurahan', 'mahasiswa_detail_alamat_kodepos as kodepos', 'mahasiswa_detail_alamat_kota as kota',
            'mahasiswa_detail_alamat_propinsi as propinsi', 'mahasiswa_detail_phone_fixed as fixedPhone', 'mahasiswa_detail_phone_mobile as mobilePhone',
            'mahasiswa_detail_nama_ayah as namaAyah', 'mahasiswa_detail_nama_ibu as namaaIbu', 'mahasiswa_detail_alamat_ortu as alamatOrtu',
            'mahasiswa_detail_propinsi_ortu as propinsiOrtu', 'pendidikan_ayah_id as pendidikanAyahId', 'pendidikan_ibu_id as pendidikanIbuId',
            'pekerjaan_ayah_id as pekerjaanAyahId', 'mahasiswa_detail_instansi_ayah as instansiAyah', 'pekerjaan_ibu_id as pekerjaanIbuId',
            'mahasiswa_detail_instansi_ibu as instansiIbu', 'golongan_mahasiswa_id as golMhsId',
            'mahasiswa_detail_gender as gender', 'agama_nama as agama', DB::raw('date_format(calon_mahasiswa_dob,"%d/%m/%Y") as dob'),
            'program_studi.prodi_name as nama_prodi', 'calon_mahasiswa.prodi_id as prodiId', 'tahun_akademik_name as angkatan', 'calon_mahasiswa_status as status')
            ->leftjoin('tahun_akademik', 'calon_mahasiswa.angkatan_id', '=', 'tahun_akademik.tahun_akademik_id' )
            ->leftjoin('program_studi', 'calon_mahasiswa.prodi_id', '=', 'program_studi.prodi_id' )
            ->leftjoin('mahasiswa_detail', 'calon_mahasiswa.calon_mahasiswa_id', '=', 'mahasiswa_detail.calon_mahasiswa_id' )
            ->leftjoin('agama', 'mahasiswa_detail.agama_id', '=', 'agama.agama_id' )
            ->where('calon_mahasiswa.calon_mahasiswa_id', '=', $id)
            ->orderBy('calon_mahasiswa_no_pendaftaran')
            ->first();

        return view('/admin/mahasiswa_form')
            ->with('newNIM', GlobalHelper::getNewNim($datas->prodiId, GlobalHelper::getYearId(GlobalHelper::getCurrentYear())))
            ->with('arrProdi', GlobalHelper::getProdiList())
            ->with('years', GlobalHelper::getYearList())
            ->with('arrAgama', GlobalHelper::getAgamaList())
            ->with('arrPekerjaan', GlobalHelper::getPekerjaanList())
            ->with('year', GlobalHelper::getCurrentYear("single"))
            ->with('mhs', $datas);
    }
}
