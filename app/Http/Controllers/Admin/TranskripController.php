<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Prodi;
use App\Models\TahunAkademik;
use Illuminate\Http\Request;
use App\Models\TranskripDetail;
use App\Helpers\GlobalHelper;
use App\Models\Mahasiswa;

class TranskripController extends Controller
{
    public function index()
    {

        $datas = Mahasiswa::getMahasiswaList(Prodi::getProdiId('Tata Busana'), TahunAkademik::getLatestYearId());

        $prodis = Prodi::getProdiList();
        $years = TahunAkademik::getYearList();

        return view('/admin/transkrip_list')
            ->with('datas', $datas)
            ->with('prodis', $prodis)
            ->with('years', $years)
            ->with('latestYear', GlobalHelper::getLatestYearId())
            ->with('i', 1);
    }

    public function getTranskripDetail($mhsId) {
        $datas = TranskripDetail::select('transcript_detail.id as transkrip_detail_id', 'courses.code as kode_mk',
            'courses.name as nama_mk', 'courses.credit as sks', 'transcript_detail.grade as nilai',
            'transcript_detail.gradecredit as sksxn', 'courses.semester as semester')
            ->leftjoin('courses', 'transcript_detail.courses_id', '=', 'courses.id')
            ->leftjoin('transcript', 'transcript.id', '=', 'transcript_detail.transcript_id')
            ->where('transcript.student_id', $mhsId)
            ->orderby('category_id')
            ->orderby('kode_mk')
            ->get();

        $totalSks = 0;
        $totalSksxn = 0;
        foreach ($datas as $data) {
            $totalSks = $totalSks + $data->sks;
            $totalSksxn = $totalSksxn + $data->sksxn;
        }
        $ipk = $totalSksxn / $totalSks;
        $ipk = round($ipk,2);

        $mhs = Mahasiswa::select('students.nim as nim', 'student_detail.name as nama', 'years.range as angkatan',
            'students.id as mahasiswa_id', 'study_programs.name as prodi')
            ->leftjoin('years', 'students.year_id', '=', 'years.id')
            ->leftjoin('student_detail', 'student_detail.student_id', '=', 'students.id')
            ->leftjoin('study_programs', 'study_programs.id', '=', 'students.prodi_id')
            ->where('students.id', '=', $mhsId)
            ->first();
        $nama = $mhs->nama;
        $nim = $mhs->nim;
        $angkatan = $mhs->angkatan;
        $prodi = $mhs->prodi_name;

        return view('/admin/transkrip_detail')
            ->with('datas', $datas)
            ->with('nama', $nama)
            ->with('nim', $nim)
            ->with('angkatan', $angkatan)
            ->with('totalSKS', $totalSks)
            ->with('totalSksxn', $totalSksxn)
            ->with('ipk', $ipk)
            ->with('prodi',$prodi )
            ->with('mahasiswa_id', $mhsId);

    }

    public function search(Request $request) {
        $prodiId = $request->prodiId;
        $yearId = $request->yearId;

        $datas = GlobalHelper::getMahasiswaList($prodiId, $yearId);

        return response ()-> json ($datas);
    }
}
