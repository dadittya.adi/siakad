<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\KurikulumDetail;
use App\Models\Prodi;
use App\Models\TahunAkademik;
use Illuminate\Http\Request;
use App\Models\Kurikulum;
use App\Helpers\GlobalHelper;
use Ramsey\Uuid\Uuid;

use Illuminate\Support\Facades\DB;

class KurikulumController extends Controller
{
     public function index()
    {


        $datas = Kurikulum::select(
            'curriculum.id as kurikulum_id', 'curriculum.name as kurikulum_nama', 'years.name as tahun',
            'study_programs.name as nama_prodi')
            ->leftjoin('study_programs', 'study_programs.id', '=', 'curriculum.prodi_id')
            ->leftjoin('years', 'years.id', '=', 'curriculum.year_id')
            ->orderBy('years.name','desc')
            ->where('curriculum.prodi_id', '=', Prodi::getProdiId('Tata Boga'))
            ->get();

        $prodis = Prodi::getProdiList();

        return view('/admin/kurikulum_list')
            ->with('datas', $datas)
            ->with('prodis', $prodis)
            ->with('prodiId', Prodi::getProdiId('Tata Boga'));
    }

    public function getKurikulumForJson(Request $request) {

        $datas = Kurikulum::select(
            'curriculum.id as kurikulum_id', 'curriculum.name as kurikulum_nama', 'years.name as tahun',
            'study_programs.name as nama_prodi')
            ->leftjoin('study_programs', 'study_programs.id', '=', 'curriculum.prodi_id')
            ->leftjoin('years', 'years.id', '=', 'curriculum.year_id')
            ->orderBy('years.name','desc')
            ->where('curriculum.prodi_id', '=', $request->prodiId)
            ->get();

        return response ()-> json ($datas);
    }

    public function getKurikulumDetail($id) {
        $datas = KurikulumDetail::select('curriculum_detail.id', 'courses.name as nama_mk', 'courses.code as kode_mk',
            'courses.credit as sks', 'courses.semester as semester')
            ->where('curriculum_id', '=', $id)
            ->leftjoin('courses', 'courses.id', '=', 'curriculum_detail.courses_id')
            ->orderby('courses.semester')
            ->orderby('courses.code')
            ->get();

        $info = Kurikulum::select('curriculum.id as kurikulum_id', 'curriculum.name as nama', 'study_programs.name as prodi', 'required_credit as sksWajib',
            'elective_credit as sksPilihan', 'years.range as tahun')
            ->leftjoin('years', 'years.id', '=', 'curriculum.year_id')
            ->leftjoin('study_programs', 'study_programs.id', '=', 'curriculum.prodi_id')
            ->where('curriculum.id', $id)
            ->first();

        return view('/admin/kurikulum_detail')
            ->with('datas', $datas)
            ->with('kurikulumInfo', $info);
    }

    public function copy($id) {
        $prodis = Prodi::getProdiList();
        $years = TahunAkademik::getYearList();

        $info = Kurikulum::select('id as kurikulum_id', 'required_credit as sksWajib',
            'elective_credit as sksPilihan', 'year_id', 'prodi_id')
            ->where('curriculum.id', $id)
            ->first();

        return view('/admin/kurikulum_form')
            ->with('prodis', $prodis)
            ->with('prodiId', $info->prodi_id)
            ->with('years', $years)
            ->with('yearId', TahunAkademik::getLatestYearId())
            ->with('info', $info);

    }

    public function store(Request $request) {

        DB::beginTransaction();

        try {
            $info = Kurikulum::select('prodi_id')
                ->where('id', '=', $request->kurikulum_id)
                ->first();

            $kurikulum = new Kurikulum();
            $kurikulum->id = UUid::uuid4()->getHex();
            $kurikulum->name = $request->nama;
            $kurikulum->prodi_id = $info->prodi_id;
            $kurikulum->year_id =$request->tahun;
            $kurikulum->required_credit = $request->required;
            $kurikulum->elective_credit = $request->elective;
            $kurikulum->save();

            $datas = KurikulumDetail::select('courses_id')
                ->where('curriculum_id', '=', $request->kurikulum_id)
                ->get();
            foreach ($datas as $data) {
                $kurikulum_detail = new KurikulumDetail();
                $kurikulum_detail->id = UUid::uuid4()->getHex();
                $kurikulum_detail->curriculum_id = $kurikulum->id;
                $kurikulum_detail->courses_id = $data->courses_id;
                $kurikulum_detail->save();
            }

            DB::commit();
            $datas = Kurikulum::select(
                'curriculum.id as kurikulum_id', 'curriculum.name as kurikulum_nama', 'years.name as tahun',
                'study_programs.name as nama_prodi')
                ->leftjoin('study_programs', 'study_programs.id', '=', 'curriculum.prodi_id')
                ->leftjoin('years', 'years.id', '=', 'curriculum.year_id')
                ->orderBy('years.name','desc')
                ->where('curriculum.prodi_id', '=', $kurikulum->prodi_id)
                ->get();

            $prodis = Prodi::getProdiList();

            return view('/admin/kurikulum_list')
                ->with('datas', $datas)
                ->with('prodis', $prodis)
                ->with('prodiId',  $kurikulum->prodi_id);
        }
        catch (\Exception $e) {
            DB::rollback();
            return view('/admin/import')
                ->with('datas', '')
                ->with('error', $e);
        }
    }
}
