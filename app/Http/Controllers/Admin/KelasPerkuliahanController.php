<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Helpers\GlobalHelper;
use App\Models\Prodi;
use App\Models\TahunAkademik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\KelasPerkuliahan;

class KelasPerkuliahanController extends Controller
{
    public function index()
    {
        $prodiId = Prodi::select('id')
                    ->orderby('name')
                    ->first();
        $prodiId = $prodiId->id;

        $datas = KelasPerkuliahan::select(
            'class.id as kelas_id', 'class.name as nama_kelas', 'courses.code as kode_mk',
            'courses.name as nama_mk', 'courses.credit as sks', 'lecturer.name as dosen',
            DB::raw('count(krs_detail.id) as peserta'), 'study_programs.name as nama_prodi')
            ->leftjoin('krs_detail', 'krs_detail.class_id', '=', 'class.id')
            ->leftjoin('study_programs', 'class.prodi_id', '=', 'study_programs.id')
            ->leftjoin('courses', 'class.courses_id', '=', 'courses.id')
            ->leftjoin('courses_lecturer', 'courses_lecturer.class_id', '=', 'class.id')
            ->leftjoin('lecturer', 'lecturer.id', '=', 'courses_lecturer.lecturer_id')
            ->where('class.prodi_id', '=', $prodiId)
            ->where('class.year_id', GlobalHelper::getLatestYearId())
            ->where('class.semester', '=', GlobalHelper::getLatestSemester())
            ->groupby('class.id')
            ->get();

        $prodis = Prodi::getProdiList();
        $years = TahunAkademik::getYearList();

        return view('/admin/kelas_list')
            ->with('datas', $datas)
            ->with('prodis', $prodis)
            ->with('prodiId', $prodiId)
            ->with('semester', GlobalHelper::getLatestSemester())
            ->with('years', $years)
            ->with('now', TahunAkademik::getLatestYearId());
    }

    public function getKelasForJson(Request $request) {
        $datas = KelasPerkuliahan::select(
            'class.id as kelas_id', 'class.name as nama_kelas', 'courses.code as kode_mk',
            'courses.name as nama_mk', 'courses.credit as sks', 'lecturer.name as dosen',
            DB::raw('count(krs_detail.id) as peserta'), 'study_programs.name as nama_prodi')
            ->leftjoin('krs_detail', 'krs_detail.class_id', '=', 'class.id')
            ->leftjoin('study_programs', 'class.prodi_id', '=', 'study_programs.id')
            ->leftjoin('courses', 'class.courses_id', '=', 'courses.id')
            ->leftjoin('courses_lecturer', 'courses_lecturer.class_id', '=', 'class.id')
            ->leftjoin('lecturer', 'lecturer.id', '=', 'courses_lecturer.lecturer_id')
            ->where('class.prodi_id', '=', $request->prodiId)
            ->where('class.year_id', $request->tahun)
            ->where('class.semester', '=', $request->semester)
            ->groupby('class.id')
            ->get();

        return response ()-> json ($datas);
    }
}
