<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Helpers\GlobalHelper;
use Illuminate\Http\Request;
use App\Models\Prodi;
use App\Models\Dosen;

class ProdiController extends Controller
{
    Public Function getNimCode($prodiId) {
        $datas = Prodi::select('nim_code')
            ->where('id', $prodiId)
            ->get();

        return $datas->prodi_kode_nim;
    }

    public function index() {
        $datas = Prodi::select(
            'study_programs.id as prodi_id', 'code as kode', 'study_programs.name as nama', 'lecturer.name as kaprodi')
            ->leftjoin('lecturer', 'study_programs.lecturer_id', '=', 'lecturer.id')
            ->get();

        return view('/admin/prodi_list')
            ->with('datas', $datas)
            ->with('dosens', Dosen::getDosenList());
    }

    public function getProdiData($prodiId) {
        $datas = Prodi::select('study_programs.id as prodi_id', 'code as kode', 'name as nama', 'lecturer_id as kaprodi',
            'nim_code as kodeNim', 'certificate_code as kodeIjazah', 'sk')
            ->where('study_programs.id', $prodiId)
            ->first();


        return response ()->json ( $datas );
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $prodi = Prodi::where('id', $id)->first();
        $prodi->code = $request->kode;
        $prodi->name = $request->nama;
        $prodi->lecturer_id = $request->kaprodi;
        $prodi->nim_code = $request->kodeNim;
        $prodi->certificate_code = $request->kodeIjazah;
        $prodi->sk = $request->sk;
        $prodi->update();

        //$this->getProdiData($id);

        $datas = Prodi::select('study_programs.id as prodi_id', 'code as kode', 'study_programs.name as nama', 'lecturer.name as kaprodi',
            'nim_code as kodeNim', 'certificate_code as kodeIjazah', 'sk')
            ->leftjoin('lecturer', 'study_programs.lecturer_id', '=', 'lecturer.id')
            ->where('study_programs.id', $id)
            ->first();


        return response ()->json ( $datas );
    }
}
