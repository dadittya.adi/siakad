<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use App\Models\Dosen;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use App\Models\Prodi;
use App\Http\Controllers\ProdiController;
use App\Helpers\GlobalHelper;
use App\Models\TahunAkademik;

class MahasiswaController extends Controller
{
    public function index()
    {
        $datas = Mahasiswa::select(
            'students.id as mahasiswa_id', 'student_detail.name as nama', 'students.nim as nim',
            'student_detail.gender as gender', 'religions.name as agama', DB::raw('date_format(students.dob,"%d/%m/%Y") as dob'),
            'study_programs.name as nama_prodi', 'years.name as angkatan', 'students.status as status')
            ->leftjoin('years', 'students.year_id', '=', 'years.id' )
            ->leftjoin('study_programs', 'students.prodi_id', '=', 'study_programs.id' )
            ->leftjoin('student_detail', 'students.id', '=', 'student_detail.student_id' )
            ->leftjoin('religions', 'student_detail.religion_id', '=', 'religions.id' )
            ->where('year_id', TahunAkademik::getLatestYearId())
            ->orderBy('students.nim')
            ->get();

        $prodis = Prodi::getProdiList();
        $years = TahunAkademik::getYearList();

        return view('/admin/mahasiswa_list')
            ->with('datas', $datas)
            ->with('prodis', $prodis)
            ->with('years', $years)
            ->with('latestYear', TahunAkademik::getLatestYearId())
            ->with('i', 1);

    }

    public function store(Request $request)
    {
        $rules = array (
            'mahasiswa_nama' => 'required',
            'mahasiswa_nim' => 'required'
        );
        $validator = Validator::make ( Input::all (), $rules );
        if ($validator->fails ())
            return Response::json ( array (

                'errors' => $validator->getMessageBag ()->toArray ()
            ) );
        else {
            $user_id = UUid::uuid4()->getHex();

            $username = $request->mahasiswa_nim;
            $password = str_replace('/', '', $request->plaindob);

            $user = new User();
            $user->user_id = $user_id;
            $user->username = $username;
            $user->name = $request->nama;
            $user->level = 'Mahasiswa';
            $user->password = bcrypt($password);
            $user->status = 1;
            $user->save();

            $mahasiswa = new Mahasiswa ();
            $mahasiswa->mahasiswa_id = Uuid::uuid4()->getHex();
            $mahasiswa->mahasiswa_nama = $request->nama;
            $mahasiswa->mahasiswa_nim = $request->mahasiswa_nim;
            $mahasiswa->mahasiswa_angkatan = $request->angkatan;
            $mahasiswa->mahasiswa_tempat_lahir = $request->tempatLahir;
            $mahasiswa->mahasiswa_dob = $request->dob;
            $mahasiswa->mahasiswa_gender = $request->gender;
            $mahasiswa->mahasiswa_agama = $request->agama;
            $mahasiswa->mahasiswa_status = 'Aktif';
            $mahasiswa->mahasiswa_alamat = $request->alamat;
            $mahasiswa->mahasiswa_alamat_kelurahan = $request->kelurahan;
            $mahasiswa->mahasiswa_alamat_kecamatan = $request->kecamatan;
            $mahasiswa->mahasiswa_alamat_kota = $request->kota;
            $mahasiswa->mahasiswa_alamat_propinsi = $request->propinsi;
            $mahasiswa->mahasiswa_alamat_kodepos = $request->kodepos;
            $mahasiswa->mahasiswa_phone_fixed = $request->phoneFixed;
            $mahasiswa->mahasiswa_phone_mobile = $request->phoneMobile;
            $mahasiswa->mahasiswa_asal_sekolah = $request->asalSekolah;
            $mahasiswa->mahasiswa_tahun_lulus = $request->tahunLulus;
            $mahasiswa->mahasiswa_nama_ibu = $request->namaIbu;
            $mahasiswa->mahasiswa_nama_ayah = $request->namaAyah;
            $mahasiswa->mahasiswa_pekerjaan_ayah = $request->pekerjaanAyah;
            $mahasiswa->save ();
            return response ()->json ( $mahasiswa );
        }
    }


    public function search(Request $request) {
        $mahasiswa = Mahasiswa::query();
        $mahasiswa = $mahasiswa->select(
            'students.id as mahasiswa_id', 'student_detail.name as nama', 'students.nim as nim',
            'student_detail.gender as gender', 'religions.name as agama', DB::raw('date_format(students.dob,"%d/%m/%Y") as dob'),
            'study_programs.name as nama_prodi', 'years.name as angkatan', 'students.status as status')
            ->leftjoin('years', 'students.year_id', '=', 'years.id' )
            ->leftjoin('study_programs', 'students.prodi_id', '=', 'study_programs.id' )
            ->leftjoin('student_detail', 'students.id', '=', 'student_detail.student_id' )
            ->leftjoin('religions', 'student_detail.religion_id', '=', 'religions.id' );

        $arrProdi = $request->prodi;
        $arrTahun = $request->tahun;

        if (count($arrProdi) > 0) {
            $mahasiswa = $mahasiswa->where(function ($query) use ($arrProdi){
                $query = $query->where('students.prodi_id', $arrProdi[0]);
                for ($i = 1; $i < count($arrProdi); $i++) {
                    $query = $query->orWhere('students.prodi_id', '=', $arrProdi[$i]);
                }
            });
        }

        if (count($arrTahun) > 0) {
            $mahasiswa = $mahasiswa->where(function ($query) use ($arrTahun){
                $query = $query->where('students.year_id', $arrTahun[0]);
                for ($i = 1; $i < count($arrTahun); $i++) {
                    $query = $query->orWhere('students.year_id', '=', $arrTahun[$i]);
                }
            });
        }

        $mahasiswa = $mahasiswa->orderBy('students.nim');
        return response() ->json($mahasiswa->get());

    }

}
