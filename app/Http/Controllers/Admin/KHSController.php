<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\KrsDetail;
use App\Models\Prodi;
use App\Models\TahunAkademik;
use Illuminate\Http\Request;
use App\Models\Krs;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\DB;
use App\Helpers\GlobalHelper;

class KHSController extends Controller
{
    private $krsId = '';

    public function index()
    {
        $result = KRS::query();
        $result = $result->select(
            'students.id as mahasiswa_id', 'student_detail.name as nama', 'students.nim as nim',
            'student_detail.gender as gender',
            'study_programs.name as nama_prodi', 'years.name as angkatan', 'students.status as status')
            ->leftJoin('students', 'krs.student_id', '=', 'students.id')
            ->leftjoin('years', 'students.year_id', '=', 'years.id' )
            ->leftjoin('study_programs', 'students.prodi_id', '=', 'study_programs.id' )
            ->leftjoin('student_detail', 'students.id', '=', 'student_detail.student_id' )
            ->where('krs.year_id', '=', TahunAkademik::getLatestYearId());
        if (GlobalHelper::getLatestSemester() == 'Genap') {
            $result = $result->whereRaw('krs.semester % 2 = 0');
        }
        else {
            $result = $result->whereRaw('krs.semester % 2 = 1');
        }

        $result = $result->orderBy('nim');
        $result = $result->get();

        $prodis = Prodi::getProdiList();
        $years = TahunAkademik::getYearList();

        return view('/admin/khs_list')
            ->with('datas', $result)
            ->with('prodis', $prodis)
            ->with('years', $years)
            ->with('latestYear', GlobalHelper::getLatestYearId())
            ->with('i', 1);

    }

    public function KHSDetailIndex($mhsId) {
        $semesters = Krs::select('krs.semester as krs_semester', 'krs.id as krs_id')
            ->leftjoin('krs_detail', 'krs.id', '=', 'krs_detail.krs_id')
            ->where('student_id', $mhsId)
            ->where('grade', '<>', '')
            ->groupby('krs.semester')
            ->get();

        $krsId = $this->getKrsId($mhsId);
        $datas = $this->getKrSdetail($krsId);
        $info = $this->getKrsInfo($krsId);

        return view('/admin/khs_detail')
            ->with('datas', $datas)
            ->with('semesters', $semesters)
            ->with('krsInfo', $info)
            ->with('id', $krsId);
    }

    public function  getKhsdetailforJson(Request $request) {
        $krsId = $request->krsId;

        $datas = $this->getKrsDetail($krsId)->toArray();
        $info = $this->getKrsInfo($krsId)->toArray();

        return response()->json(array('datas'=>$datas, 'krsInfo'=>$info));
    }

    public function getKrsId($mhsId, $semester = 0) {
        if ($semester == 0) {
            $result = KRS::select('krs.id as id', 'semester')
                ->leftjoin('krs_detail', 'krs_detail.krs_id', '=', 'krs.id')
                ->where('krs_detail.grade', '<>', '' )
                ->where('krs.student_id', '=', $mhsId)
                ->orderby('krs.semester', 'desc')
                ->first();
            $krsId = $result->id;
        }
        else {
            $result = KRS::select('krs.id as id')
                ->where('krs.semester', '=', $semester)
                ->where('student_id', '=', $mhsId)
                ->orderby('krs.semester')
                ->first();
            $krsId = $result->id;
        }
        return $krsId;
    }

    public function getKrsDetail($krsId) {
        $datas = KrsDetail::select('krs_detail.id as krs_detail.id', 'courses.code as kode_mk', 'courses.name as nama_mk',
            'courses.credit as sks', 'krs_detail.grade as nilai', 'krs.credit_total as krs_sks', 'last_gp as krs_ip_lalu', 'gradecredit as sksxn')
            ->leftjoin('krs', 'krs.id', '=', 'krs_detail.krs_id')
            ->leftjoin('class', 'class.id', '=', 'krs_detail.class_id')
            ->leftjoin('courses', 'class.courses_id', '=', 'courses.id')
            ->where('krs_detail.krs_id', $krsId)
            ->orderby('courses.code')
            ->get();

        return $datas;
    }

    public function search(Request $request) {
        $prodiId = $request->prodiId;
        $yearId = $request->yearId;
        $semester = $request->semester;

       $result = $this->getKhsList($yearId, $prodiId, $semester);
       $result = $result->groupBy('krs.mahasiswa_id');

        return response ()-> json ($result->distinct()->get());
    }

    public function getKhsList($yearId, $prodiId, $semester) {
        $result = KRS::query();
        $result = $result->select(
            'krs.id as krs_id', 'students.id as mahasiswa_id', 'student_detail.name as nama', 'students.nim as nim',
            'student_detail.gender as gender', 'krs.semester as semester', 'last_gp as ip_lalu', 'dWali.name as nama_wali',
            'xdProdi.name as kaprodi', 'transcript.credit', 'transcript.gpa',
            'study_programs.name as nama_prodi', 'years.range as angkatan', 'students.status as status')
            ->leftJoin('students', 'krs.student_id', '=', 'students.id')
            ->leftjoin('transcript', 'students.id', '=', 'transcript.student_id')
            ->leftJoin('lecturer as dWali', 'mahasiswa.lecturer_id', '=', 'dWali.id')
            ->leftjoin('years', 'students.year_id', '=', 'years.id' )
            ->leftjoin('study_programs', 'students.prodi_id', '=', 'study_programs.id' )
            ->leftJoin('lecturer as dProdi', 'study_programs.lecturer_id', 'dProdi.id')
            ->leftjoin('student_detail', 'students.id', '=', 'student_detail.student_id' );

        if (is_array($prodiId)) {
            if (count($prodiId) > 0) {
                $result = $result->where(function ($query) use ($prodiId){
                    $query = $query->where('students.prodi_id', $prodiId[0]);
                    for ($i = 1; $i < count($prodiId); $i++) {
                        $query = $query->orWhere('students.prodi_id', '=', $prodiId[$i]);
                    }
                });
            }
        }
        else {
            if ($prodiId != '') {
                $result = $result->where('students.prodi_id', '=', $prodiId);
            }

        }


        if (is_array($yearId)) {
            if (count($yearId) > 0) {
                $result = $result->where(function ($query) use ($yearId){
                    $query = $query->where('students.year_id', $yearId[0]);
                    for ($i = 1; $i < count($yearId); $i++) {
                        $query = $query->orWhere('students.year_id', '=', $yearId[$i]);
                    }
                });
            }
        }
        else {
            if ($yearId != '') {
                $result = $result->where('krs.year_id', '=', $yearId);
            }
            else {
                $result = $result->where('krs.year_id', '=', GlobalHelper::getLatestYearId());
            }
        }

        if (is_array($semester)) {
            if (count($semester) > 0) {
                if ($semester[0] == 'Genap') {
                    $result = $result->whereRaw('krs.semester % 2 = 0');
                }
                else {
                    $result = $result->whereRaw('krs.semester % 2 = 1');
                }
                for ($i = 1; $i < count($semester); $i++) {
                    if ($semester[i] == 'Genap') {
                        $result = $result->orwhereRaw('krs.semester % 2 = 0');
                    }
                    else {
                        $result = $result->orwhereRaw('krs.semester % 2 = 1');
                    }
                }
            }
        }
        else {
            if (GlobalHelper::getLatestSemester() == 'Genap') {
                $result = $result->whereRaw('krs.semester % 2 = 0');
            }
            else {
                $result = $result->whereRaw('krs.semester % 2 = 1');
            }
            for ($i = 1; $i < count($semester); $i++) {
                if ($semester[i] == 'Genap') {
                    $result = $result->orwhereRaw('krs.semester % 2 = 0');
                }
                else {
                    $result = $result->orwhereRaw('krs.semester % 2 = 1');
                }
            }
        }

        $result = $result->orderBy('students.nim');

        return $result;

    }

    public function getKrsInfo($krsId) {
        $result = Krs::select('students.nim as nim', 'students.id', 'krs.id as krs_id', 'student_detail.name as nama',
            'krs.credit_next as allowed_sks', 'krs.semester as semester', 'last_gp as ip_lalu', 'dWali.name as nama_wali',
            'gp as ip_semester', 'credit_total as total_sks',
            'dProdi.name as kaprodi', 'transcript.credit as sks_transkrip', 'transcript.gpa as ipk',
            'study_programs.name as nama_prodi', 'tm.name as angkatan', 'tk.range as tahun_akademik')
            ->leftJoin('students', 'krs.student_id', '=', 'students.id')
            ->leftjoin('student_detail', 'students.id', '=', 'student_detail.student_id' )
            ->leftjoin('transcript', 'students.id', '=', 'transcript.student_id')
            ->leftJoin('lecturer as dWali', 'students.lecturer_id', '=', 'dWali.id')
            ->leftjoin('years as tm', 'students.year_id', '=', 'tm.id' )
            ->leftjoin('years as tk', 'krs.year_id', '=', 'tk.id' )
            ->leftjoin('study_programs', 'students.prodi_id', '=', 'study_programs.id' )
            ->leftJoin('lecturer as dProdi', 'study_programs.lecturer_id', 'dProdi.id')
            ->where('krs.id', '=', $krsId)
            ->first();

        return $result;
    }

    public function print_all($yearId, $prodiId, $semester) {
        $result = $this->getKhsList($yearId, $prodiId, $semester);


    }

    public function print_single($krsId) {
        $krsInfo = $this->getKrsInfo($krsId);
        $krsDetail = $this->getKrsDetail($krsId);

        return view('/admin/khs_print_single')
            ->with('krsInfo', $krsInfo)
            ->with('arrKrsDetail', $krsDetail)
            ->with('tanggal', GlobalHelper::tanggal_indo(date('Y-m-d')));

    }
}
