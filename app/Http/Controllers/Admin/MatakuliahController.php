<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\JenisMK;
use App\Models\KelompokMK;
use App\Models\Prodi;
use Illuminate\Http\Request;
use App\Helpers\GlobalHelper;
use App\Models\matakuliah;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class MatakuliahController extends Controller
{
    public function index()
    {
        $datas = Matakuliah::select(
            'courses.id as mk_id','courses.name as nama_mk', 'courses.code as kode_mk', 'courses.credit as sks',
            'study_programs.name as nama_prodi', 'courses_type.name as jenisMk', 'courses.name as kelompokMk')
            ->leftjoin('study_programs', 'courses.prodi_id', '=', 'study_programs.id')
            ->leftjoin('courses_type', 'courses.type_id', '=', 'courses_type.id')
            ->leftjoin('courses_category', 'courses.category_id', '=', 'courses_category.id')
            ->groupby('courses.code')
            ->groupby('courses.name')
            ->groupby('courses.credit')
            ->groupby('study_programs.name')
            ->groupby('courses_type.name')
            ->groupby('category_id')
            ->orderBy('courses.code')
            ->get();

        $prodis = Prodi::getProdiList();

        $jenisMk = JenisMK::getJenisMkList();

        $kelompokMk = KelompokMK::getKelompokMkList();

        return view('/admin/matakuliah_list')
            ->with('datas', $datas)
            ->with('arrProdi', $prodis)
            ->with('arrJenisMk', $jenisMk)
            ->with('arrKelompokMk', $kelompokMk)
            ->with('arrMK', $datas);
    }

    public function store(Request $request)
    {

            DB::beginTransaction();
            try {
                $matakuliah = new Matakuliah();

                $matakuliah->name = $request->nama;
                $matakuliah->code = $request->kode;
                $matakuliah->credit = $request->sks;
                $matakuliah->type_id = $request->jenis;
                $matakuliah->category_id = $request->kelompok;
                $matakuliah->semester = $request->semester;
                $matakuliah->prodi_id = $request->prodi;
                $matakuliah->prerequisite_course = $request->prasyarat;

                $matakuliah->save();

                $result = Matakuliah::select('matakuliah_id')
                    ->orderby('matakuliah_id', 'desc')
                    ->first();
                $mkId = $result->matakuliah_id;
                DB::commit();

                return response()->json($this->getMkData($mkId));
            }
            catch (\Exception $e) {
                DB::rollback();
                return response()->json($e);
            }



    }

    public function edit(Request $request) {
        DB::beginTransaction();
        try {
            $id = $request->id;
            $matakuliah = Matakuliah::where('id', $id)->first();

            $matakuliah->name = $request->nama;
            $matakuliah->code = $request->kode;
            $matakuliah->credit = $request->sks;
            $matakuliah->type_id = $request->jenis;
            $matakuliah->caegory_id = $request->kelompok;
            $matakuliah->semester = $request->semester;
            $matakuliah->prodi_id = $request->prodi;
            $matakuliah->prerequisite_course = $request->prasyarat;

            $matakuliah->update();

            DB::commit();
            return response()->json($this->getMkData($id));
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json($e);
        }
    }

    public function show($id) {
        $matakuliah = $this->getMkData($id);
        return response()->json($matakuliah);
    }
    public function getMkData($id) {
        $result = Matakuliah::select(
            'courses.id as mk_id','courses.name as nama_mk', 'courses.code as kode_mk', 'courses.credit as sks',
            'study_programs.name as nama_prodi', 'courses_type.name as jenisMk', 'courses_category.name as kelompokMk', 'courses.semester as semester',
            'courses.type_id as jenisMkId', 'courses.category_id as kelompokMkId', 'courses.prodi_id as prodiId')
            ->leftjoin('study_programs', 'courses.prodi_id', '=', 'study_programs.id')
            ->leftjoin('courses_type', 'courses.type_id', '=', 'courses_type.id')
            ->leftjoin('courses_category', 'courses.category_id', '=', 'courses_category.id')
            ->where('courses.id', '=', $id)
            ->first();

        return $result;
    }
}
