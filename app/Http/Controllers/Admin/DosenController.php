<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Helpers\GlobalHelper;
use App\Models\Agama;
use App\Models\Prodi;
use App\Models\StatusDosen;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Dosen;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class DosenController extends Controller
{
    public function index()
    {
        $datas = Dosen::select('lecturer.id as dosen_id', 'lecturer.name as name', 'nidn', 'nid', 'nip', 'gender', 'religions.name as agama', DB::raw('date_format(dob,"%d/%m/%Y") as dob'),
            'lecturer_status.name as status')
            ->leftjoin('religions', 'religion_id', '=', 'religions.id' )
            ->leftjoin('lecturer_status', 'lecturer.status_id', '=', 'lecturer_status.id')
            ->where('lecturer_status.name', '<>', 'Tidak Aktif')
            ->where('lecturer_status.name', '<>', 'Keluar')
            ->where('lecturer_status.name', '<>', 'Almarhum')
            ->orderBy('lecturer.name')
            ->get();

        return view('/admin/dosen')
            ->with('datas', $datas)
            ->with('arrStatus', StatusDosen::getDosenStatusList())
            ->with('arrAgama', Agama::getAgamaList())
            ->with('arrProdi', Prodi::getProdiList());

    }

    public function store(Request $request)
    {
        $rules = array (
            'nama' => 'required',
            'gender' => 'required',
            'dob' => 'required'
        );
        $validator = Validator::make ( Input::all (), $rules );
        if ($validator->fails ())
            return Response::json ( array (

                'errors' => $validator->getMessageBag ()->toArray ()
            ) );
        else {
            DB::beginTransaction();
                try {
                    $user_id = UUid::uuid4()->getHex();
                    $username = str_replace(' ', '', $request->nama);
                    $username = substr($username, 0, 8);
                    $password = str_replace('/', '', $request->plaindob);

                    $user = new User();
                    $user->user_id = $user_id;
                    $user->usenrname = $username;
                    $user->name = $request->nama;
                    $user->level = 'Dosen';
                    $user->password = bcrypt($password);
                    $user->status = 1;
                    $user->save();

                    $dosen = new Dosen ();
                    $dosen->id = Uuid::uuid4()->getHex();
                    $dosen->user_id = $user_id;
                    $dosen->name = $request->nama;
                    $dosen->nidn = $request->nidn;
                    $dosen->nip = $request->nip;
                    $dosen->alamat = $request->alamat;
                    $dosen->phone_fixed = $request->fixedPhone;
                    $dosen->phone_mobile = $request->mobilePhone;
                    $dosen->email = $request->email;
                    $dosen->dob_place = $request->tempatLahir;
                    $dosen->dob = $request->dob;
                    $dosen->type = $request->ikatan;
                    $dosen->prodi_id = $request->prodi;
                    $dosen->gender = $request->gender;
                    $dosen->religion_id = $request->agama;
                    $dosen->status_id = $request->status;

                    $dosen->save();

                    DB::commit();
                    return response()->json($this->getDosenData($dosen->id));



                }
            catch (\Exception $e) {
                DB::rollback();
            }

        }
    }

    public function edit(Request $request) {

        DB::beginTransaction();
        try {
            $id = $request->id;
            $dosen = Dosen::where('dosen_id', $id)->first();
            $dosen->nama = $request->nama;
            $dosen->nidn = $request->nidn;
            $dosen->nip = $request->nip;
            $dosen->alamat = $request->alamat;
            $dosen->fixed_phone = $request->fixedPhone;
            $dosen->mobile_phone = $request->mobilePhone;
            $dosen->email = $request->email;
            $dosen->tempat_lahir = $request->tempatLahir;
            $dosen->dob = $request->dob;
            $dosen->tipe_dosen = $request->ikatan;
            $dosen->prodi_id = $request->prodi;
            $dosen->gender = $request->gender;
            $dosen->agama_id = $request->agama;
            $dosen->status_dosen_id = $request->status;
            $dosen->update ();
            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            return response ()->json ( $request );
        }


        return response ()->json ( $this->getDosenData($id) );
    }

    public function show($id)
    {
        $dosen = $this->getDosenData($id);
        return response ()->json ( $dosen );
    }

    public function search(Request $request)
    {
        $gender = $request->gender;
        $status = $request->status;
        $agama = $request->agama;

        $dosen = Dosen::query();
        $dosen = $dosen->select('lecturer.id as dosen_id', 'lecturer.name as name', 'nidn', 'nid', 'nip', 'gender', 'religions.name as agama', DB::raw('date_format(dob,"%d/%m/%Y") as dob'),
            'lecturer_status.name as status');
        $dosen = $dosen
            ->leftjoin('religions', 'religion_id', '=', 'religions.id' )
            ->leftjoin('lecturer_status', 'lecturer.status_id', '=', 'lecturer_status.id');

        if ($gender <> '0') {
            $dosen  = $dosen->where('gender', '=', $gender);
        }

        if ($status <> '0') {
            $dosen = $dosen->where('status_id', '=', $status);
        }

        if ($agama <> '0') {
            $dosen = $dosen->where('religion_id', '=', $agama);
        }

        $dosen = $dosen->orderBy('lecturer.name');

        return response ()-> json ($dosen->get());
    }

    public function getDosenData($id) {
        $dosen = Dosen::select('lecturer.id as dosen_id', 'lecturer.name', 'nid', 'nidn', 'nip', 'gender', 'alamat', 'phone_fixed as fixedPhone',
            'phone_mobile as mobilePhone', 'email', 'dob_place', 'type as ikatan', 'prodi_id', 'religion_id as agamaId',
            'religions.name as agama', DB::raw('date_format(dob,"%d/%m/%Y") as dob'),
            'lecturer_status.name as status', 'status_id as statusId')
            ->leftjoin('religions', 'religion_id', '=', 'religions.id' )
            ->leftjoin('lecturer_status', 'lecturer.status_id', '=', 'lecturer_status.id')
            ->where('lecturer.id', '=', $id)
            ->first();
        return $dosen;
    }
}
