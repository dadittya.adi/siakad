<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Agama;
use App\Models\GolonganMahasiswa;
use App\Models\JenisMK;
use App\Models\KelasPengajar;
use App\Models\KelompokMK;
use App\Models\Pekerjaan;
use App\Models\StatusDosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dosen;
use App\Models\User;
use App\Models\siakad_dosen;
use App\Models\siakad_mk;
use App\Models\siakad_mhs;
use App\Models\siakad_aktivitas_mhs;
use App\Models\Kurikulum;
use App\Models\Prodi;
use App\Models\TahunAkademik;
use App\Models\Mahasiswa;
use App\Models\Matakuliah;
use App\Models\MahasiswaDetail;
use App\Models\KurikulumDetail;
use App\Models\KelasPerkuliahan;
use App\Models\siakad_krs;
use App\Models\siakad_krs_detail;
use App\Models\Krs;
use App\Models\KrsDetail;
use App\Models\Transkrip;
use App\Models\TranskripDetail;
use App\Helpers\GlobalHelper;
use App\Models\siakad_ijazah;
use App\Models\Grade;
use App\Models\Pendidikan;
use Ramsey\Uuid\Uuid;

class import_db_controller extends Controller
{

    public function importKRS() {
        ini_set('max_execution_time', 600);
        DB::beginTransaction();

        try {
            $superadminId = $this->getSuperadminId();

            $krsDatas = siakad_krs::select('nim', 'semester', 'tahun', 'sks', 'ip_lalu', 'tanggal')
                ->orderBy('nim')
                ->orderBy('semester')
                ->get();

            foreach ($krsDatas as $krsSource) {

                $Mahasiswa = new Mahasiswa();
                $mhsId = $Mahasiswa->getMhsId($krsSource->nim);
                $tahunId = $this->getTahunId($krsSource->tahun);
                $krsId = UUid::uuid4()->getHex();

                $krs = new Krs();
                $krs->id = $krsId;
                $krs->date = $krsSource->tanggal;
                $krs->semester = $krsSource->semester;
                $krs->status = 'OK';
                $krs->credit_next = $krsSource->sks;
                $krs->last_gp = $krsSource->ip_lalu;
                $krs->student_id = $mhsId;
                $krs->year_id = $tahunId;
                $krs->edited_by = $superadminId;
                $krs->save();

                $result = Transkrip::select('transcript.id as id')
                    ->leftjoin('students', 'students.id', '=', 'transcript.student_id')
                    ->where('nim', $krsSource->nim)
                    ->first();
                if (!$result) {
                    $result2 = siakad_ijazah::select('nomor_transkrip', 'tanggal_transkrip')
                        ->where('nim', $krsSource->nim)
                        ->first();
                    $transkrip = new Transkrip();
                    $transkrip->id = UUid::uuid4()->getHex();
                    $transkrip->student_id = $mhsId;
                    if ($result2) {
                        $transkrip->code = $result2->nomor_transkrip;
                        $transkrip->date = $result2->tanggal_transkrip;
                    }
                    $transkrip->save();

                    $result = Transkrip::select('transcript.id as id')
                        ->leftjoin('students', 'students.id', '=', 'transcript.student_id')
                        ->where('nim', $krsSource->nim)
                        ->first();
                    $transkripId = $result->id;
                }
                else {
                    $transkripId = $result->id;
                }

                $krsDetails = siakad_krs_detail::select('kode_mk', 'matakuliah_nama', 'tahun', 'nilai')
                    ->leftjoin('siakad_mk', function ($join) {
                        $join->on('siakad_mk.matakuliah_kode', '=', 'siakad_krs_detail.kode_mk');
                        $join->on('siakad_mk.tahun_akademik', '=', 'siakad_krs_detail.tahun');
                    })
                    ->where('nim', $krsSource->nim)
                    ->where('semester', $krsSource->semester)
                    ->orderBy('semester')
                    ->get();

                foreach ($krsDetails as $dataDetail) {
                    $prodi = $this->getMhsProdi($mhsId);
                    $kelas = KelasPerkuliahan::select('class.id as kelas_id', 'courses.id as mkId',
                        'courses.credit as sks')
                        ->leftjoin('courses', 'class.courses_id', '=', 'courses.id')
                        ->where('courses.code', $dataDetail->kode_mk)
                        ->where('courses.name', $dataDetail->matakuliah_nama)
                        ->where('class.year_id', $this->getTahunId($dataDetail->tahun))
                        ->where('class.prodi_id', '=', $prodi)
                        ->first();


                    if (!$kelas) {
                        //$kelas = $this->insertKelas($prodi,$dataDetail->kode_mk, $dataDetail->tahun, $superadminId);
                       $mk = siakad_mk::select('matakuliah_semester', 'matakuliah_sks', 'matakuliah_kode', 'tahun_akademik', 'dosen1', 'dosen2', 'dosen3', 'dosen4', 'matakuliah_sks')
                            ->where('tahun_akademik', $dataDetail->tahun)
                            ->where('matakuliah_kode', '=', $dataDetail->kode_mk)
                            ->first();

                        $dosenId[0] = $this->getDosenId($mk->dosen1);
                        $dosenId[1] = $this->getDosenId($mk->dosen2);
                        $dosenId[2] = $this->getDosenId($mk->dosen3);
                        $dosenId[3] = $this->getDosenId($mk->dosen4);

                        if ((!$mk->matakuliah_sks) or (!$mk->matakuliah_semester))
                        {
                            $result2 = Matakuliah::select('id')
                                ->where('courses.code', '=',  $dataDetail->kode_mk)
                                ->where('courses.name', '=', $dataDetail->matakuliah_nama)
                                ->where('courses.credit', '=', $mk->matakuliah_sks)
                                ->where('courses.semester', '=', $mk->matakuliah_semester)
                                ->first();
                        }
                        else {
                            $result2 = Matakuliah::select('id')
                                ->where('courses.code', '=', $dataDetail->kode_mk)
                                ->where('courses.name', '=', $dataDetail->matakuliah_nama)
                                ->first();
                        }

                        $id_mk = $result2->id;
                        /*if(!$result) {
                            $result = Matakuliah::select('matakuliah_id')
                                ->where('matakuliah_kode', '=', $dataDetail->kode_mk)
                                ->where('matakuliah_sks', '=', $mk->matakuliah_sks)
                                ->first();
                            $id_mk = $result->matakuliah_id;
                        }
                        else {
                            $id_mk = $result->matakuliah_id;
                        }*/


                        $kelasId = UUid::uuid4()->getHex();
                        $kelas = new KelasPerkuliahan();
                        $kelas->id = $kelasId;
                        $kelas->name = '01';
                        $kelas->year_id = $this->getTahunId($dataDetail->tahun);
                        $kelas->courses_id = $id_mk;
                        $kelas->status = 'Aktif';
                        $kelas->prodi_id = $prodi;
                        if ($krsSource->semester % 2 == 0) {
                            $kelas->semester = 'Genap';
                        } else {
                            $kelas->semester = 'Ganjil';
                        }
                        $kelas->edited_by = $superadminId;
                        $kelas->save();




                        for ($i = 0; $i < 4; $i++) {

                            if ($dosenId[$i] <> '0') {
                                $pengajar = new KelasPengajar();
                                $pengajar->id = UUid::uuid4()->getHex();
                                $pengajar->class_id = $kelasId;
                                $pengajar->lecturer_id = $dosenId[$i];
                                $pengajar->save();
                            }
                        }

                        $mkId = $id_mk;
                        $sks = $result2->sks;
                    }
                    else {
                        $kelasId = $kelas->kelas_id;
                        $mkId = $kelas->mkId;
                        $sks = $kelas->sks;
                    }




                    $krsDetail = new KrsDetail();
                    $krsDetail->id = UUid::uuid4()->getHex();
                    $krsDetail->grade = $dataDetail->nilai;
                    $krsDetail->krs_id = $krsId;
                    $krsDetail->class_id = $kelasId;
                    $krsDetail->save();

                    if ($dataDetail->nilai) {
                        $result = TranskripDetail::select('id', 'grade as nilai')
                            ->where('transcript_id', $transkripId)
                            ->where('courses_id', $mkId)
                            ->first();

                        $grades = Grade::select('number')
                            ->where('grade', '=', $dataDetail->nilai)
                            ->first();
                        $grade = $grades->number;
                        $sksxn = $grade * $sks;

                        if (!$result) {
                            $transkripDetail = new TranskripDetail();
                            $transkripDetail->id = UUid::uuid4()->getHex();
                            //var_dump($mkId); die;
                            $transkripDetail->courses_id = $mkId;
                            $transkripDetail->grade = $dataDetail->nilai;
                            $transkripDetail->transcript_id = $transkripId;
                            $transkripDetail->gradecredit = $sksxn;
                            $transkripDetail->save();
                        }
                        else {
                            if (GlobalHelper::getGradeValue($dataDetail->nilai) > GlobalHelper::getGradeValue($result->nilai)) {
                                $transkripDetail = TranskripDetail::where('id', $result->id)->first();
                                $transkripDetail->grade = $dataDetail->nilai;
                                $transkripDetail->gradecredit = $sksxn;
                                $transkripDetail->update();
                            }
                        }
                    }

                }
                /*$transkrips = Transkrip::select('transkrip_id', 'mahasiswa_id')
                    ->get();
                foreach ($transkrips as $transkrip) {
                    $update = Transkrip::where('transkrip_id', $transkrip->transkrip_id)->first();
                    $update->ipk = GlobalHelper::recalculateIpk($transkrip->mahasiswa_id);
                    $update->update();
                }*/

            }

            DB::commit();
            return view('/admin/import')
                    ->with('datas', $krsDatas)
                    ->with('error', 'none');

        }
        catch (\Exception $e) {
                DB::rollback();
                return view('/admin/import')
                    ->with('datas', '')
                    ->with('error', $e);
        }

    }

    public function importMatakuliah() {
        DB::beginTransaction();

        try {
            $superadminId = $this->getSuperadminId();

            $kurikulums = Kurikulum::select('id', 'prodi_id', 'year_id')
                ->get();

            foreach ($kurikulums as $kurikulum) {

                $prodiId = Prodi::select('other_id')
                        ->where('id', '=', $kurikulum->prodi_id)
                        ->first();
                $prodiId = $prodiId->other_id;

                $year = new TahunAkademik();


                $datas = siakad_mk::select('matakuliah_kode', 'matakuliah_prasyarat', 'matakuliah_nama', 'tahun_akademik', 'prodi_id',
                    'matakuliah_sks', 'matakuliah_semester', 'kelompok_mk_id', 'dosen1', 'dosen2', 'dosen3', 'dosen4')
                    ->where('tahun_akademik', $year->getYearString($kurikulum->year_id))
                    ->where(function ($query) use ($prodiId){
                        $query->where('prodi_id', $prodiId)
                            ->orWhere('prodi_id', '0');
                    })

                    ->orderBy('matakuliah_kode')
                    ->get();

                foreach ($datas as $data) {
                    $isExsist = Matakuliah::select('id')
                        ->where('code', $data->matakuliah_kode)
                        ->where('credit', $data->matakuliah_sks)
                        ->where('semester', $data->matakuliah_semester)
                        ->where('name', $data->matakuliah_nama)
                        ->first();

                    if (!$isExsist) {
                        $insert = true;

                        if ($data->prodi_id == '0') {
                            $result = Matakuliah::select('id')
                                ->where('code', $data->matakuliah_kode)
                                ->where('credit', $data->matakuliah_sks)
                                ->where('semester', $data->matakuliah_semester)
                                ->where('name', $data->matakuliah_nama)
                                ->where('prodi_id', '')
                                ->first();
                            if ($result) {
                                $insert = false;
                            }
                        }

                        if ($insert) {
                            $mkId =  UUid::uuid4()->getHex();
                            $prodi = Prodi::select('id')
                                    ->where('other_id', '=',$data->prodi_id)
                                    ->first();
                            if (!$prodi) {
                                $prodi = '';
                            }
                            else {
                                $prodi = $prodi->id;
                            }
                            $cat = KelompokMK::select('id')
                                    ->where('other_id', '=', $data->kelompok_mk_id)
                                    ->first();
                            $type = JenisMK::select('id')
                                ->where('other_id', '=', '1')
                                ->first();
                            $matakuliah = new Matakuliah();
                            $matakuliah->id = $mkId;
                            $matakuliah->code = $data->matakuliah_kode;
                            $matakuliah->name = $data->matakuliah_nama;
                            $matakuliah->prodi_id = $prodi;
                            $matakuliah->category_id = $cat->id;
                            $matakuliah->type_id = $type->id;
                            $matakuliah->credit = $data->matakuliah_sks;
                            $matakuliah->semester = $data->matakuliah_semester;
                            $matakuliah->prerequisite_course = '';
                            $matakuliah->edited_by = $superadminId;
                            $matakuliah->year_id = $this->getTahunId($data->tahun_akademik);
                            $matakuliah->save();
                        }

                    }
                    else {
                        $mkId = $isExsist->id;
                    }

                    $kurikulum_detail = new KurikulumDetail();
                    $kurikulum_detail->id = UUid::uuid4()->getHex();
                    $kurikulum_detail->curriculum_id = $kurikulum->id;
                    $kurikulum_detail->courses_id = $mkId;
                    $kurikulum_detail->save();

                }
            }
            DB::commit();
            return view('/admin/import')
                ->with('datas', $kurikulums)
                ->with('error', 'none');

        }
        catch (\Exception $e) {
            DB::rollback();
            return view('/admin/import')
                ->with('datas', '')
                ->with('error', $e);
        }catch (\Exception $e) {
            DB::rollback();
            return view('/admin/import')
                ->with('datas', '')
                ->with('error', $e);
        }
    }

    public function getTahunId($tahun) {
        $result = TahunAkademik::select('id')
            ->where('name', $tahun)
            ->orWhere('range', $tahun)
            ->first();
        if ($result) {
            return $result->id;
        }
        else {
            return '';
        }
    }

    public function getKurikulumId($tahun)
    {
        $result = Kurikulum::select('id')
            ->where('year_id', $tahun)
            ->first();
        if ($result) {
            return $result->id;
        } else {
            return 0;
        }
    }

    public function getDosenId($nid) {
            $result = Dosen::select('id')
                ->where('nid', $nid)
                ->first();
            if ($result) {
                return $result->id;
            }
            else {
                return '0';
        }
    }

    public function importMahasiswa() {
        //ini_set('max_execution_time', 300);
        DB::beginTransaction();
        try {
            $superadminId = $this->getSuperadminId();

            $datas = siakad_mhs::select('nim', 'nama', 'alamat', 'tel', 'hp', 'email', 'tempat_lahir', 'dob', 'gol_darah',
                'gender', 'warga_negara', 'agama', 'status_kerja', 'status_kawin', 'gol_mhs', 'ijazah_SLA', 'th_ijazah_SLA',
                'asal_SLA', 'status_pindah', 'nama_pt_asal', 'tahun_masuk', 'nama_ortu', 'alamat_ortu', 'propinsi', 'ijazah_ayah',
                'ijazah_ibu', 'pekerjaan_ayah', 'pekerjaan_ibu', 'prodi_id', 'nid', DB::raw('date_format(dob,"%d/%m/%Y") as dobForPass'))
               ->orderBy('tahun_masuk')
               ->orderBy('nim')
                ->get();

            foreach ($datas as $data) {

                $tahun_id = $this->getTahunId($data->tahun_masuk);

                $kurikulum_id = $this->getKurikulumId($this->getTahunId($data->tahun_masuk));

                $dosenId = $this->getDosenId($data->nid);

                $prodiId = Prodi::select('id')
                        ->where('other_id', '=', $data->prodi_id)
                        ->first();
                $prodiId = $prodiId->id;

                $result = Mahasiswa::select('nim')
                    ->where('nim', $data->nim)
                    ->first();
                if (!$result) {

                    $status = 'Aktif';

                    $result = siakad_aktivitas_mhs::select('siakad_aktivitas_mhs_id', 'Aktivitas_Kuliah as status')
                        ->where('nim', $data->nim)
                        ->orderBy('siakad_aktivitas_mhs_id', 'desc')
                        ->first();

                    $user_id = '0';
                    if ($result) {
                        $status = $result->status;
                        if ($status == '0') {
                            $status = 'Aktif';
                        }
                        elseif ($status == '1') {
                            $status = 'Lulus';
                        }
                        elseif ($status == '2') {
                            $status = 'Keluar';
                        }
                        elseif ($status == '3') {
                            $status = 'Cuti';
                        }
                        elseif ($status == '4') {
                            $status = 'Non Aktif';
                        }

                        if (($status != 'Lulus') && ($status != 'Keluar')) {
                            $user_id = UUid::uuid4()->getHex();
                            $username = $data->nim;
                            $password = bcrypt(str_replace('/', '', $data->dobForPass));

                            $user = new User();
                            $user->id = $user_id;
                            $user->username = $username;
                            $user->name = $data->nama;
                            $user->level = 'Mahasiswa';
                            $user->password = $password;
                            $user->status = 1;
                            $user->save();
                        }
                    }
                    else {
                        $user_id = UUid::uuid4()->getHex();
                        $username = $data->nim;
                        $password = bcrypt(str_replace('/', '', $data->dobForPass));

                        $user = new User();
                        $user->id = $user_id;
                        $user->username = $username;
                        $user->name = $data->nama;
                        $user->level = 'Mahasiswa';
                        $user->password = $password;
                        $user->status = 1;
                        $user->save();
                    }

                    $pekerjaanAyah = $data->pekerjaan_ayah;
                    if (($pekerjaanAyah == '') or ($pekerjaanAyah == '-') or ($pekerjaanAyah == '0') ) {
                        $pekerjaanAyah = '8';
                    }
                    $Job = Pekerjaan::select('id')
                        ->where('other_id', '=', $pekerjaanAyah)
                        ->first();

                    $pekerjaanAyah = $Job->id;

                    $pendidikanAyah = $data->ijazah_ayah;
                    if (($pendidikanAyah == '') or ($pendidikanAyah == '-') or ($pendidikanAyah == '9')) {
                        $pendidikanAyah = '0';
                    }
                    $Education = Pendidikan::select('id')
                        ->where('other_id', '=', $pendidikanAyah)
                        ->first();
                    $pendidikanAyah = $Education->id;

                    $pekerjaanIbu = $data->pekerjaan_ibu;
                    if (($pekerjaanIbu == '') or ($pekerjaanIbu == '-') or ($pekerjaanIbu == '0')) {
                        $pekerjaanIbu = '8';
                    }
                    $Job = Pekerjaan::select('id')
                        ->where('other_id', '=', $pekerjaanIbu)
                        ->first();
                    $pekerjaanIbu = $Job->id;

                    $pendidikanIbu = $data->ijazah_ibu;
                    if (($pendidikanIbu == '') or ($pendidikanIbu == '-') or ($pendidikanIbu == '9')) {
                        $pendidikanIbu = '0';
                    }

                    $Education = Pendidikan::select('id')
                        ->where('other_id', '=', $pendidikanIbu)
                        ->first();
                    $pendidikanIbu = $Education->id;

                    $agama = $data->agama;
                    if (($agama == '') or ($agama == '-') or ($agama == '0')) {
                        $agama = '7';
                    }

                    $Religion = Agama::select('id')
                        ->where('other_id', '=', $agama)
                        ->first();
                    $agama = $Religion->id;

                    $golMhs = $data->gol_mhs;
                    if (($golMhs == '') or ($golMhs == '-') or ($golMhs == '0')) {
                        $golMhs = '1';
                    }
                    $Category = GolonganMahasiswa::select('id')
                        ->where('other_id', '=', $golMhs)
                        ->first();

                    $golMhs = $Category->id;

                    $mahasiswa_id = UUid::uuid4()->getHex();
                    $mahasiswa = new Mahasiswa();
                    $mahasiswa->id = $mahasiswa_id;
                    $mahasiswa->nim = $data->nim;
                    $mahasiswa->year_id = $tahun_id;
                    $mahasiswa->prodi_id = $prodiId;
                    $mahasiswa->lecturer_id = $dosenId;
                    $mahasiswa->curriculum_id  = $kurikulum_id;
                    $mahasiswa->user_id = $user_id;
                    $mahasiswa->status = $status;
                    $mahasiswa->dob = $data->dob;
                    $mahasiswa->category_id = $golMhs;
                    $mahasiswa->edited_by = $superadminId;
                    $mahasiswa->save();

                    $detailMhs = new MahasiswaDetail();
                    $detailMhs->id = UUid::uuid4()->getHex();
                    $detailMhs->candidate_id = '';
                    $detailMhs->name = $data->nama;
                    $detailMhs->idcard_number = '';
                    $detailMhs->dob_place = $data->tempat_lahir;
                    $detailMhs->gender = $data->gender;
                    $detailMhs->religion_id = $agama;
                    $detailMhs->address = $data->alamat;
                    $detailMhs->kelurahan = '';
                    $detailMhs->district = '';
                    $detailMhs->city = '';
                    $detailMhs->province = $data->propinsi;
                    $detailMhs->postal_code = '';
                    $detailMhs->phone_fixed = $data->tel;
                    $detailMhs->phone_mobile = $data->hp;
                    $detailMhs->email = $data->email;
                    $detailMhs->high_school = $data->asal_SLA;
                    $detailMhs->year_graduate = $data->th_ijazah_SLA;
                    $detailMhs->father_name = $data->nama_ortu;
                    $detailMhs->parent_address = $data->alamat_ortu;
                    $detailMhs->father_occupation_id =$pekerjaanAyah;
                    $detailMhs->father_agency = '';

                    $detailMhs->student_id = $mahasiswa_id;
                    $detailMhs->blood_type = $data->gol_darah;
                    $detailMhs->citizen = $data->warga_negara;
                    $detailMhs->status_work = $data->status_kerja;
                    $detailMhs->student_diploma = $data->ijazah_SLA;
                    $detailMhs->status_transfer = $data->status_pindah;
                    $detailMhs->university_origin = $data->nama_pt_asal;
                    $detailMhs->father_occupation_id	 = $pekerjaanAyah;
                    $detailMhs->father_education_id = $pendidikanAyah;
                    $detailMhs->mother_education_id = $pendidikanIbu;
                    $detailMhs->mother_occupation_id = $pekerjaanIbu;

                    $detailMhs->save();
                }

            }
            DB::commit();
            return view('/admin/import')
                ->with('datas', $datas)
                ->with('error', 'none');

        }
        catch (\Exception $e) {
            DB::rollback();
            return view('/admin/import')
                ->with('datas', '')
                ->with('error', $e);
        }
    }

    public function importKurikulum () {

        DB::beginTransaction();
        try {
            $superadminId = $this->getSuperadminId();

            $datas = siakad_mk::select('tahun_akademik')
                ->groupBy('tahun_akademik')
                ->orderBy('tahun_akademik')
                ->get();



            foreach ($datas as $data) {
                $prodis = Prodi::select('id')
                    ->get();

                foreach ($prodis as $prodi) {
                    $yearId = $this->getTahunId($data->tahun_akademik);
                    //var_dump($yearId);die();
                    $result = Kurikulum::select('id')
                        ->where('year_id', $yearId)
                        ->where('prodi_id', $prodi->id)
                        ->first();
                    if (!$result) {
                        $kurikulum = new Kurikulum();
                        $kurikulum->id = UUid::uuid4()->getHex();
                        $kurikulum->name = $data->tahun_akademik;
                        $kurikulum->prodi_id = $prodi->id;
                        $kurikulum->year_id =$yearId;
                        $kurikulum->required_credit = '0';
                        $kurikulum->elective_credit = '0';
                        $kurikulum->edited_by = $superadminId;
                        $kurikulum->save();
                    }
                }
            }
            DB::commit();


        }
        catch (\Exception $e) {
            DB::rollback();
            return view('/admin/import')
                ->with('datas', '')
                ->with('error', $e);
        }
    }

    public function getSuperadminId() {
        $data = User::select('id')
            ->where('username', 'superadmin')
            ->first();
        $superadmin_id = $data->id;

        return $superadmin_id;
    }

    public function importDosen() {
        DB::beginTransaction();
        try {
            $superadminId = $this->getSuperadminId();
            $datas = siakad_dosen::select('NID', 'nama', 'alamat', 'gender', 'fixed_phone', 'mobile_phone', 'email',
                'tempat_lahir', 'status_dosen', DB::raw('date_format(dob,"%d/%m/%Y") as dob_for_pass'), 'dob', 'prodi_id', 'tipe_dosen')
                ->orderBy('NID')
                ->get();

            $user = new User();
            $dosen = new Dosen();

            foreach ($datas as $data) {
                $result = Dosen::select('nid')
                    ->where('nid', $data->NID)
                    ->first();
                if (!$result) {

                    $user_id = UUid::uuid4()->getHex();
                    $username = $data->NID;
                    $password = bcrypt(str_replace('/', '', $data->dob_for_pass));

                    $user = new User();
                    $user->id = $user_id;
                    $user->username = $username;
                    $user->name = $data->nama;
                    $user->level = 'Dosen';
                    $user->password = $password;
                    $user->status = 1;
                    $user->save();

                    $dosen = new Dosen();
                    $dosen->id = Uuid::uuid4()->getHex();
                    $dosen->user_id = $user_id;
                    $dosen->name = $data->nama;
                    $dosen->nid = $data->NID;
                    $dosen->nidn = '';
                    $dosen->nip = '';
                    $dosen->alamat = $data->alamat;
                    $dosen->gender = $data->gender;
                    $dosen->alamat = $data->alamat;
                    $dosen->phone_fixed = $data->fixed_phone;
                    $dosen->phone_mobile = $data->mobile_phone;
                    $dosen->email = $data->email;
                    $dosen->dob_place = $data->tempat_lahir;
                    $dosen->dob = $data->dob;
                    if ($data->prodi_id != null) {
                        $prodiId = Prodi::select('id')->where('other_id', '=',  $data->prodi_id)->first();
                        $dosen->prodi_id = $prodiId->id;
                    } else {
                        $dosen->prodi_id = '';
                    }

                    $dosen->type = $data->tipe_dosen;
                    $status = StatusDosen::select('id')
                        ->where('other_id', '=',  $data->prodi_id)
                        ->first();

                    if (! $status) {
                        $status = StatusDosen::select('id')->where('other_id', '=', '2')->first();
                    }
                    $dosen->status_id = $status->id;

                    $dosen->religion_id = '';
                    $dosen->edited_by = $superadminId;
                    $dosen->save();
                }


            }
            DB::commit();

        }
        catch (\Exception $e) {
            DB::rollback();
            return view('/admin/import')
                ->with('datas', '')
                ->with('error', $e);

        }
    }

    public function importTemp() {
        ini_set('max_execution_time', 12000);

        $this->importDosen();
        $this->importKurikulum();
        $this->importMahasiswa();
        $this->importMatakuliah();
        $this->importKRS();
        $this->fixingKRS();

        //return view('/admin/import');
    }

    public function getMhsProdi($mhsId) {
        $result = Mahasiswa::select('prodi_id')
            ->where('id', '=', $mhsId)
            ->first();

        return $result->prodi_id;
    }

    public function fixingKRS() {
        ini_set('max_execution_time', 1200);
        $krsId = '';
        DB::beginTransaction();
        try {
        $krs = Krs::select('student_id', 'id')
            ->get();
            $totalSks = 0;
            $totalSKsxn = 0;
        foreach ($krs as $item) {
            $krsDetail = KrsDetail::select('krs_detail.id as krs_detail_id', 'krs_detail.grade as nilai', 'courses.credit as sks' )
                ->leftjoin('class', 'class.id', '=', 'krs_detail.class_id')
                ->leftjoin('courses', 'courses.id', '=', 'class.courses_id')
                ->where('krs_id', '=', $item->id)
                ->get();

            $totalSks = 0;
            $totalSKsxn = 0;
            foreach ($krsDetail as $data) {
                $sks = (int) ($data->sks);
                $sksxn = $sks * GlobalHelper::getGradeValue($data->nilai);
                $totalSks += $sks;
                $totalSKsxn = $totalSKsxn + $sksxn;

                $temp = KrsDetail::where('id', '=', $data->krs_detail_id)->first();
                $temp->gradecredit = $sksxn;
                $temp->update();
            }
            if ($totalSks == 0) {
                $ip = 0;
            }
            else {
                $ip = $totalSKsxn / $totalSks;
            }


            if ($ip < 1.5) {
                $allowedSks = 12;
            }
            elseif ($ip >= 1.5 && $ip <= 2) {
                $allowedSks = 16;
            }
            elseif ($ip >= 1.5 && $ip <= 2) {
                $allowedSks = 16;
            }
            elseif ($ip >= 2 && $ip <= 2.5) {
                $allowedSks = 20;
            }
            elseif ($ip >= 2.51 && $ip <= 3.5) {
                $allowedSks = 22;
            }
            elseif ($ip >= 3.51 && $ip <= 4) {
                $allowedSks = 24;
            }

            $temp = Krs::where('id', '=', $item->id)->first();
            $temp->gp = $ip;
            $temp->credit_next = $allowedSks;
            $temp->credit_total = $totalSks;
            $temp->update();
        }

        $arrTranskrip = Transkrip::select('id')
            ->get();

        foreach ($arrTranskrip as $transkrip) {
            $totalSKsxn = 0;
            $totalSks2 = 0;
            $detail = TranskripDetail::Select('courses.credit as sks', 'grade as nilai',
                'gradecredit as sksxn')
                ->leftjoin('courses', 'courses.id', '=', 'transcript_detail.courses_id')
                ->where('transcript_id', '=', $transkrip->id)
                ->get();
            foreach ($detail as $item) {
                $sks = (int)($item->sks);
                $totalSks2 += $sks;
                $sksxn = (int)($item->sksxn);
                $totalSKsxn += $sksxn;
            }
            if ($totalSks2 == 0) {
                $ipk = 0;
            }
            else {
                $ipk = $totalSKsxn / $totalSks2;
            }


            $temp = Transkrip::where('id', '=', $transkrip->id)->first();
            $temp->credit = $totalSks2;
            $temp->gpa = $ipk;
            $temp->update();
        }
            DB::commit();

        }
        catch (\Exception $e) {
            DB::rollback();
            return view('/admin/import')
                ->with('datas', $totalSks)
                ->with('error', $e);

        }
    }


}
