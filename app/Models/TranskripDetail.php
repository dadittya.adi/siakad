<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranskripDetail extends Model
{
    protected $table = 'transcript_detail';
    protected $primaryKey = 'id';
    public $incrementing = false;
}
