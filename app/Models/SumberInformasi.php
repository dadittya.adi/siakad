<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SumberInformasi extends Model
{
    protected $table = 'source_information';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public static function getInfSourceList() {
        $infSources = SumberInformasi::select('id as source_id', 'name as source_name')
            ->get();

        return $infSources;
    }
}
