<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisMK extends Model
{
    protected $table = 'courses_type';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public static function getJenisMkList() {
        $result = JenisMK::select('id as jenis_id', 'name as jenis_nama')
            ->get();

        return $result;
    }
}
