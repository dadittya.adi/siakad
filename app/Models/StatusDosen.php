<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusDosen extends Model
{
    protected $table = 'lecturer_status';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public static function getDosenStatusList() {
        $result = self::select('id as status_id', 'name as status_nama')
            ->get();

        return $result;
    }

}
