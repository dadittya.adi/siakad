<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    protected $table = 'study_programs';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public static function getProdiList() {
        $prodis = Prodi::select('id as prodi_id', 'name as prodi_name')
            ->where('status', '1')
            ->orderBy('name')
            ->get();

        return $prodis;
    }

    public static function getProdiCode($prodiId) {
        $result = Prodi::select('nim_code as kode')
            ->where('id', '=', $prodiId)
            ->first();

        return $result->kode;
    }

    public static function getProdiId($key) {
        $result = Prodi::select('id')
            ->where('name', '=', $key)
            ->first();

        return $result->id;
    }

}
