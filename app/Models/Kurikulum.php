<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kurikulum extends Model
{
    protected $table = 'curriculum';
    protected $primaryKey = 'id';
    public $incrementing = false;
}
