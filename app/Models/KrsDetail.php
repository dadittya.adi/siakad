<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KrsDetail extends Model
{
    protected $table = 'krs_detail';
    protected $primaryKey = 'id';
    public $incrementing = false;
}
