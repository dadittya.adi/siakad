<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'lecturer';
    public $incrementing = false;
    protected $primaryKey = 'id';

    public function user() {
        return $this->belongsTo('App/Models/User');
    }

    Public function getDosenId($nid) {
    $result = Dosen::select('id')
        ->where('nid', $nid)
        ->first();
    if ($result) {
        return $result->id;
    }
    else {
        return '0';
    }
    }

    public static function getDosenList() {
        $dosen = Dosen::select('lecturer.id as id', 'lecturer.name as name')
            ->leftJoin('lecturer_status', 'lecturer_status.id', '=', 'lecturer.status_id')
            ->where('lecturer_status.name', '=', 'Aktif')
            ->get();

        return $dosen;
    }

}
