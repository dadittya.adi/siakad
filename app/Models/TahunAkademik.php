<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;


class TahunAkademik extends Model
{
    protected $table = 'years';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public static function getYearString($id, $type='single') {
        $year = self::select('name', 'range')
            ->where('id', '=', $id)
            ->first();
        if($type == 'single') {
            return $year->name;
        }
        else {
            return $year->range;
        }

    }

    public static function getYearList() {
        $years = TahunAkademik::select('id as year_id', 'name as year_angkatan',
            'range as year_akademik')
            ->orderby('name', 'desc')
            ->get();
        return $years;
    }

    public static function getYearId($string) {
        $years = TahunAkademik::select('id')
            ->where('name', $string)
            ->orWhere('range', $string)
            ->first();
        if ($years){
            return $years->id;
        }
        else {
            if (strlen($string) == 9) {
                $strName = $string;
                $strYear = substr($string,4);
            }
            else {
                $strName = $string.'/'.($string+1);
                $strYear = $string;
            }
            $id = Uuid::uuid4()->getHex();
            $tahun = new TahunAkademik();

            $tahun->id = $id;
            $tahun->name = $strName;
            $tahun->range = $strYear;
            $tahun->save();

            return $id;
        }

    }

    public static function getLatestYearId($type = 'students') {
        //cari apakah sudah ada mhasiswa baru yang masuk dan belum krs
        if ($type == 'students') {
            $result = Mahasiswa::select('year_id')
                ->orderby('created_at', 'desc')
                ->first();
        }
        else {
            $result = CalonMahasiswa::select('year_id')
                ->orderby('created_at', 'desc')
                ->first();
        }

        if($result) {
            return $result->year_id;
        }
        else {
            $result = TahunAkademik::select('id')
                ->orderby('name', 'desc')
                ->first();
            return $result->id;
        }
    }

}
