<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transkrip extends Model
{
    protected $table = 'transcript';
    public $incrementing = false;
    protected $primaryKey = 'id';
}
