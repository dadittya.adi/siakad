<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class siakad_dosen extends Model
{
    protected $table = 'siakad_dosen';
    protected $primaryKey = 'siakad_dosen';
}
