<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KurikulumDetail extends Model
{
    protected $table = 'curriculum_detail';
    protected $primaryKey = 'id';
    public $incrementing = false;
}
