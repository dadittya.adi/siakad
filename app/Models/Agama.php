<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    protected $table = 'religions';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public static function getAgamaList() {
        $religions = Agama::select('id', 'name')
            ->orderby('name')
            ->get();

        return $religions;
    }
}
