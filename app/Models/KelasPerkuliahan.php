<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KelasPerkuliahan extends Model
{
    protected $table = 'class';
    protected $primaryKey = 'id';
    public $incrementing = false;
}
