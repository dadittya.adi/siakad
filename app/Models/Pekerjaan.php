<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $table = 'ocupation';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public static function getPekerjaanList() {
        $jobs = Pekerjaan::select('id as pekerjaan_id', 'name as pekerjaan_nama')
            ->get();

        return $jobs;
    }
}
