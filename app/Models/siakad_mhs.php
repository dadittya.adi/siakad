<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class siakad_mhs extends Model
{
    protected $table = 'siakad_mhs';
    protected $primaryKey = 'nim';
}
