<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KelasPengajar extends Model
{
    protected $table = 'courses_lecturer';
    protected $primaryKey = 'id';
    public $incrementing = false;
}
