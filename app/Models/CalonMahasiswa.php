<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalonMahasiswa extends Model
{
    protected $table = 'candidates';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public function prodi() {
        return $this->belongsTo('App/Models/Prodi');
    }

    public function mahassiswaDetail() {
        return $this->belongsTo('App/Models/MahasiswaDetail');
    }

    public static function getNewRegNumber($prodiId, $yearId) {
        $count = CalonMahasiswa::select('id')
            ->where('prodi_id', '=', $prodiId)
            ->where('year_id', '=', $yearId)
            ->count();
        if (($count+1) < 10) {
            $nextNum = '00'.($count+1);
        }
        else {
            $nextNum = '0'.($count+1);
        }

        $year = TahunAkademik::getYearString($yearId);
        $year = substr($year, -2);

        return $year.Prodi::getProdiCode($prodiId).$nextNum;
    }
}
