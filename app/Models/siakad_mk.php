<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class siakad_mk extends Model
{
    protected $table = 'siakad_mk';
    protected $primaryKey = 'siakad_mk_id';
}
