<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KelompokMK extends Model
{
    protected $table = 'courses_category';
    protected $primaryKey = 'id';
    public $incrementing = false;

    public static function getKelompokMkList() {
        $result = KelompokMK::select('id as kelompok_id', 'name as kelompok_nama')
            ->get();

        return $result;
    }
}
