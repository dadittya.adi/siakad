<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MahasiswaDetail extends Model
{
    protected $table = 'student_detail';
    protected $primaryKey = 'id';
    public $incrementing = false;

}
