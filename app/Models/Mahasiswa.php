<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mahasiswa extends Model
{
    protected $table = 'students';
    public $incrementing = false;
    protected $primaryKey = 'id';

    public function prodi() {
        return $this->belongsTo('App/Models/Prodi');
    }

    public function mahasiswaDetail() {
        return $this->belongsTo('App/Models/MahaaiswaDetail');
    }

    public function user() {
        return $this->belongsTo('App/Models/User');
    }

    public static function getMhsId($nim) {
        $result = Mahasiswa::select('id')
            ->where('nim', $nim)
            ->first();
        if ($result) {
            return $result->id;
        }
        else {
            return '';
        }
    }

    public static function getMahasiswaList($prodiId, $yearId) {
        $result = Mahasiswa::query();
        $result = $result->select(
            'students.id as mahasiswa_id', 'student_detail.name as nama', 'students.nim as nim',
            'student_detail.gender as gender', 'religions.name as agama', DB::raw('date_format(students.dob,"%d/%m/%Y") as dob'),
            'study_programs.name as nama_prodi', 'years.range as angkatan', 'students.status as status')
            ->leftjoin('years', 'students.year_id', '=', 'years.id' )
            ->leftjoin('study_programs', 'students.prodi_id', '=', 'study_programs.id' )
            ->leftjoin('student_detail', 'students.id', '=', 'student_detail.student_id' )
            ->leftjoin('religions', 'religions.id', '=', 'student_detail.religion_id' );

        if (is_array($prodiId)) {
            if (count($prodiId) > 0) {
                $result = $result->where(function ($query) use ($prodiId){
                    $query = $query->where('students.prodi_id', $prodiId[0]);
                    for ($i = 1; $i < count($prodiId); $i++) {
                        $query = $query->orWhere('students.prodi_id', '=', $prodiId[$i]);
                    }
                });
            }
        }
        else {
            if ($prodiId != '0') {
                $result = $result->where('students.prodi_id', '=', $prodiId);
            }
        }


        if (is_array($yearId)) {
            if (count($yearId) > 0) {
                $result = $result->where(function ($query) use ($yearId){
                    $query = $query->where('students.year_id', $yearId[0]);
                    for ($i = 1; $i < count($yearId); $i++) {
                        $query = $query->orWhere('students.year_id', '=', $yearId[$i]);
                    }
                });
            }
        }
        else {
            if ($yearId != '0') {
                $result = $result->where('students.year_id', '=', $yearId);
            }
        }

        $result = $result->orderBy('students.nim');
        return $result->get();
    }

    public static function getNewNim($prodiId, $yearId) {
        $count = Mahasiswa::select('calon_mahasiswa_id')
            ->where('prodi_id', '=', $prodiId)
            ->where('angkatan_id', '=', $yearId)
            ->count();
        if (($count+1) < 9) {
            $nextNum = '00'.($count+1);
        }
        else {
            $nextNum = '0'.($count+1);
        }

        $year = self::getSingleYearString($yearId);
        $year = substr($year, -2);

        return self::getProdiCode($prodiId).$year.$nextNum;
    }



}
