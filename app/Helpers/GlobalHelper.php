<?php

/**
 * Created by PhpStorm.
 * User: dadit
 * Date: 5/11/2017
 * Time: 9:54 AM
 */
namespace App\Helpers;

use App\Models\Dosen;
use App\Models\KelompokMK;
use App\Models\Krs;
use App\Models\Prodi;
use App\Models\StatusDosen;
use App\Models\TahunAkademik;
use App\Models\Agama;
use App\Models\Pekerjaan;
use App\Models\Mahasiswa;
use App\Models\SumberInformasi;
use App\Models\JenisMK;
use App\Models\CalonMahasiswa;
use App\Models\Transkrip;
use App\Models\TranskripDetail;
use Illuminate\Support\Facades\DB;

class GlobalHelper
{


    public static function getCurrentYear($type = "double") {
        $currYear = date('Y');
        if ($type == "single") {
            return $currYear;
        }
        else {
            return $currYear.'/'.($currYear+1);
        }

    }

    public static function getLatestYearId($type = 'students') {
        //cari apakah sudah ada mhasiswa baru yang masuk dan belum krs
        if ($type == 'students') {
            $result = Mahasiswa::select('year_id')
                ->orderby('created_at', 'desc')
                ->first();
        }
        else {
            $result = CalonMahasiswa::select('year_id')
                ->orderby('created_at', 'desc')
                ->first();
        }

        if($result) {
            return $result->year_id;
        }
        else {
            $result = TahunAkademik::select('id')
                ->orderby('name', 'desc')
                ->first();
            return $result->id;
        }
    }



    public static function getSingleYearString($yearId) {
        $years = TahunAkademik::select('tahun_akademik_angkatan')
            ->where('tahun_akademik_id', $yearId)
            ->first();
        return $years->tahun_akademik_angkatan;
    }

    public static function getGradeValue($grade) {
        $value = 0;
        if ($grade == 'A') {
            $value = 4;
        }
        elseif ($grade =='AB'){
            $value = '3.5';
        }
        elseif ($grade =='B'){
            $value = '3';
        }
        elseif ($grade =='BC'){
            $value = '2.5';
        }
        elseif ($grade =='C'){
            $value = '2';
        }
        elseif ($grade =='CD'){
            $value = '1.5';
        }
        elseif ($grade =='D'){
            $value = '1';
        }
        elseif ($grade =='E'){
            $value = '0';
        }
        elseif ($grade =='T'){
            $value = '0';
        }
        return $value;
    }













    public static function recalculateIpk($mhsId) {
        $result = Transkrip::select('transkrip_id')
            ->where('mahasiswa_id', '=', $mhsId)
            ->first();
        $details = TranskripDetail::select(DB::raw('sum(matakuliah_.sks) as sks'),
            DB::raw('sum(transkrip_detail_sksxn) as sksxn'))
            ->leftjoin('matakuliah', 'matakuliah.matakuliah_id', '=', 'transkrip_detail.matakuliah_id')
            ->where('transkrip_id', '=', $result->transkrip_id)
            ->first();

        return $details->sksxn / $details->sks;
    }





    public static function getLatestSemester(){
        $result = Krs::select('semester')
            ->orderBy('created_at', 'desc')
            ->first();

        if (($result->krs_semester % 2) == 0) {
            return 'Genap';
        }
        else {
            return 'Ganjil';
        }
    }

    public static function tanggal_indo($tanggal)
    {
        $bulan = array (1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

    public static function getSemester() {
        $month = date('n');
        if ($month <= 3 && $month <=9) {
            return 'Ganjil';
        }
        else {
            return 'Genap';
        }
    }
}