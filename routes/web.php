<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin', function () {
    return view('/admin/dashboard');
});

Route::get('/login', 'Auth/LoginController@showLogin');
Route::get('/admin/user', 'Admin\UserController@index');
Route::post('/admin/user/add', 'Admin\UserController@store');
Route::post('/admin/user/search', 'Admin\UserController@search');
Route::get('/admin/user/{id}', 'Admin\UserController@show');
Route::put('admin/user/edit', 'Admin\UserController@edit');

Route::get('/admin/dosen', 'Admin\DosenController@index');
Route::post('/admin/dosen/add', 'Admin\DosenController@store');
Route::post('/admin/dosen/search', 'Admin\DosenController@search');
Route::get('/admin/dosen/{id}', 'Admin\DosenController@show');
Route::put('/admin/dosen/edit', 'Admin\DosenController@edit');

Route::get('/admin/pendaftaran/', 'Admin\CalonMahasiswaController@index');
Route::get('/admin/pendaftaran/add', 'Admin\CalonMahasiswaController@showMhsRegistrationForm');
Route::post('/admin/pendaftaran/pendaftaran/store', 'Admin\CalonMahasiswaController@store');
Route::post('/admin/pendaftaran/search', 'Admin\DosenController@search');
Route::get('/admin/pendaftaran/{id}', 'Admin\DosenController@show');
Route::put('/admin/pendaftaran/edit', 'Admin\DosenController@edit');
Route::post('/admin/pendaftaran/grabRegNumber/', 'Admin\CalonMahasiswaController@grabNewNoPendaftaran');
Route::get('/admin/penerimaan-mahasiswa/{id}', 'Admin\CalonMahasiswaController@getDataToAccept');
Route::get('/admin/pendaftaran/penerimaan-mahasiswa/{id}', 'Admin\CalonMahasiswaController@getDataToAccept');

Route::get('/admin/matakuliah', 'Admin\MatakuliahController@index');
Route::post('/admin/matakuliah/add', 'Admin\MatakuliahController@store');
Route::get('/admin/matakuliah/{id}', 'Admin\MatakuliahController@show');
Route::put('admin/matakuliah/edit', 'Admin\MatakuliahController@edit');
Route::post('/admin/matakuliah/search', 'Admin\MatakuliahController@search');

Route::get('/admin/kurikulum', 'Admin\KurikulumController@index');
Route::get('/admin/kurikulum/copy/{id}', 'Admin\KurikulumController@copy');
Route::post('/admin/kurikulum/search', 'Admin\KurikulumController@getKurikulumForJson');
Route::get('/admin/kurikulum/detail/{id}', 'Admin\KurikulumController@getKurikulumDetail');
Route::post('/admin/kurikulum/store', 'Admin\KurikulumController@store');

Route::get('/admin/mahasiswa/', 'Admin\MahasiswaController@index');
Route::post('/admin/mahasiswa/search', 'Admin\MahasiswaController@search');

Route::get('/admin/khs/', 'Admin\KHSController@index');
Route::get('/admin/khs/{mhsId}', 'Admin\KHSController@KHSDetailIndex');
Route::post('/admin/khs/search', 'Admin\KHSController@search');
Route::post('/admin/khs/khs/search/semester', 'Admin\KHSController@getKhsdetailforJson');
Route::get('/admin/khs/print-single/{krsId}', 'Admin\KHSController@print_single');

Route::get('/admin/transkrip/', 'Admin\TranskripController@index');
Route::get('/admin/transkrip/{mhsId}', 'Admin\TranskripController@getTranskripDetail');
Route::post('/admin/transkrip/search', 'Admin\TranskripController@search');

Route::get('/admin/import/', 'Admin\import_db_controller@importTemp');
Route::get('/admin/import/dosen', 'Admin\import_db_controller@importDosen');
Route::get('/admin/import/kurikulum', 'Admin\import_db_controller@importKurikulum');
Route::get('/admin/import/matakuliah', 'Admin\import_db_controller@importMatakuliah');
Route::get('/admin/import/mahasiswa', 'Admin\import_db_controller@importMahasiswa');
Route::get('/admin/import/krs', 'Admin\import_db_controller@importKRS');
Route::get('/admin/import/transkrip', 'Admin\import_db_controller@fixingKRS');

Route::get('test', 'TestController@index');

Route::get('/admin/kelas/', 'Admin\KelasPerkuliahanController@index');
Route::post('/admin/kelas/search', 'Admin\KelasPerkuliahanController@getKelasForJson');

Route::get('admin/program-studi/', 'Admin\ProdiController@index');
Route::get('admin/program-studi/{id}', 'Admin\ProdiController@getProdiData');
Route::put('admin/program-studi/edit', 'Admin\ProdiController@edit');

Route::get('/admin/nilai/', 'Admin\NilaiController@index');
Route::get('/admin/nilai/admin/nilai', 'Admin\NilaiController@index');
Route::get('/admin/nilai/{id}', 'Admin\NilaiController@KelasDetail');

Route::get('/admin/krs/', 'Admin\KRSController@index');
Route::get('/admin/krs/admin/krs', 'Admin\KRSController@index');
Route::get('/admin/krs/{id}', 'Admin\KRSController@KelasDetail');




Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/admin/test/{prodiId}/{tahun}', 'TestController@tempTest');